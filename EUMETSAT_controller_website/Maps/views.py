from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from django.http import JsonResponse

# Create your views here.



def Maps_Initial_data():

    dict_initial_data = {
    }
    return dict_initial_data
#end def


def Maps_view(request):
    dict_initial_data = Maps_Initial_data()

    # template & context
    template = 'Maps/Maps.html'
    context = {

    }
    return render(request, template, context)
#end def


class Maps_view2(View):

    # =============================================================================
    # GET def
    # =============================================================================
    def get(self, request):
        dict_initial_data = Maps_Initial_data()
        
        if ( request.is_ajax() ):

            # ajax context
            context_ajax = {
            }
            return JsonResponse(context_ajax, status=200)
        #end if

        # template & context
        template = 'Maps/Maps.html'
        context = {
        }
        return render(request, template, context)
    #end def
#end class