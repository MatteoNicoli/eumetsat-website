# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 21:15:00 2021

@author: Nicoli
"""
# =============================================================================
# 
# =============================================================================
import os, sys

DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))


#
OS_Name = os.name
OS_UserName = os.getlogin()
UserName = OS_UserName

if (OS_Name=='nt'):
    path = os.path.join( 'C:', os.sep, "Users", OS_UserName )
elif (OS_Name=='posix'):
    path = os.path.join( os.sep, "home", OS_UserName )
#end if
DesktopPath = os.path.join(path, "Desktop")


# =============================================================================
# 
# =============================================================================
from static.py.Sup_fcns import *
from EPS_Sup import *









# =============================================================================
# 
# =============================================================================
Path_SupFile_List_LogBookIDs = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','LogBookID.csv')
if (os.path.isfile(Path_SupFile_List_LogBookIDs) == True):
    List_LogBookIDs = pd.read_csv(Path_SupFile_List_LogBookIDs, index_col=0)
#end if

Path_SupFile_List_ESOC_IDs = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','ESOC_antenna_ID.csv')
if (os.path.isfile(Path_SupFile_List_ESOC_IDs) == True):
    List_ESOC_IDs = pd.read_csv(Path_SupFile_List_ESOC_IDs, index_col=0)
#end if



# =============================================================================
# 
# =============================================================================
Datetime_UTC = datetime.datetime.utcnow()
Mission= 'EPS'

Sh = 'M'
DateS = datetime.datetime(2021, 11, 2, 11, 00 )
DateE = datetime.datetime(2021, 11, 2, 20, 0, 0 )

#
Date = datetime.datetime(2021, 10, 30, 0, 0, 0 )
Date = datetime.datetime(Datetime_UTC.year, Datetime_UTC.month, Datetime_UTC.day, 0, 0, 0 )
Sh = 'M'
hour_shift, hour_shift_2 = DateSolar( Date )
DateS, DateE = Date2Shift( Mission, Date, Sh, hour_shift, hour_shift_2)

#
Shift_Info = Date_Shift( Mission, Datetime_UTC )
Sh = Shift_Info[0]
DateS = Shift_Info[1]
DateE = Shift_Info[2]


# =============================================================================
# 
# =============================================================================
SetUp = {
        'SideSpaCon': True,
        'SideGrndCon': True,
        'Antenna': 'CDA1',
        'SC': ['METOP-B','METOP-C'],
        'Daily_Events': True,
        'NOAA_BOS': True,
        'Date_Shift': datetime.date(2021, 8, 19),
        'Shift': Sh,
        'Date_start': DateS,
        'Date_end': DateE,
        'file_report': False,
    }
MS_Antenna = 'CDA2'
if ( SetUp['Antenna']=='CDA2' ):
    MS_Antenna = 'CDA1'
#end if
SetUp_MS = {
        'MS_Date_start': None,
        'MS_Date_end': None,
        'MS_SC': ['METOP-A','METOP-B','METOP-C', 'NOAA 19','NOAA 18'],
        'MS_Antenna': MS_Antenna,
    }
SetUp_CXTTCFBK = {
        'C_XTTC_FBK_SC': 'METOP-B',
        'C_XTTC_FBK_nOrbit': None,
        'C_XTTC_FBK_Antenna': 'FBK_',
        'C_XTTC_FBK_Covis': False,
        'C_XTTC_FBK_Extend': False,
    }

SetUp_Overall = {
    'SetUp': SetUp,
    'SetUp_MS': SetUp_MS,
    'SetUp_CXTTCFBK': [SetUp_CXTTCFBK],
    }
ToUberlog = {
    'ToUberlog': 'import',
    'Uberlog_ID': 'MatteoN',
    'Uberlog_Paswd': 'elog',
    }





# =============================================================================
# 
# =============================================================================
from multiprocessing.pool import ThreadPool
pool = ThreadPool(processes=1)
def ThreadPool_EPS_FDF_events(SetUp):
    vm.attachCurrentThread()
    return EPS_FDF_events(SetUp)
#end def

result = pool.apply_async(
    ThreadPool_EPS_FDF_events,
    (SetUp_Overall,)
    )
FDF_events_EPS = result.get()


#FDF_events_EPS = EPS_FDF_events(SetUp_Overall)

# =============================================================================
# 
# =============================================================================
# =============================================================================
# Batch_info = Events2csv(fcn_EPS_events_csv, FDF_events_EPS, SetUp_Overall, ToUberlog, List_LogBookIDs )
# 
# 
# file_Path_Name_csv = os.path.join( DesktopPath, Batch_info['Name']+'.csv' )
# Batch_csv = Batch_info['Data']
# Batch_csv.to_csv (
#     r''+file_Path_Name_csv,
#     #path_or_buf = response,
#     index = False,
#     header = False,
#     )
# =============================================================================
