# -*- coding: utf-8 -*-
"""
Created on Thu Aug 26 17:23:21 2021

@author: Nicoli
"""

# =============================================================================
# 
# =============================================================================
import os
OS_UserName = os.getlogin()
UserName = OS_UserName
DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
Orekit_folder = 'FDF_Orekit'
Orekit_dir = os.path.join(
    DIR[:DIR_int],
    'EUMETSAT_controller_website',
    'static',
    Orekit_folder,
    )




# =============================================================================
# 
# =============================================================================
import datetime
import numpy as np
import pandas as pd
import requests
import re


import matplotlib.path as mpath
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
plt.close('all')

from concurrent.futures import ProcessPoolExecutor


# =============================================================================
# import cartopy.crs as ccrs
# from cartopy.feature.nightshade import Nightshade
# =============================================================================

from spacetrack import SpaceTrackClient


# =============================================================================
# Orekit
# =============================================================================
import orekit
vm = orekit.initVM()
orekit.getVMEnv()



from orekit.pyhelpers import setup_orekit_curdir, absolutedate_to_datetime, to_elevationmask

setup_orekit_curdir(filename=Orekit_dir)

from org.orekit.frames import FramesFactory, TopocentricFrame
from org.orekit.bodies import OneAxisEllipsoid, GeodeticPoint, CelestialBodyFactory
from org.orekit.time import TimeScalesFactory, AbsoluteDate, DateComponents, TimeComponents
from org.orekit.utils import IERSConventions, Constants, PVCoordinates, PVCoordinatesProvider, AbsolutePVCoordinates

from org.orekit.propagation.analytical.tle import TLE, TLEPropagator
from org.orekit.propagation.events import EclipseDetector, EventsLogger, ElevationDetector, PythonAbstractDetector, ElevationExtremumDetector, NodeDetector, EventSlopeFilter, FilterType
from org.orekit.propagation.events.handlers import ContinueOnEvent, PythonEventHandler, EventHandler, StopOnEvent

from org.hipparchus.geometry.euclidean.threed import Vector3D, SphericalCoordinates
from org.hipparchus.ode.events import Action

from org.orekit.orbits import KeplerianOrbit, PositionAngle
from org.orekit.propagation.analytical import KeplerianPropagator
























# =============================================================================
# 
# =============================================================================
def fcn_SCIDs():
    data = [
        ('EPS1', 'M02', 29499),
        ('EPS1', 'M01', 38771),
        ('EPS1', 'M03', 43689),
        ('EPS1', 'N19', 33591),
        ('EPS1', 'N18', 28654),
        ('EPS1', '', 41240),
        ('EPS1', '', 39086),
        ('S3', 'S3A', 41335),
        ('S3', 'S3B', 43437),
        ('S6', 'S6', 46984),
        ]
    data_index   = [
        'METOP-A',
        'METOP-B',
        'METOP-C',
        'NOAA 19',
        'NOAA 18',
        'JASON 3',
        'SARAL',
        'SENTINEL 3A',
        'SENTINEL 3B',
        'S6 MICHAEL FREILICH',
        ]
    data_columns = ['Mission', "shortcut","NORAD CAT ID"]
    list_SCIDs = pd.DataFrame(data, index=data_index, columns=data_columns )
    return list_SCIDs
#end def


def FDF_AntLoc(list_file):
    
    file_coordsband = list_file[0]
    fp=open(file_coordsband, 'r' )
    lines=fp.readlines()
    fp.close()
    
    Mission = list(file_coordsband.split("."))[-1].upper()
    
    
    Antennas_name = []
    Antennas_data = []
    
    for i_row in range(6,len(lines),2):
        X = float(lines[i_row][55:67])
        Y = float(lines[i_row][67:80])
        Z = float(lines[i_row][80:93])
        
        antenna = {'SITE_NAME': lines[i_row][10:18],
            'Provider': Mission,
            'X': X,
            'Y': Y,
            'Z': Z,
            'VX': float(lines[i_row+1][55:67]),
            'VY': float(lines[i_row+1][67:80]),
            'VZ': float(lines[i_row+1][80:93]),
            }
        Antennas_name.append(lines[i_row][32:36])
        Antennas_data.append(antenna)
    #end for
    
    df1 = pd.DataFrame(Antennas_data,index=Antennas_name)
    
    
    
    maskfile = list_file[1]
    fp=open(maskfile, 'r' )
    lines=fp.readlines()
    fp.close()

    
    
    nAnt = int((len(lines)-1)/361.0)    
    Antennas_name = []
    Antennas_data = []
    
    flag_newAnt = True
    Az_lst = []
    El_lst = []
    AzEl_List = []
    for i_row in range(1,len(lines)):
        
        if ( flag_newAnt == True ):
            nrow = int(lines[i_row][:4])
            irow = 0
            ncol = int(lines[i_row][4:7])
            
            Antennas_name.append( lines[i_row][11:15] )
            Az_lst = []
            El_lst = []
            
            flag_newAnt = False
        else:
            irow = irow + 1
            
            data_list = re.findall(r"[-+]?\d*\.\d+|\d+", lines[i_row] )
        
            Az_lst.append( float( data_list[0] ) )
            if ( len(data_list) <= 3 ):
                El_lst.append( float( data_list[1] ) )
            else:
                El_lst.append( float( data_list[2] ) )
                
            if ( irow == nrow ):
                flag_newAnt = True
                AzEl_List.append( [Az_lst,El_lst] )
                Az_lst = []
                El_lst = []
        
    
    
    df2 = pd.DataFrame(AzEl_List, columns = ['Az_M', 'El_M'], index=Antennas_name)
    
    result = pd.concat([df1, df2], axis=1)
    
    return result
#end def


































def getEUM_TLE(scid):
    EUM_TLE = "https://service.eumetsat.int/tle/index.html"
    
    if (scid==29499):
        EUM_TLE_M02 = "https://service.eumetsat.int/tle/data_out/latest_m02_tle.txt"
        EUM_TLE = EUM_TLE_M02
    elif (scid==38771):
        EUM_TLE_M01 = "https://service.eumetsat.int/tle/data_out/latest_m01_tle.txt"
        EUM_TLE = EUM_TLE_M01
    elif (scid==43689):
        EUM_TLE_M03 = "https://service.eumetsat.int/tle/data_out/latest_m03_tle.txt"
        EUM_TLE = EUM_TLE_M03
    elif (scid==33591):
        EUM_TLE_N19 = "https://service.eumetsat.int/tle/data_out/latest_n19_tle.txt"
        EUM_TLE = EUM_TLE_N19
    elif (scid==28654):
        EUM_TLE_N18 = "https://service.eumetsat.int/tle/data_out/latest_n18_tle.txt"
        EUM_TLE = EUM_TLE_N18
    #end if
    
    f = requests.get(EUM_TLE)
    TLEstr = f.text
    tle_line1 = TLEstr[:69]
    tle_line2 = TLEstr[70:-1]
    
    tle = [tle_line1,tle_line2]
    mytle = TLE(tle_line1,tle_line2)
    return mytle
#end def








# =============================================================================
# 
# =============================================================================










def TLEManager(SCID):
    st = SpaceTrackClient('matteo.nicoli.17@gmail.com', 'KG*p_h_VzK7xz*z')
    TLE_Spacetrack = st.tle_latest(norad_cat_id=[SCID], ordinal=1, format='tle')
    tle_list = list(TLE_Spacetrack.split("\n"))
    
    tle_line1 = tle_list[0]
    tle_line2 = tle_list[1]
    mytle = TLE(tle_line1,tle_line2)
    return mytle








def AntenneInfo():
    file_path = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static',Orekit_folder, "files", "stat")
    
    file1 = [ os.path.join( file_path, "coord.sband.eps" ), os.path.join( file_path, "masks.eps" )]
    file2 = [ os.path.join( file_path, "coord.sband.ssc" ), os.path.join( file_path, "masks.ssc" ) ]
    file3 = [ os.path.join( file_path, "coord.sband.esoc" ), os.path.join( file_path, "test_masks.esoc" ) ]
    file4 = [ os.path.join( file_path, "coord.sband.hrpt" ), os.path.join( file_path, "masks.hrpt" ) ]
    file5 = [ os.path.join( file_path, "coord.sband.ant" ), os.path.join( file_path, "masks.ant" ) ]
    
    #
    FDF_AntLoc_EPS = FDF_AntLoc(file1)
    FDF_AntLoc_SSC = FDF_AntLoc(file2)
    FDF_AntLoc_ESOC = FDF_AntLoc(file3)
    # =============================================================================
    # FDF_AntLoc_SSC = FDF_AntLoc(file3)
    # FDF_AntLoc_ESOC = FDF_AntLoc(file2)
    # =============================================================================
    
    frames_FDF_AntLoc = [FDF_AntLoc_EPS, FDF_AntLoc_SSC, FDF_AntLoc_ESOC]
    FDF_AntLoc_Tot = pd.concat(frames_FDF_AntLoc)#.reset_index().drop(columns='index')
    return FDF_AntLoc_Tot


def Frames( FDF_AntLoc_Tot, Antenna):
    # =============================================================================
    # 
    # =============================================================================
    utc = TimeScalesFactory.getUTC()
    inertialFrame = FramesFactory.getEME2000()
    ITRF = FramesFactory.getITRF(
        IERSConventions.IERS_2010,
        True,
        )
    earth = OneAxisEllipsoid(
        Constants.WGS84_EARTH_EQUATORIAL_RADIUS, 
        Constants.WGS84_EARTH_FLATTENING, 
        ITRF,
        )
    
    sun = CelestialBodyFactory.getSun()
    sunRadius = 696000000.0
    
    # =============================================================================
    # Location of the Antennas
    # =============================================================================
    FDF_AntLoc_Tot = AntenneInfo()
    
    
    
    
    # =============================================================================
    # 
    # =============================================================================
    
    position = Vector3D(
        float(FDF_AntLoc_Tot.loc[Antenna,'X']),
        float(FDF_AntLoc_Tot.loc[Antenna,'Y']),
        float(FDF_AntLoc_Tot.loc[Antenna,'Z']),
        )
    
    ElMask = None
    if (FDF_AntLoc_Tot.loc[Antenna,'Az_M']!=float('nan')):
        ElMask = to_elevationmask(FDF_AntLoc_Tot.loc[Antenna,'Az_M'], FDF_AntLoc_Tot.loc[Antenna,'El_M'])
    
    nowUTC = datetime.datetime.utcnow()
    extrapDate = AbsoluteDate(nowUTC.year, nowUTC.month, nowUTC.day, nowUTC.hour, nowUTC.minute, 00.000, utc)
    
    longitude = earth.transform(
        position,
        ITRF,
        extrapDate,
        ).getLongitude()
    long_deg = longitude*180/np.pi
    latitude = earth.transform(
        position,
        ITRF,
        extrapDate,
        ).getLatitude()
    lat_deg = latitude*180/np.pi
    altitude = earth.transform(
        position,
        ITRF,
        extrapDate,
        ).getAltitude()
    
    station = GeodeticPoint(latitude, longitude, altitude)
    station_frame = TopocentricFrame(earth, station, "station 1")
    return utc, earth, inertialFrame, station_frame, ElMask











def PropagetorEvents(inertialFrame, station_frame, ElMask, propagator):

    class myContinueOnEvent(PythonEventHandler):
    
        def init(self, initialstate, target):
            pass
    
        def eventOccurred(self, s, T, increasing):
            return Action.CONTINUE
    
        def resetState(self, detector, oldState):
            return oldState
    
    logger = EventsLogger()
    
# =============================================================================
#     detector_ANX = NodeDetector(inertialFrame)
#     detector_ANX = detector_ANX.withHandler(myContinueOnEvent().of_(NodeDetector))
# =============================================================================
    #
    rawDetector = NodeDetector(inertialFrame)
    filter_ANX = EventSlopeFilter(rawDetector, FilterType.TRIGGER_ONLY_INCREASING_EVENTS)
    detector_ANX = filter_ANX.withHandler(ContinueOnEvent().of_(NodeDetector ))
    logged_detector_ANX = logger.monitorDetector(detector_ANX)
    propagator.addEventDetector(logged_detector_ANX)

    #
    if (ElMask!=None):
        detector_ElMask = ElevationDetector(station_frame).withElevationMask(ElMask)
        detector_ElMask = detector_ElMask.withHandler(ContinueOnEvent().of_(ElevationDetector))
        logged_detector_ElMask = logger.monitorDetector(detector_ElMask)
        propagator.addEventDetector(logged_detector_ElMask)
    
    #
    detector_maxEl= ElevationExtremumDetector(station_frame)
    detector_maxEl = detector_maxEl.withHandler(ContinueOnEvent().of_(ElevationDetector))
    logged_detector_maxEl = logger.monitorDetector(detector_maxEl)
    propagator.addEventDetector(logged_detector_maxEl)
    
    #
    El_AOS0 = np.deg2rad(0.0)
    detector_AOSLOS_0 = ElevationDetector(station_frame).withConstantElevation(float(El_AOS0))
    detector_AOSLOS_0 = detector_AOSLOS_0.withHandler(ContinueOnEvent().of_(ElevationDetector))
    logged_detector_AOSLOS_0 = logger.monitorDetector(detector_AOSLOS_0)
    propagator.addEventDetector(logged_detector_AOSLOS_0)
    
    #
    El_AOS5 = np.deg2rad(5.0)
    detector_AOSLOS_5 = ElevationDetector(station_frame).withConstantElevation(float(El_AOS5))
    detector_AOSLOS_5 = detector_AOSLOS_5.withHandler(ContinueOnEvent().of_(ElevationDetector))
    logged_detector_AOSLOS_5 = logger.monitorDetector(detector_AOSLOS_5)
    propagator.addEventDetector(logged_detector_AOSLOS_5)
    

    return propagator, logger










def AzElDet(propagator, station_frame, inertialFrame, Date):
    pv = propagator.getPVCoordinates(Date, inertialFrame)
    az_tmp = station_frame.getAzimuth(
        pv.getPosition(),
        inertialFrame,
        Date,
        )
    deg_az = np.rad2deg(az_tmp)
    el_tmp = station_frame.getElevation(
        pv.getPosition(),
        inertialFrame,
        Date,
        )
    deg_el = np.rad2deg(el_tmp)
    return deg_az, deg_el
# end def





def PassInfo(mylog, propagator, TLEDate, station_frame, inertialFrame, TLEnOrbit, Antenna, SC, Period):
    start_time = None
    #result = pd.DataFrame()
    TLEnOrbit = TLEnOrbit
    
    
    result = []

    mylog_list = list(mylog)
    nANX = 0
    i_nOrbit = TLEnOrbit
    flag_norbit = False
    for event in range(1,len(mylog_list)):
        idate = absolutedate_to_datetime( mylog_list[event].getState().getDate() )
        #print(idate, mylog_list[event].getEventDetector().toString())
        
        if ( idate>=TLEDate ):
            # ANX
            # ANX = ''
            if ( (mylog_list[event].getEventDetector().toString()).find(('EventSlopeFilter')) != -1 ):
                ANX = mylog_list[event].getState().getDate()
                nANX = nANX + 1
                if (flag_norbit == False):
                    if ( TLEDate<absolutedate_to_datetime(ANX)-datetime.timedelta(minutes=5) ):
                        i_nOrbit = i_nOrbit + 2
                    else:
                        i_nOrbit = i_nOrbit + 1
                else:
                    i_nOrbit = i_nOrbit + 1
                
                flag_norbit = True
            #end if
            if ( event+6 < len(mylog_list) ):
                if ((mylog_list[event-1].getEventDetector().toString()).find(('EventSlopeFilter')) != -1 and
                    (mylog_list[event].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
                    (mylog_list[event+1].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
                    (mylog_list[event+2].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
                    (mylog_list[event+4].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
                    (mylog_list[event+5].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
                    (mylog_list[event+6].getEventDetector().toString()).find(('ElevationDetector')) != -1
                    ):
# =============================================================================
#                 i3date = absolutedate_to_datetime( mylog_list[event+6].getState().getDate() )
#                 if (i3date <= idate+datetime.timedelta(minutes=Period/60/3)):
# =============================================================================
                    ANX = mylog_list[event-1].getState().getDate()
                    AOS0 = mylog_list[event].getState().getDate()
                    AOSM = mylog_list[event+1].getState().getDate()
                    AOS5 = mylog_list[event+2].getState().getDate()
                    
                    LOS5 = mylog_list[event+4].getState().getDate()
                    LOSM = mylog_list[event+5].getState().getDate()
                    LOS0 = mylog_list[event+6].getState().getDate()
                    
                    
                    
                    # AOS 0
                    deg_az_tmp_AOS0, deg_el_tmp_AOS0 = AzElDet(propagator, station_frame, inertialFrame, AOS0)
                    
                    # AOS M
                    deg_az_tmp_AOSM, deg_el_tmp_AOSM = AzElDet(propagator, station_frame, inertialFrame, AOSM)
                    
                    # AOS 5
                    deg_az_tmp_AOS5, deg_el_tmp_AOS5 = AzElDet(propagator, station_frame, inertialFrame, AOS5)
                    
                    # Mid
                    Mid = ''
                    deg_az_tmp_Mid = ''
                    deg_el_tmp_Mid = ''
                    if ( event+3 >= 0 ):
                        if (mylog_list[event+3].getEventDetector().toString().find(('ElevationExtremumDetector')) != -1 ):
                            Mid = mylog_list[event+3].getState().getDate()
                            deg_az_tmp_Mid, deg_el_tmp_Mid = AzElDet(propagator, station_frame, inertialFrame, Mid)
                        #end if
                    #end if
                    
                    # LOS 5
                    deg_az_tmp_LOS5, deg_el_tmp_LOS5 = AzElDet(propagator, station_frame, inertialFrame, LOS5)
                    
                    # LOS M
                    deg_az_tmp_LOSM, deg_el_tmp_LOSM = AzElDet(propagator, station_frame, inertialFrame, LOSM)
                    
                    # LOS 0
                    deg_az_tmp_LOS0, deg_el_tmp_LOS0 = AzElDet(propagator, station_frame, inertialFrame, LOS0)
                    
                    #
                    i_result = [
                        SC,
                        Antenna, 
                        i_nOrbit,
                        
                        absolutedate_to_datetime(ANX),
                        
                        absolutedate_to_datetime(AOS0),
                        deg_az_tmp_AOS0,
                        absolutedate_to_datetime(AOSM),
                        deg_az_tmp_AOSM,
                        absolutedate_to_datetime(AOS5),
                        deg_az_tmp_AOS5,
                    
# =============================================================================
#                     absolutedate_to_datetime(Mid) - datetime.timedelta(seconds=4*60),
#                     absolutedate_to_datetime(Mid) - datetime.timedelta(seconds=3*60+30),
# =============================================================================
                    
                        absolutedate_to_datetime(Mid),
                        deg_az_tmp_Mid,
                        deg_el_tmp_Mid,
                    
# =============================================================================
#                     absolutedate_to_datetime(Mid) + datetime.timedelta(seconds=3*60+30),
#                     absolutedate_to_datetime(Mid) + datetime.timedelta(seconds=4*60),
# =============================================================================
                    
                        absolutedate_to_datetime(LOS5),
                        deg_az_tmp_LOS5,
                        absolutedate_to_datetime(LOSM),
                        deg_az_tmp_LOSM,
                        absolutedate_to_datetime(LOS0),
                        deg_az_tmp_LOS0,
                        
                        
                        absolutedate_to_datetime(LOS0)-absolutedate_to_datetime(AOS0),
                        absolutedate_to_datetime(LOSM)-absolutedate_to_datetime(AOSM),
                        absolutedate_to_datetime(LOS5)-absolutedate_to_datetime(AOS5),
                        ]
                    result.append(i_result)
    
    
    
    datacolumns_Share = ['S/C','Pass','Dump','RNG&DOP', 'Antenna','nOrbit','ANX']
    PassEvent = [
        ("SC"),
        ( "Antenna" ),
        ( 'nOrbit' ),
        ( "ANX" ),
        ( "AOS0" ),
        ( "AOS0_Az" ),
        ( "AOSM" ),
        ( "AOSM_Az" ),
        ( "AOS5" ),
        ( "AOS5_Az" ),
# =============================================================================
#         ( "FillData_start" ),
#         ( "UserData_start" ),
# =============================================================================
        ( "Mid" ),
        ( "Mid_Az" ),
        ( "Mid_el" ),
# =============================================================================
#         ( "UserData_end" ),
#         ( "FillData_end" ),
# =============================================================================
        ( "LOS5" ),
        ( "LOS5_Az" ),
        ( "LOSM" ),
        ( "LOSM_Az" ),
        ( "LOS0" ),
        ( "LOS0_Az" ),
        ( "TM0_Duration" ),
        ( "TM_Duration" ),
        ( "TC_Duration" ),
            ]
    
    
    FDF_events = pd.DataFrame (result, columns = PassEvent)
    return FDF_events






def FDF_PassGenerator(Antenna, list_SCIDs, SC, DateStart, DateEnd, utc, inertialFrame, station_frame, ElMask ):
    # =============================================================================
    # 
    # =============================================================================
    
    #list_SCIDs = fcn_SCIDs()
    SCID = list_SCIDs.loc[SC, "NORAD CAT ID"]
    SC_shortcut = list_SCIDs.loc[SC, "shortcut"]
    mytle = TLEManager(SCID)
    TLE_nOrbit = mytle.getRevolutionNumberAtEpoch()
    TLE_Date = mytle.getDate()    
    Period = 2*np.pi/mytle.getMeanMotion()
    
    propagator = TLEPropagator.selectExtrapolator(mytle)
    propagator, logger = PropagetorEvents(inertialFrame, station_frame, ElMask, propagator)
    
    # =============================================================================
    # 
    # =============================================================================
    TLEDate = absolutedate_to_datetime( TLE_Date )
    DS = TLEDate-datetime.timedelta( minutes=102 )
    initial_date = AbsoluteDate(
        DS.year,
        DS.month,
        DS.day,
        DS.hour,
        DS.minute,
        DS.second + DS.microsecond * 1e-6,
        utc
        )
    final_date = AbsoluteDate(
        DateEnd.year,
        DateEnd.month,
        DateEnd.day,
        DateEnd.hour,
        DateEnd.minute,
        DateEnd.second + DateEnd.microsecond * 1e-6,
        utc,
        )
    
    # =============================================================================
    # 
    # =============================================================================
    state = propagator.propagate(initial_date, final_date)
    
    mylog = logger.getLoggedEvents()
    
    
    FDF_events = PassInfo(mylog, propagator, TLEDate, station_frame, inertialFrame, TLE_nOrbit, Antenna, SC, Period)
    FDF_events['SC_shortcut'] = SC_shortcut
    return FDF_events, propagator