# =============================================================================
# MODULE - django
# =============================================================================
from django import forms
from easy_select2 import select2_modelform, Select2Multiple

# models & forms
from . import models

# Create your forms here.

class MyDateTimeInput(forms.DateTimeInput):
    input_type = 'date'
#end class

class S6_ShiftSetUp_form(forms.ModelForm):
    SC = forms.MultipleChoiceField(
        label='S/C',
        widget=Select2Multiple(
            select2attrs={
                'width': '250px',
                "id": "drpdw_S6_SC",
                "placeholder": "Select operational S/C",
                },
            
        ),
        choices=models.S6_ShiftSetUp.SC_CHOICES,
    )
    Daily_Events = forms.BooleanField(
        label='Daily Events',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 30px;',
                "id": "ckbx_S6_Daily_Events",
            },
        ),
    )
    Date_Shift = forms.DateField(
        label='Date Shift',
        widget=forms.DateInput(
            attrs={
                'type': 'date',
                'class': 'mydjf_datetimefield',
                "id": "date_S6_Date_Shift",
            }
        )
    )
    Shift = forms.CharField(
        label='Shift',
        widget=forms.Select(
            attrs={
                'style': 'width: 200px; height: 50px;',
                "id": "drpdw_S6_Shift",
            },
            choices=models.S6_ShiftSetUp.SHIFT_CHOICES,
        ),
    )
    Date_start = forms.DateTimeField(
        label='Date Start',
        widget=MyDateTimeInput(
            attrs={
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
                "id": "datetime_S6_Date_start",
            }
        )
    )
    Date_end = forms.DateTimeField(
        label='Date End',
        widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
                "id": "datetime_S6_Date_end",
            }
        )
    )


    class Meta:
        model = models.S6_ShiftSetUp
        fields = [
            'SC',
            'Daily_Events',
            'Date_Shift',
            'Shift',
            'Date_start',
            'Date_end',
        ]
    #end class
#end class










class S6_Operator_form(forms.ModelForm):
    ToUberlog = forms.CharField(
        label='Batch',
        widget=forms.Select(
            attrs={
                "id": "drpdw_S6_ToUberlog",
            },
            choices=models.S6_Operator.ToUberlog_CHOICES,
        ),
    )
    Uberlog_ID      = forms.CharField(
        required=False,
        label='Uberlog Username',
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "id": "str_S6_Uberlog_ID",
            }
        )
    )
    Uberlog_Paswd   = forms.CharField(
        required=False,
        label='Uberlog Password',
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "id": "str_S6_Uberlog_Paswd",
            }
        )
    )

    class Meta:
        model = models.S6_Operator
        fields = [
            'ToUberlog',
            'Uberlog_ID',
            'Uberlog_Paswd',
        ]
    #end class
#end class