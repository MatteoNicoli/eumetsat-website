const navSlide = () => {
    const bntMenu = document.querySelector('.btn-menu');
    
    
    
    bntMenu.addEventListener('click',()=> {
        // Toggle Nav
        nav.classList.toggle('nav-active');
    
        // Animate Links
        navLinks.forEach((link,index) => {
            if(link.style.animation){
                link.style.animation = '';
            } else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index/5 + 0.5}s`;
            }  
        });
        // Bnt Menu animation
        bntMenu.classList.toggle('toggle');
    });

}

navSlide();