"""EUMETSAT_controller_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    
    path("select2/", include("django_select2.urls")),

    path('EPS1GenManager/', include('EPS1GenManager.urls')),
    path('EPS2GenManager/', include('EPS2GenManager.urls')),
    path('S3Manager/', include('S3Manager.urls')),
    path('S6Manager/', include('S6Manager.urls')),
    path('GEMSManager/', include('GEMSManager.urls')),
    path('Maps/', include('Maps.urls')),
] 

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
