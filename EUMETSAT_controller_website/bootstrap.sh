#!/usr/bin/env bash
set -e

on_error_exit() {
    echo "ERROR: Command \"${BASH_COMMAND}\" at ${BASH_SOURCE} line ${BASH_LINENO} failed with exit code $?." >&2
}
trap on_error_exit ERR

cd "$(dirname "$0")"

# echo ">>> Removing old environment"
# if [ ! -d env ]; then
#     rm -rf env
# fi
echo ">>> Installing django into new virtualenv"
virtualenv env -p python3.8
env/bin/pip install --upgrade pip setuptools wheel
env/bin/pip install -r pip_req.txt
env/bin/conda install -c conda-forge orekit
env/bin/pip install -e ../
