# -*- coding: utf-8 -*-
"""
Created on Tue Nov  2 18:19:30 2021

@author: Nicoli
"""

# -*- coding: utf-8 -*-
"""
Created on Sat May 22 16:03:56 2021

@author: Nicoli
"""
import os, sys

DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))



OS_UserName = os.getlogin()
UserName = OS_UserName
DesktopPath = os.path.join("C:", os.sep,"Users", OS_UserName, "Desktop")

import matplotlib.path as mpath
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
import matplotlib.colors as mcolors
plt.close('all')

import cartopy.crs as ccrs
from cartopy.feature.nightshade import Nightshade

# =============================================================================
# 
# =============================================================================
from static.FDF_Orekit.Orekit_function import *
from static.py.Sup_fcns import *




# =============================================================================
# 
# =============================================================================
Path_SupFile_List_LogBookIDs = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','LogBookID.csv')
if (os.path.isfile(Path_SupFile_List_LogBookIDs) == True):
    List_LogBookIDs = pd.read_csv(Path_SupFile_List_LogBookIDs, index_col=0)
#end if

Path_SupFile_List_ESOC_IDs = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','ESOC_antenna_ID.csv')
if (os.path.isfile(Path_SupFile_List_ESOC_IDs) == True):
    List_ESOC_IDs = pd.read_csv(Path_SupFile_List_ESOC_IDs, index_col=0)
#end if


# =============================================================================
# 
# =============================================================================
Datetime_UTC = datetime.datetime.utcnow()
Mission = 'EPS'

Sh = 'M'
DateS = datetime.datetime(2021, 11, 2, 11, 00 )
DateE = datetime.datetime(2021, 11, 2, 20, 0, 0 )

#
Date = datetime.datetime(2021, 10, 30, 0, 0, 0 )
Date = datetime.datetime(Datetime_UTC.year, Datetime_UTC.month, Datetime_UTC.day, 0, 0, 0 )
Sh = 'M'
hour_shift, hour_shift_2 = DateSolar( Date )
DateS, DateE = Date2Shift(Mission, Date, Sh, hour_shift, hour_shift_2)

#
Shift_Info = Date_Shift( Mission, Datetime_UTC )
Sh = Shift_Info[0]
DateS = Shift_Info[1]
DateE = Shift_Info[2]

# =============================================================================
# 
# =============================================================================
SetUp = {
        'Daily_Events': True,
        'Date_Shift': datetime.date(2021, 10, 12),
        'Shift': Sh,
        'Date_start': DateS,
        'Date_end': DateE,
    }

ToUberlog = {
    'ToUberlog': 'csv',
    'Uberlog_ID': 'MatteoN',
    'Uberlog_Paswd': 'elog1',
    }
        


# =============================================================================
# 
# =============================================================================
DS = SetUp['Date_start']
DE = SetUp['Date_end']


# =============================================================================
# 
# =============================================================================
FDF_AntLoc_Tot = AntenneInfo()

#
SC = 'METOP-B'

Antenna = 'CDA1'
utc, earth, inertialFrame, station_frame, ElMask = Frames( FDF_AntLoc_Tot, Antenna)
FDF_event_S6_FBK, propagator = FDF_PassGenerator(Antenna, SC, DS, DE, utc, inertialFrame, station_frame, ElMask )






# =============================================================================
# 
# =============================================================================
extrapDate = AbsoluteDate(DS.year, DS.month, DS.day, DS.hour, DS.minute, 00.000, utc)
final_date = AbsoluteDate(DE.year, DE.month, DE.day, DE.hour, DE.minute, 00.000, utc)

el_mask=[]
az_mask=[]
el_5=[]
az_5=[]
el_fire=[]
az_fire=[]

el=[]
az =[]
cmap_vet=[]
long=[]
lat=[]
pos=[]


while (extrapDate.compareTo(final_date) <= 0.0):  
    pv = propagator.getPVCoordinates(extrapDate, inertialFrame)
    pos_tmp = pv.getPosition()
    pos.append((pos_tmp.getX(),pos_tmp.getY(),pos_tmp.getZ()))
    
    el_tmp = station_frame.getElevation(pv.getPosition(),
                    inertialFrame,
                    extrapDate)*180.0/np.pi
    az_tmp = station_frame.getAzimuth(pv.getPosition(),
                    inertialFrame,
                    extrapDate)*180.0/np.pi
    
    long_tmp  = earth.transform(
                    pv.getPosition(),
                    inertialFrame,
                    extrapDate).getLongitude()*180.0/np.pi
    lat_tmp  = earth.transform(
                    pv.getPosition(),
                    inertialFrame,
                    extrapDate).getLatitude()*180.0/np.pi
    
    el.append(el_tmp)
    az.append(az_tmp)
    if (el_tmp>=5):
        cmap_vet.append('#00FF00')
    else:
        cmap_vet.append('#FFD700')
    lat.append(lat_tmp)
    long.append(long_tmp)

    
    #print extrapDate, pos_tmp, vel_tmp
    extrapDate = extrapDate.shiftedBy(1.0)
    
    









c
cmap, norm = mcolors.from_levels_and_colors([el[0]]+el, cmap_vet)

fig, ax1 = plt.subplots()

ax1.plot(az, el, cmap=cmap)
# =============================================================================
# ax1.plot(az_fire, el_fire, color='#00FF00')
# =============================================================================
ax1.plot( FDF_AntLoc_Tot.loc[Antenna,'Az_M'], FDF_AntLoc_Tot.loc[Antenna,'El_M'], color='#A0A0A0')

ax1.axhline(5, color='k', linestyle='--')

ax1.set_ylim(0,90)
#axs[0].title('Azimuth')

ax1.set_xlabel('$Az$ - []')
ax1.set_ylabel('$El$ - []')
ax1.grid(True)
plt.show()






fig = plt.figure()
ax = plt.axes(projection=ccrs.PlateCarree())

ax.plot(np.array(az_5)+180,np.array(el_5), transform=ccrs.Geodetic(), color='#FFD700')
ax.plot(np.array(az_fire)+180,np.array(el_fire), transform=ccrs.Geodetic(), color='#00FF00')
ax.plot( np.array(FDF_AntLoc_Tot.loc[Antenna,'Az_M'])+180, np.array(FDF_AntLoc_Tot.loc[Antenna,'El_M']), transform=ccrs.Geodetic(), color='#A0A0A0')

# =============================================================================
# ax.set_xlabel('$Az$ - []')
# ax.set_ylabel('$El$ - []')
# =============================================================================
ax.set_xlabel('$Longitude$ - []')
ax.set_xticks(range(-180, 180+10, 15))
ax.set_ylabel('$Latitude$ - []')
ax.set_yticks(range(-90, 90+10, 15))
ax.grid(True)
#ax.gridlines(draw_labels=True, dms=True, x_inline=False, y_inline=False)

plt.show()







fig = plt.figure()
ax2 = fig.add_axes([0.1, 0.1, 0.8, 0.8], polar=True)
ax2.set_theta_zero_location('N')
ax2.set_theta_direction(-1)

ax2.plot(np.deg2rad(np.array(az_5)),90-np.array(el_5), color='#FFD700')
ax2.plot(np.deg2rad(np.array(az_fire)),90-np.array(el_fire), color='#00FF00')
# =============================================================================
# ax2.plot( np.deg2rad(np.array(FDF_AntLoc.loc[Antenna,'Az_M'])), 90-np.array(FDF_AntLoc.loc[Antenna,'El_M']), color='#A0A0A0')
# =============================================================================

ax2.set_yticks(range(0, 90+10, 10))
ax2.set_ylim(top=90,bottom=-0)
yLabel = ['90', '', '', '60', '', '', '30', '', '', '0']
ax2.set_yticklabels(yLabel)
ax2.grid(True)

plt.show()









fname = os.path.join('H:', os.sep, 'DesktopW10', 'MyDocument', 'MyFiles', 'Orekit_FDF', 'files', 'Earth.jpg' )


fig = plt.figure()
ax3 = plt.axes(projection=ccrs.PlateCarree())


img = plt.imread(fname)
#ax3.imshow(img, origin='upper', transform=ccrs.PlateCarree(), extent=[-180, 180, -90, 90])
ax3.coastlines()

ax3.plot(long, lat, transform=ccrs.Geodetic(), alpha=0.6, color='red', zorder=3);

ax3.stock_img()
ax3.add_feature(Nightshade(Datetime_UTC, alpha=0.5))

# =============================================================================
# ax3.set_xlabel('$Longitude$ - []')
# ax3.set_xticks(range(-180, 180+10, 15))
# ax3.set_ylabel('$Latitude$ - []')
# ax3.set_yticks(range(-90, 90+10, 15))
# ax3.grid(True)
# =============================================================================
gl = ax3.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, dms=True, x_inline=False, y_inline=False)
# =============================================================================
# gl.top_labels = False
# gl.righr_labels = False
# ax3.set_xlabel('Longitude')
# =============================================================================
gl.righr_labels = False

plt.show()