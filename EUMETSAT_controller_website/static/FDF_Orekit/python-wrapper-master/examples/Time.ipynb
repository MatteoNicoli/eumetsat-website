{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This text is almost in whole based on the text made by Luc at the [orekit webpage](https://www.orekit.org/site-orekit-10.1/architecture/time.html)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Populating the interactive namespace from numpy and matplotlib\n"
     ]
    }
   ],
   "source": [
    "# First initiate notebook parameters\n",
    "%pylab inline\n",
    "figsize(10,10)\n",
    "\n",
    "#initialize orekit and JVM\n",
    "import orekit\n",
    "orekit.initVM()\n",
    "\n",
    "from orekit.pyhelpers import setup_orekit_curdir\n",
    "setup_orekit_curdir()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The \"Time\" package is an independent package providing classes to handle epochs and time scales, and to compare instants.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Time Presentation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The principal class is *AbsoluteDate* which represents a unique instant in time, so as to be able to locate it with respect to the many different times scales in use in the space dynamics and astronomy fields.\n",
    "This greatly simplifies development as it hides some models internals. For example when using JPL-based ephemerides, time must be in Terrestrial Time (formerly known as Ephemeris Time). However, this is an implementation detail and someone calling Orekit from a high level application should not have to deal with it. The *AbsoluteDate* class allows users to pass a date regardless of the time scale it was defined in, conversions will be done as required transparently."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time Scales"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dates are commonly defined by specifying a point in a specific time scale. For example, the J2000.0 epoch is defined from its calendar components as 2000-01-01T12:00:00 in Terrestrial Time. It is of prime importance to understand the various available time scales definitions to avoid mistakes. The TimeScalesFactory class provides several predefined time scales:\n",
    "\n",
    "* [**International Atomic Time**](https://en.wikipedia.org/wiki/International_Atomic_Time), TAI this is the most accurate and regular time scale that can be used at the surface of the Earth.\n",
    "* **Terrestrial Time** as defined by IAU (1991) recommendation IV. Coordinate time at the surface of the Earth. It is the successor of Ephemeris Time TE. By convention, TT = TAI + 32.184 s.\n",
    "* **Universal Time Coordinate**. UTC is mainly related to TAI, but some step adjustments are introduced from time to time to take into account Earth rotation irregularities and to prevent the legal time from drifting with respect to day and night. The International Earth Rotation Service (IERS) is in charge of this time-keeping. These adjustments require introduction of leap seconds, which means some days are not 86400 seconds long.\n",
    "* **Universal Time 1**. UT1 is a time scale directly linked to the actual rotation of the Earth. It is an irregular scale, reflecting Earth's irregular rotation rate. The offset between UT1 and UTCScale is found in the Earth Orientation Parameters published by IERS.\n",
    "* **Geocentric Coordinate Time**. Coordinate time at the center of mass of the Earth. This time scale depends linearly on TTScale (and hence on TAI),\n",
    "* **Barycentric Dynamic Time**. Time used to compute ephemerides in the solar system. This time is offset with respect to TT by small relativistic corrections due to Earth motion,\n",
    "* **Barycentric Coordinate Time**. Coordinate time used for computations in the solar system. This time scale depends linearly on TDBScale.\n",
    "* **Global Positioning System reference scale**. This scale was equal to UTC at start of the GPS Epoch when UTC was 19 seconds behind TAI, and has stayed parallel to TAI since then (i.e. UTC is now offset from GPS due to leap seconds). TGPS = TAI - 19 s.\n",
    "* **Greenwich Mean Sidereal Time scale**. The Greenwich Mean Sidereal Time is the hour angle between the meridian of Greenwich and mean equinox of date at 0h UT1.\n",
    "\n",
    "Orekit supports both the linear models of UTC-TAI offsets used between 1961 and 1972 and the constant models with only whole seconds offsets (and leap seconds) used since 1972. See example in end of notebook for the evolution of offset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Date Definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are three main ways to define a date:\n",
    "\n",
    "* using a location in a time scale as a set of calendar and hourly components\n",
    "* using a location in a time scale as an apparent seconds offset since an origin\n",
    "* using an elapsed physical duration since a reference date\n",
    "\n",
    "The first option is the more straightforward one, but is not sufficient for some needs. The two last options are confusingly similar, because of the complexity of time scales. Understanding the differences between the two is key to avoiding large errors.\n",
    "\n",
    "\n",
    "An apparent seconds offset is the difference between two readings on a clock synchronized with a time scale. If for example the first reading is 23:59:59 and the second reading is 00:00:00, the apparent seconds offset is 1 second. An elapsed duration is the count of seconds that could be measured by a stop watch started at the first instant and stopped at the second instant. Most of the time, both times are identical. However, if the time scale is UTC and if the readings are made when a leap second is introduced, then the elapsed time between the two events is 2 seconds and not 1 second!\n",
    "\n",
    "The method offsetFrom, which takes both a date and a time scale as parameters, computes the apparent offset. The durationFrom method which takes only a date as parameter computes the elapsed duration. In the example above, the first method would return 1 second while the second method would return 2 seconds:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from org.orekit.time import TimeScalesFactory, AbsoluteDate"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "utc = TimeScalesFactory.getUTC()\n",
    "start = AbsoluteDate(2005, 12, 31, 23, 59, 59.0, utc)\n",
    "stop  = AbsoluteDate(2006,  1,  1,  0,  0,  0.0, utc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.0\n",
      "2.0\n"
     ]
    }
   ],
   "source": [
    "print (stop.offsetFrom(start, utc))\n",
    "print (stop.durationFrom(start))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This property is used in reverse to define dates. We can define the second instant as the instant which will occur when the reading of the clock is one second away for the reading of the first date. We can also define it as the instant occurring when two seconds have elapsed since the first instant, without any reference to an external clock. Both approaches are possible, it depends on the available data and its meaning. The following code shows both approaches:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2006-01-01T00:00:00.000 2006-01-01T00:00:00.000\n"
     ]
    }
   ],
   "source": [
    "utc = TimeScalesFactory.getUTC()\n",
    "referenceDate = AbsoluteDate(2005, 12, 31, 23, 59, 59.0, utc)\n",
    "date1         =  AbsoluteDate(referenceDate, 1.0, utc) \n",
    "date2         =  AbsoluteDate(referenceDate, 2.0)\n",
    "\n",
    "print (date1, date2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The two variables date1 and date2 represent the same instant. The first one has been defined relative to a time scale, the second one has been defined independently of any time scale.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reference Epochs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Orekit defines a number of reference epochs. The first 6 are commonly used in the space community, the seventh one is commonly used in the computer science field and the last two are convenient for initialization in min/max research loops"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Julian Epoch: -4712-01-01 at 12:00:00, TTScale\n",
    "* Modified Julian Epoch: 1858-11-17 at 00:00:00, TTScale\n",
    "* Fifties Epoch: 1950-01-01 at 00:00:00, TTScale\n",
    "* CCSDS Epoch: 1958-01-01 at 00:00:00, TAIScale\n",
    "* Galileo Epoch: 1999-08-22 at 00:00:00, UTCScale\n",
    "* GPS Epoch: 1980-01-06 at 00:00:00, UTCScale\n",
    "* J2000 Epoch: 2000-01-01 at 12:00:00, TTScale\n",
    "* Java Epoch: 1970-01-01 at 00:00:00, TTScale\n",
    "* Past infinity Epoch: at infinity in the past\n",
    "* Future Epoch: at infinity in the future"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These can be accessed from the AbsoluteDate class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "TTScale 1858-11-17T00:00:00.000\n",
      "UTC     1858-11-16T23:59:27.816\n"
     ]
    }
   ],
   "source": [
    "tt = TimeScalesFactory.getTT()\n",
    "mjd = AbsoluteDate.MODIFIED_JULIAN_EPOCH\n",
    "\n",
    "print ('TTScale', mjd.toString(tt))\n",
    "print ('UTC    ', mjd.toString(utc))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Orekit supports both the linear models of UTC-TAI offsets used between 1961 and 1972 and the constant models with only whole seconds offsets (and leap seconds) used since 1972. The example below shows the offset in seconds between TAI and UTC:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAmoAAAJRCAYAAAD1b5sRAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+j8jraAAAgAElEQVR4nO3debRcZZnv8e8joEiYURkCkeBF6ADJQU4YbBVQlIhILtK0EAdAuUgvB+y+HZUbG3Dg2gLd9rKdQFTERmVQJkUmFdELNAQ7YAggg9qGRAkBkgAy5rl/1E6ohHOq6gxV9Z5T389atU7tvav2fs5rHfLzrb33E5mJJEmSyvOibhcgSZKkgRnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEIZ1CRJkgq1brcLGExE/B5YATwHPJuZ/d2tSJIkqbOKDWqV/TPzoW4XIUmS1A1+9SlJklSokoNaAldHxK0RcVy3i5EkSeq0kr/6/OvMXBQRrwCuiYi7MvP6VRur8HYcwIQJE/bYeeedu1WnJElSy2699daHMvPlrbw2xkKvz4g4BXgsM88YaHt/f3/OnTu3s0VJkiQNQ0Tc2upFkkV+9RkREyJio1XPgbcA87tblSRJUmeV+tXnlsDFEQG1Gr+bmVd2tyRJkqTOKjKoZeb9wLRu1yFJktRNRQY1SZJK8cwzz7Bw4UKefPLJbpeiMWb99ddn2223Zb311hv2PgxqkiQ1sHDhQjbaaCO23357qlNypKYyk6VLl7Jw4UImT5487P0UeTGBJEmlePLJJ9liiy0MaRqSiGCLLbYY8UysQU2SpCYMaRqO0fjcGNQkSRqjzjnnHBYtWjQq+5o9eza77LILs2fPZsmSJey1117svvvu/PKXvxzSfi655BIWLFgw5G0DOfXUU+nr66Ovr4911lln9fMvfvGLAJxwwglMnDiRlStXrn7POeecw4c+9KEX7OvPf/4zBx98MNOmTWPKlCkcdNBBQ/q9VjnllFM444wBb+vaFp6jJknSGHXOOeew6667ss0224x4X2eddRYPP/ww66yzDt///vfZbbfdOPvss4e8n0suuYSDDz6YKVOmDGnbQObMmcOcOXMA2HDDDZk3b97qbStXruTiiy9mu+224/rrr2e//fZruK+TTjqJN7/5zZxwwgkA3H777S3+Rt3ljJokSQX7/e9/z6677rp6+YwzzuCUU07hoosuYu7cubzrXe+ir6+Pv/zlL9xyyy289rWvZdq0aey5556sWLFijX1lJrNnz2bXXXdlt9124/zzzwfgkEMO4bHHHmOPPfbg85//PB/72Me49NJL6evr4/HHH+foo49e/Z4vfOELANx3333MmDGDPfbYg9e//vXcdddd3HDDDVx22WXMnj2bvr4+7rvvvtXHHmjbvHnz2HvvvZk6dSqHHnoojzzySMvj8vOf/5xdd92Vv/u7v+N73/te09cvXryYbbfddvXy1KlTVz8/7bTT2G233Zg2bRqf+MQnAPj617/O9OnTmTZtGocddhhPPPHEC/Y50BiMuswc84899tgjJUlqhwULFnT1+L/73e9yl112Wb18+umn58knn5yZmfvuu2/ecsstmZn51FNP5eTJk/Pmm2/OzMxly5blM888s8a+LrroojzggAPy2WefzT/96U+53Xbb5aJFizIzc8KECatf961vfSs/+MEPZmbm3Llz84ADDli97ZFHHsnMzDe+8Y3529/+NjMzb7rpptx///0zM/Ooo47KCy+8cMDfZe1tu+22W1533XWZmflP//RPecIJJww6DvX1ZWa+//3vz3PPPTeXLVuW22yzTT799NMvqL3elVdemZtssknut99++dnPfjYfeOCBzMy84oorcp999snHH388MzOXLl2amZkPPfTQ6vfOmTMnv/jFL2Zm5sknn5ynn356wzGoN9DnB5ibLWYcv/qUJKlFn7r8DhYsWj6q+5yyzcac/PZdRryfu+++m6233prp06cDsPHGG7/gNb/61a848sgjWWedddhyyy3Zd999ueWWWzjkkEMG3e8OO+zA/fffz4c//GHe9ra38Za3vIXHHnuMG264gcMPP3z165566qkh1bts2TIeffRR9t13XwCOOuqoNfbXyNNPP80VV1zBF77wBTbaaCP22msvrr76at72trcN+p4DDzyQ+++/nyuvvJKf/OQn7L777syfP59rr72WY445hg022ACAzTffHID58+fzyU9+kkcffZTHHnuMAw88cI39jcYYtMKgJklSwdZdd901TpYf7HYPmdn0KsPaZM7QbLbZZtx2221cddVVfO1rX+OCCy7g3/7t39h0003XOGesk6688kqWLVvGbrvtBsATTzzBBhts0DCoQS2EzZo1i1mzZnHwwQdz/fXXDzpuRx99NJdccgnTpk3jnHPO4brrrltj+8qVKzsyBgY1SZJaNBozX0O15ZZb8uCDD7J06VI23HBDfvSjHzFjxgwANtpoo9Xnoe28884sWrSIW265henTp7NixQpe+tKXsu66z/9T/4Y3vIEzzzyTo446iocffpjrr7+e008/veHxH3roIV784hdz2GGHsdNOO/Hud7+bjTfemMmTJ3PhhRdy+OGHk5ncfvvtTJs2bY2a1la/bZNNNmGzzTbjl7/8Ja9//ev5zne+s3p2rZnvfe97nH322Rx55JEAPP7440yePHnA88hW+dnPfsbee+/NBhtswIoVK7jvvvuYNGkSEyZM4NOf/jSzZs1igw024OGHH2bzzTdnxYoVbL311jzzzDOcd955TJw4cY39NRqD0eTFBJIkFWy99dbjpJNOYs899+SAAw5g5513Xr3t6KOP5vjjj6evr4/nnnuO888/nw9/+MNMmzaNN7/5zS+YfTv00EOZOnUq06ZN441vfCOnnXYaW221VcPjP/DAA+y333709fXx7ne/m8997nMAnHfeeXzjG99g2rRp7LLLLlx66aUAHHHEEZx++unsvvvua1xMMNC2b3/728yePZupU6cyb948TjrppKbj8cQTT3DVVVetMXs2YcIEXve613H55ZcP+r5bb72V/v5+pk6dyj777MOxxx7L9OnTmTFjBocccgj9/f309fWtvvXGZz7zGfbaay/++q//eo0xrzfYGIymGM40aGn6+/tz7ty53S5DkjQO3XnnnfzVX/1Vt8vQGDXQ5ycibs3M/lbe74yaJElSoQxqkiRJhTKoSZIkFcqgJklSE+PhfG513mh8bgxqkiQ1sP7667N06VLDmoYkM1m6dCnrr7/+iPbjfdQkSWpg2223ZeHChSxZsqTbpWiMWX/99dfoLzocBjVJkhpYb731mDx5crfLUI/yq09JkqRCGdQkSZIKZVCTJEkqlEFNkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEIZ1CRJkgplUJMkSSqUQU2SJKlQBjVJkqRCGdQkSZIKZVCTJEkqlEFNkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEIZ1CRJkgplUJMkSSqUQU2SJKlQBjVJkqRCGdQkSZIKZVCTJEkqlEFNkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVDFBrWImBERd0fEvRHxiW7XI0mS1GlFBrWIWAf4MvBWYApwZERM6W5VkiRJnVVkUAP2BO7NzPsz82ng+8DMLtckSZLUUet2u4BBTAT+WLe8ENirS7UA8KnL72DBouUdP+7MvonM2mtSx48rSZK6r9QZtRhgXa7xgojjImJuRMxdsmRJh8rqrAWLl3PpvAe6XYYkSeqSUmfUFgLb1S1vCyyqf0FmngWcBdDf379GiGuHk9++S7sP8QLvPPPGjh9TkiSVo9QZtVuAHSNickS8GDgCuKzLNUmSJHVUkTNqmflsRHwIuApYB/hmZt7R5bIkSZI6qsigBpCZVwBXdLsOSZKkbin1q09JkqSeZ1CTJEkqlEFNkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEIZ1CRJkgplUJMkSSqUQU2SJKlQxfb6VM2Cxct555k3Drp9Zt9EZu01qYMVSZKkTjGoFWxm38SG2xcsXg5gUJMkaZwyqBVs1l6TGoawRjNtkiRp7PMcNUmSpEIZ1CRJkgrlV59jnBcbSJI0fhnUxjAvNpAkaXwzqI1hXmwgSdL45jlqkiRJhTKoSZIkFcqvPse5ZhcbgBccSJJUKoPaONbsYgPwggNJkkpmUBvHml1sAF5wIElSyTxHTZIkqVAGNUmSpEIZ1CRJkgrlOWqyDZUkSYUyqPU421BJklQug1qPsw2VJEnl8hw1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEIZ1CRJkgrl7TnUlDfElSSpOwxqasgb4kqS1D0GNTXkDXElSeoez1GTJEkqlEFNkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEJ5HzWNWLPOBWD3AkmShsOgphFp1rkA7F4gSdJwGdQ0Is06F4DdCyRJGi7PUZMkSSqUQU2SJKlQxQW1iDglIh6IiHnV46Bu1yRJktQNpZ6j9oXMPKPbRUiSJHVTcTNqkiRJqik1qH0oIm6PiG9GxGbdLkaSJKkbuhLUIuLaiJg/wGMm8FXgVUAfsBj4l0H2cVxEzI2IuUuWLOlg9ZIkSZ3RlXPUMvOAVl4XEV8HfjTIPs4CzgLo7+/P0atOkiSpDMVdTBARW2fm4mrxUGB+N+vR6GjWZsoWU5IkvVBxQQ04LSL6gAR+D3ygu+VopJq1mbLFlCRJAysuqGXme7pdg0ZXszZTtpiSJGlgpV71KUmS1PMMapIkSYUyqEmSJBXKoCZJklQog5okSVKhDGqSJEmFMqhJkiQVyqAmSZJUKIOaJElSoYrrTKDeZC9QSZJeyKCmrrMXqCRJAzOoqevsBSpJ0sA8R02SJKlQBjVJkqRCGdQkSZIKZVCTJEkqlEFNkiSpUAY1SZKkQhnUJEmSCuV91DQmNOtcAHYvkCSNPwY1Fa9Z5wKwe4EkaXwyqKl4zToXgN0LJEnjk+eoSZIkFcqgJkmSVCiDmiRJUqEMapIkSYUyqEmSJBXKoCZJklQog5okSVKhDGqSJEmF8oa3GjeatZmyxZQkaawxqGlcaNZmyhZTkqSxyKCmcaFZmylbTEmSxiLPUZMkSSqUQU2SJKlQBjVJkqRCGdQkSZIKZVCTJEkqlEFNkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpSdCdQz7AUqSRprDGrqCfYClSSNRQY19QR7gUqSxiLPUZMkSSqUQU2SJKlQBjVJkqRCGdQkSZIKZVCTJEkqlEFNkiSpUF0JahFxeETcERErI6J/rW0nRsS9EXF3RBzYjfokSZJK0DCoRcSLIuK1bTjufOAdwPVrHW8KcASwCzAD+EpErNOG40uSJBWvYVDLzJXAl0f7oJl5Z2bePcCmmcD3M/OpzPwdcC+w52gfX5IkaSxopTPBTyPiMOCHmZltrmcicFPd8sJqndR2zXqBgv1AJUmd1UpQ+wDwD8CzEfEkEEBm5saN3hQR1wJbDbBpTmZeOtjbBlg3YDiMiOOA4wAmTfIfTo1Ms16gYD9QSVLnNQ1qmbnRcHacmQcM420Lge3qlrcFFg2y/7OAswD6+/vbPdOnca5ZL1CwH6gkqfNaasoeEZsBOwLrr1qXmdcP/o5huwz4bkT8K7BNdcyb23AcSZKk4jUNahFxLHACtdmtecDewI3AG4d70Ig4FPh34OXAjyNiXmYemJl3RMQFwALgWeCDmfnccI8jSZI0lrVyH7UTgOnAHzJzf2B34NGRHDQzL87MbTPzJZm5ZWYeWLft1Mx8VWbulJk/GclxJEmSxrJWgtqTmfkkQES8JDPvAnZqb1mSJElq5Ry1hRGxKXAJcE1EPAL8ob1lSZIkqZWrPg+tnp4SET8HNgGubGtVkiRJavmqz9cBO2bmtyLi5dRuQvu7tlYmSZLU45qeoxYRJwMfB06sVq0H/Ec7i5IkSVJrFxMcChwCPA6QmYuAYd0EV5IkSa1rJag9XfX4TICImNDekiRJkgStBbULIuJMYNOI+F/AtcDX21uWJEmSWrnq84yIeDOwnNr9007KzGvaXpkkSVKPa6WF1IeA8wxnkiRJndXKV59bAbdExAURMSMiot1FSZIkqYWglpmfBHYEvgEcDdwTEf83Il7V5tokSZJ6WiszalRXff6pejwLbAZcFBGntbE2SZKkntbKOWofAY4CHgLOBmZn5jMR8SLgHuBj7S1RkiSpN7XSQuplwDsyc41G7Jm5MiIObk9ZkiRJauX2HCc12Hbn6JYjSZKkVVo6R02SJEmdN2hQi4iXdLIQSZIkranRjNqNABHxnQ7VIkmSpDqNzlF7cUTMAl4bEe9Ye2Nm/rB9ZUllWrB4Oe8888ZBt8/sm8isvSZ1sCJJ0njWKKgdD7wL2BR4+1rbEjCoqafM7JvYcPuCxcsBDGqSpFEzaFDLzF8Bv4qIuZn5jQ7WJBVp1l6TGoawRjNtkiQNRyv3UftOddPbN1TLvwC+lpnPtK8sSZIktRLUvgKsV/0EeA/wVeDYdhUlSZKk1oLa9MycVrf8s4i4rV0FSZIkqaaVG94+FxGvWrUQETsAz7WvJEmSJEFrM2qzgZ9HxP1AAK8EjmlrVZIkSWqp1+dPI2JHYCdqQe2uzHyq7ZVJkiT1uFZm1KiC2e1trkWSJEl1bMouSZJUKIOaJElSoVr66jMiJlK7iGD16zPz+nYVJY1V9gKVJI2mpkEtIj4PvBNYwPO35UjAoCbVsReoJGm0tTKj9j+BnbzSU2rMXqCSpNHWyjlq91NrISVJkqQOamVG7QlgXkT8FFg9q5aZH2lbVZIkSWopqF1WPSRJktRBrXQm+HYnCpEkSdKaWrnqc0fgc8AUYP1V6zNzhzbWJUmS1PNauZjgW8BXgWeB/YFzgf9oZ1GSJElqLai9NDN/CkRm/iEzTwHe1t6yJEmS1MrFBE9GxIuAeyLiQ8ADwIbtLUuSJEmtzKh9FNgA+AiwB/Ae4Kh2FiVJkqTWrvq8pXr6GHBMe8uRxrdmvUDBfqCSpOcNGtQi4nJqPT0H8hRwH/DlzPxjOwqTxptmvUDBfqCSpDU1mlE7o8n7dgEuAPYZ1YqkcapZL1CwH6gkaU2DBrXM/EWT9/40IqaOcj2SJEmqtHIxwaAy89jRKkSSJElrGlFQkyRJUvs0DWoRcXgr6yRJkjS6WplRO7HFdZIkSRpFjW7P8VbgIGBiRHyxbtPG1Pp+SpIkqY0azagtAuYCTwK31j0uAw4cyUEj4vCIuCMiVkZEf9367SPiLxExr3p8bSTHkSRJGssa3Z7jNuC2iPhuZj4DEBGbAdtl5iMjPO584B3AmQNsuy8z+0a4f0mSpDGvlabs10TEIdVrbwUejIgbMvPvh3vQzLwTICKGuwtJkqRxr5WgtklmLo+IY4FzM/PkiLi9jTVNjoj/ApYDn8zMX7bxWFJxmvUDtReoJPWOVoLauhGxNfC3wJxWdxwR1wJbDbBpTmZeOsjbFgOTMnNpROwBXBIRu2Tm8gH2fxxwHMCkSf6jpfGhWT9Qe4FKUm9pJah9GrgK+FVm3hIROwD3NHtTZh4w1GIy8ylqDd/JzFsj4j7g1dQualj7tWcBZwH09/cP1jxeGlOa9QO1F6gk9ZamQS0zLwQurFu+HzisHcVExMuBhzPzuSoQ7gjc345jSZIklW5YLaQi4qSRHDQiDo2IhcA+wI8j4qpq0xuA2yPiNuAi4PjMfHgkx5IkSRqrWvnqcyDHUvtKdFgy82Lg4gHW/wD4wXD3K0mSNJ406kzwghP4V20CXtqeciRJkrRKoxm1R4HpmfnntTdExB/bV5IkSZKg8Tlq5wKvHGTbd9tQiyRJkuo0aiH1yQbbPt6eciRJkrTKsK76lCRJUvsZ1CRJkgo13NtzSOoSe4FKUu9oOqMWEd9pZZ2k9pvZN5EpW2886PYFi5dz6bwHOliRJKmdWplR26V+ISLWAfZoTzmSGrEXqCT1lkFn1CLixIhYAUyNiOXVYwXwIHBpxyqUJEnqUYMGtcz8XGZuBJyemRtXj40yc4vMPLGDNUqSJPWkpl99ZuaJEbEZsCOwft3669tZmCRJUq9rGtQi4ljgBGBbYB6wN3Aj8Mb2liZJktTbWrmP2gnAdOAPmbk/sDu1PqCSJElqo1aC2pOZ+SRARLwkM+8CdmpvWZIkSWrl9hwLI2JT4BLgmoh4BPhDe8uSJElSKxcTHFo9PSUifg5sAlzZ1qokSZI0tBZSmfmLdhUiSZKkNdnrUxpn7AUqSeOHQU0aR2b2TWy4fcHi5QAGNUkaI1q5j9oE4C+ZuTIiXg3sDPwkM59pe3WShsReoJI0vrRye47rgfUjYiJwNfAe4Jx2FiVJkqTWglpk5hPAO4CvZObhwJT2liVJkqSWglpE7AO8C/hxtc5z2yRJktqslaD2UeBE4OLMvCMidgB+3t6yJEmS1MoNb38B/KJu+X7gI+0sSpIkSQ2CWkRcDuRg2zPzkLZUJEmSJKDxjNoZ1c93AFsB/1EtHwn8uZ1FSZIkqUFQW9UuKiL+JTP76zZdHhFz216ZJElSj2vlYoIJ1QUEAETEZGBC+0qSJEkStHabjb8HrouI+6vl7YEPtK0iSW3VrBco2A9UkkrRylWfV0bEjtRaRwHclZlPtbcsSe3QrBco2A9UkkrS6o1r96A2k7YuMC0iyMxz21aVpLZo1gsU7AcqSSVppSn7d4BXAfOA56rVCRjUJEmS2qiVGbV+YEpmDnpPNUmSJI2+Vq76nE/tPmqSJEnqoFZm1F4GLIiIm4HVFxHYmUCSJKm9Wglqp7S7CEmSJL1QS03ZI2JLYHq16ubMfLC9ZUmSJKnpOWoR8bfAzcDhwN8C/xkRf9PuwiRJknpdK199zgGmr5pFi4iXA9cCF7WzMEmSpF7XylWfL1rrq86lLb5PkiRJI9DKjNqVEXEV8L1q+Z3AT9pXkqRua6UfaCP2CpWk0dHKxQSzI+IdwOuAAM7KzIvbXpmkrmilH2gj9gqVpNHTSgupycAVmfnDavmlEbF9Zv6+3cVJ6rxW+oE2Yq9QSRo9rZxrdiGwsm75uWqdJEmS2qiVoLZuZj69aqF6/uL2lSRJkiRoLagtiYjV7aIiYibwUPtKkiRJErR21efxwHkR8WUggYXAe9talSRJklq66vM+YO+I2BCIzFzR/rIkSZLUSgupLSPiG8CFmbkiIqZExPs7UJskSVJPa+UctXOAq4BtquXfAh8dyUEj4vSIuCsibo+IiyNi07ptJ0bEvRFxd0QcOJLjSJIkjWWtBLWXZeYFVLfoyMxnqd2iYySuAXbNzKnUgt+JABExBTgC2AWYAXwlItYZ4bEkSZLGpFaC2uMRsQW1CwmIiL2BZSM5aGZeXQU+gJuAbavnM4HvZ+ZTmfk74F5gz5EcS5Ikaaxq5arPfwAuA14VEf8PeDnwN6NYw/uA86vnE6kFt1UWVuskjSGt9Aq1H6gkNdfKVZ+/joh9gZ2o9fq8OzOfafa+iLgW2GqATXMy89LqNXOAZ4HzVr1toBIG2f9xwHEAkyb5H3upFK30CrUfqCS1ppVen4cDV2bmHRHxSeA1EfHZzPx1o/dl5gFN9nsUcDDwpsxcFcYWAtvVvWxbYNEg+z8LOAugv79/wDAnqfNa6RVqP1BJak0r56j9U3VbjtcBbwK+AXx1JAeNiBnAx4FDMvOJuk2XAUdExEuqZvA7AjeP5FiSJEljVStBbdUVnm8Dvp6ZP2bkvT6/BGwEXBMR8yLiawCZeQdwAbAAuBL4YGaO9ApTSZKkMamViwkeiIgzgQOAz0fES2gt4A0qM/9Hg22nAqeOZP+SJEnjQSuB62+p3fB2RmY+CmwOzG5rVZIkSWrpqs8ngB/WLS8GFrezKEmSJI3wK0xJkiS1j0FNkiSpUAY1SZKkQjUNahHxjoi4JyKWRcTyiFgREcs7UZwkSVIva+X2HKcBb8/MO9tdjKTe0awfqL1AJam1oPZnQ5qk0dSsH6i9QCWpppWgNjcizgcuAZ5atTIzfzj4WyRpcM36gdoLVJJqWglqGwNPAG+pW5fU3VtNkiRJo6+VG94e04lCJEmStKZBg1pEfCwzT4uIf6c2g7aGzPxIWyuTJEnqcY1m1FZdQDC3E4VIkiRpTYMGtcy8vPr57c6VI0mSpFXsTCBJklQog5okSVKhDGqSJEmFGjSoRcRpEXH8AOs/EBGfb29ZkiRJanTV55uAjw+w/uvA7YNsk6RR0awXaCvsFypprGsU1F6SmQPdP21lREQba5LU45r1Am2F/UIljQeNgtpfImLHzLynfmVE7Aj8pb1lSeplzXqBtsJ+oZLGg0ZB7STgJxHxWeDWal0/cCLw0XYXJkmS1OsaBbVHgJnAx4APV+vmA4dl5m/aXZgkSVKvaxTUvpKZrwGO6lQxkiRJep73UZMkSSpUoxm1HSLissE2ZuYhbahHkiRJlUZBbQnwL50qRJIkSWtqFNQey8xfdKwSSZIkraHROWq/61gVkiRJeoFGM2rnRcQ76pYTeAiYl5kr2luWJEmSGgW1gwdYtzkwNSLen5k/a1NNkjQqRtov1F6hkrpt0KCWmccMtD4iXglcAOzVrqIkaaRG2i/UXqGSStBoRm1AmfmHiFivHcVI0mgZab9Qe4VKKsGQb3gbETsBT7WhFkmSJNUZdEYtIi6ndgFBvc2BrYF3t7MoSZIkNf7q84y1lhNYCtyTmU+3ryRJkiRB44sJvNmtJElSF9mUXZIkqVAGNUmSpEINKahFxGvaVYgkSZLWNNQZtbPbUoUkSZJeYKhBLdpShSRJkl5gqEHtU22pQpIkSS8wpBZSmXlJuwqRpNKMtKk72Nhd0sgMudenJPWCkTZ1Bxu7Sxo5g5okDWCkTd3Bxu6SRq5Rr8/NG70xMx8e/XIkSZK0SqMZtVup9fcc6ErPBHZoS0WSJEkCGvf6nNzJQiRJkrSmoXYmeFVEzImI+e0qSJIkSTVNg1pEbB0RH42Im4E7qM3CHdn2yiRJknrcoEEtIv5XRPwM+AXwMuBYYHFmfrXuYFcAABGNSURBVCozf9OpAiVJknpVo4sJvgzcCMzKzLkAEZGjcdCIOB14O/A0cB9wTGY+GhHbA3cCd1cvvSkzjx+NY0qSJI01jb763Bb4PvCvEXF3RHwGWG+UjnsNsGtmTgV+C5xYt+2+zOyrHoY0SZLUsxoFtSsz86uZ+QbgTcAy4MGIuDMi/u9IDpqZV2fms9XiTdRCoSRJkuo0+upz9f3TMnMhcAZwRkS8mtG9mOB9wPl1y5Mj4r+A5cAnM/OXo3gsSeqoZv1C7QUqqZFGQe3lEfEPg2xb0WzHEXEtsNUAm+Zk5qXVa+YAzwLnVdsWA5Myc2lE7AFcEhG7ZObyAfZ/HHAcwKRJ/kdOUnma9Qu1F6ikZhoFtXWADRm8M0FDmXlAo+0RcRRwMPCmzMzqPU8BT1XPb42I+4BXA3MH2P9ZwFkA/f39o3KRgySNpmb9Qu0FKqmZRkFtcWZ+uh0HjYgZwMeBfTPzibr1LwcezsznImIHYEfg/nbUIEmSVLqWzlFrgy8BLwGuiQh4/jYcbwA+HRHPAs8Bx9v8XZIk9apGQe1N7TpoZv6PQdb/APhBu44rSZI0lgx6ew5nsiRJkrprSE3ZJUmS1DkGNUmSpEIZ1CRJkgplUJMkSSqUQU2SJKlQjW7PIUlqs2a9QFthv1Bp/DKoSVKXNOsF2gr7hUrjm0FNkrqkWS/QVtgvVBrfPEdNkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEIZ1CRJkgplUJMkSSqUQU2SJKlQdiaQpDGuWb9Qe4FKY5dBTZLGsGb9Qu0FKo1tBjVJGsOa9Qu1F6g0tnmOmiRJUqEMapIkSYUyqEmSJBXKoCZJklQog5okSVKhDGqSJEmFMqhJkiQVyqAmSZJUKIOaJElSoexMIEnjXLNeoGA/UKlUBjVJGsea9QIF+4FKJTOoSdI41qwXKNgPVCqZ56hJkiQVyqAmSZJUKIOaJElSoQxqkiRJhTKoSZIkFcqgJkmSVCiDmiRJUqEMapIkSYUyqEmSJBXKzgSSpKb9QO0FKnWHQU2SelyzfqD2ApW6x6AmST2uWT9Qe4FK3eM5apIkSYUyqEmSJBXKoCZJklQog5okSVKhDGqSJEmFMqhJkiQVqmtBLSI+ExG3R8S8iLg6Irap1kdEfDEi7q22v6ZbNUqSJHVTN2fUTs/MqZnZB/wIOKla/1Zgx+pxHPDVLtUnSZLUVV0Lapm5vG5xApDV85nAuVlzE7BpRGzd8QIlSZK6rKudCSLiVOC9wDJg/2r1ROCPdS9bWK1b3NnqJEmrNOsF2oy9QqXhaeuMWkRcGxHzB3jMBMjMOZm5HXAe8KFVbxtgV7n2iog4LiLmRsTcJUuWtO+XkKQeN7NvIlO23njY71+weDmXzntgFCuSekdbZ9Qy84AWX/pd4MfAydRm0Lar27YtsGiAfZ8FnAXQ39//giAnSRodzXqBNmOvUGn4unnV5451i4cAd1XPLwPeW139uTewLDP92lOSJPWcbp6j9s8RsROwEvgDcHy1/grgIOBe4AngmO6UJ0mS1F1dC2qZedgg6xP4YIfLkSRJKo6dCSRJkgplUJMkSSqUQU2SJKlQBjVJkqRCGdQkSZIKZVCTJEkqVFd7fUqSekMrvULtByq9kEFNktRWM/smNn3NgsXLAQxq0loMapKktmqlV6j9QKWBeY6aJElSoQxqkiRJhTKoSZIkFcqgJkmSVCiDmiRJUqEMapIkSYUyqEmSJBXKoCZJklQog5okSVKh7EwgSSpCK/1AxwN7mmooDGqSpK5rpR/oeGBPUw2VQU2S1HWt9AMdD3phxlCjy3PUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEIZ1CRJkgplUJMkSSqUQU2SJKlQBjVJkqRCGdQkSZIKZWcCSZI6qBM9Te0nOn4Y1CRJ6pBO9DS1n+j4YlCTJKlDOtHT1H6i44vnqEmSJBXKoCZJklQog5okSVKhDGqSJEmFMqhJkiQVyqAmSZJUKIOaJElSoQxqkiRJhTKoSZIkFcrOBJIkjTOd6CfajP1GR4dBTZKkcaQT/USbsd/o6DGoSZI0jnSin2gz3Z7NG088R02SJKlQBjVJkqRCGdQkSZIKZVCTJEkqlEFNkiSpUAY1SZKkQnUlqEXEZyLi9oiYFxFXR8Q21fr9ImJZtX5eRJzUjfokSZJK0K0ZtdMzc2pm9gE/AuoD2S8zs696fLpL9UmSJHVdV4JaZi6vW5wAZDfqkCRJKlnXOhNExKnAe4FlwP51m/aJiNuARcA/ZuYd3ahPkiQNXyf6jfZCP9G2zahFxLURMX+Ax0yAzJyTmdsB5wEfqt72a+CVmTkN+Hfgkgb7Py4i5kbE3CVLlrTr15AkSUM0s28iU7beuK3HWLB4OZfOe6CtxyhBZHb3W8eIeCXw48zcdYBtvwf6M/OhRvvo7+/PuXPntqlCSZJUmlWzded/YJ8uVzJ0EXFrZva38tpuXfW5Y93iIcBd1fqtIiKq53tSq29p5yuUJEnqvm6do/bPEbETsBL4A3B8tf5vgL+LiGeBvwBHZLen/CRJkrqkK0EtMw8bZP2XgC91uBxJkqQi2ZlAkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEJ1rdenJEnSSDTrJzoeeoEa1CRJ0pgzs29iw+0LFi8HMKhJkiR12qy9JjUMYY1m2sYSz1GTJEkqlEFNkiSpUAY1SZKkQhnUJEmSCmVQkyRJKpRBTZIkqVAGNUmSpEIZ1CRJkgplUJMkSSqUQU2SJKlQtpCSJEnjUrOm7QOZss3GnPz2XdpU0dAZ1CRJ0rjTrGn7WGFQkyRJ406zpu1jheeoSZIkFcqgJkmSVCiDmiRJUqEMapIkSYUyqEmSJBXKoCZJklQog5okSVKhDGqSJEmFMqhJkiQVyqAmSZJUKIOaJElSoQxqkiRJhTKoSZIkFcqgJkmSVCiDmiRJUqEMapIkSYUyqEmSJBXKoCZJklQog5okSVKhDGqSJEmFMqhJkiQVyqAmSZJUKIOaJElSoQxqkiRJhTKoSZIkFcqgJkmSVCiDmiRJUqEMapIkSYUyqEmSJBXKoCZJklSorge1iPjHiMiIeFm1HBHxxYi4NyJuj4jXdLtGSZKkbuhqUIuI7YA3A/9dt/qtwI7V4zjgq10oTZIkqeu6PaP2BeBjQNatmwmcmzU3AZtGxNZdqU6SJKmLuhbUIuIQ4IHMvG2tTROBP9YtL6zWSZIk9ZR127nziLgW2GqATXOA/wO8ZaC3DbAuX/CiiOOofTUK8FhE3D3cOgvzMuChbhcxTjiWo8exHD2O5ehxLEePYzm6mo3nK1vdUWS+IAO1XUTsBvwUeKJatS2wCNgT+BRwXWZ+r3rt3cB+mbm444V2QUTMzcz+btcxHjiWo8exHD2O5ehxLEePYzm6RnM8u/LVZ2b+JjNfkZnbZ+b21L7efE1m/gm4DHhvdfXn3sCyXglpkiRJ9dr61ecwXQEcBNxLbcbtmO6WI0mS1B1FBLVqVm3V8wQ+2L1quu6sbhcwjjiWo8exHD2O5ehxLEePYzm6Rm08u3KOmiRJkprr9n3UJEmSNAiDWptFxDcj4sGImF+3blpE3BgRv4mIyyNi47ptU6ttd1Tb16/W71Et31u12BroNibj2lDGMiLeFRHz6h4rI6Kv2uZYDm0s14uIb1fr74yIE+veMyMi7q7G8hPd+F1KMMTxfHFEfKtaf1tE7Ff3np7+bEbEdhHx8+pzdkdEnFCt3zwiromIe6qfm1XrB205GBFHVa+/JyKO6tbv1E3DGM+dq8/sUxHxj2vtq6f/1ocxlu+qPpO3R8QNETGtbl9DG8vM9NHGB/AG4DXA/Lp1twD7Vs/fB3ymer4ucDswrVreAlinen4zsA+1+8z9BHhrt3+3ksdyrfftBtxft+xYDu1zOQv4fvV8A+D3wPbAOsB9wA7Ai4HbgCnd/t3GwHh+EPhW9fwVwK3Ai6rlnv5sAltTuwMAwEbAb4EpwGnAJ6r1nwA+Xz0/qBqnAPYG/rNavzlwf/Vzs+r5Zt3+/cbAeL4CmA6cCvxj3X56/m99GGP52lWfOWqtMVd9Noc8ls6otVlmXg88vNbqnYDrq+fXAIdVz98C3J5Vt4bMXJqZz0WthdbGmXlj1v6XPhf4n+2vvixDHMt6RwKr7svnWDLksUxgQkSsC7wUeBpYTu2+h/dm5v2Z+TTwfWot4HrOEMdzCrX7SJKZDwKPAv1+NiEzF2fmr6vnK4A7qXWmmQl8u3rZt3l+XAZrOXggcE1mPpyZj1Ab/xkd/FWKMNTxzMwHM/MW4Jm1dtXzf+vDGMsbqs8ewE3U7hcLwxhLg1p3zAcOqZ4fDmxXPX81kBFxVUT8OiI+Vq2fSO1ec6vYVut5g41lvXdSBTUcy0YGG8uLgMeBxcB/A2dk5sPY7q2ZwcbzNmBmRKwbEZOBPaptfjbrRMT2wO7AfwJbZnU/zernK6qXDfYZ9LO5lhbHczCOZ51hjOX7qc38wjDG0qDWHe8DPhgRt1KbQn26Wr8u8DrgXdXPQyPiTbTYVqtHDTaWAETEXsATmbnq3CHHcnCDjeWewHPANsBk4H9HxA44ls0MNp7fpPYf57nAvwE3AM/ieK4WERsCPwA+mpnLG710gHXZYH1PGsJ4DrqLAdb15HgOdSwjYn9qQe3jq1YN8LKGY1nEfdR6TWbeRdXnNCJeDbyt2rQQ+EVmPlRtu4LaeS//wfPTpvB8y62e12AsVzmC52fToDbGjuUAGozlLODKzHwGeDAi/h/QT+3/FdbPYDqWdQYbz8x8Fvj7Va+LiBuAe4BH8LNJRKxH7R/C8zLzh9XqP0fE1pm5uPpq88Fq/UIG/gwuBPZba/117ay7VEMcz8EMNs49ZahjGRFTgbOpnWu6tFo95LF0Rq0LIuIV1c8XAZ8EvlZtugqYGhEbVOcD7QssqKZTV0TE3tVVYO8FLu1C6cVpMJar1h1O7RwAYPXUtGM5gAZj+d/AG6sr7CZQO2n7Lmony+8YEZMj4sXUQvFlna+8TIONZ/X3PaF6/mbg2cz075zaVZzAN4A7M/Nf6zZdBqy6cvMonh+XwVoOXgW8JSI2q67Ce0u1rqcMYzwH0/N/60Mdy4iYBPwQeE9m/rbu9UMfy25fSTHeH9RmcxZTOzlzIbUp0BOoXTHyW+CfqW48XL3+3cAd1M5vOa1ufX+17j7gS/Xv6ZXHMMZyP+CmAfbjWA5hLIENgQurz+UCYHbdfg6qXn8fMKfbv9cYGc/tgbupnYx8LfDKuv309GeT2ikfSe3q93nV4yBqV8D/lNrM40+BzavXB/Dlarx+A/TX7et91FoR3gsc0+3fbYyM51bV53c5tYtcFlK7wKXn/9aHMZZnU5slX/XauXX7GtJY2plAkiSpUH71KUmSVCiDmiRJUqEMapIkSYUyqEmSJBXKoCZJklQog5okSVKhDGqSNEIRsU63a5A0PhnUJPWUiPhMRJxQt3xqRHwkImZHxC0RcXtEfKpu+yURcWtE3BERx9Wtfywi/iUibgP26fCvIalHGNQk9ZpvULV8qdo7HQH8GdiRWgP6PmCPiHhD9fr3ZeYe1LoGfCQitqjWTwD+MzOnZeavOvkLSOodNmWX1FMy8/cRsTQidge2BP4LmE6tH+R/VS/bkFpwu55aODu0Wr9dtX4p8By1Bs2S1DYGNUm96GzgaGq9Db8JvAn4XGaeWf+iiNgPOADYJzOfiIjrgPWrzU9m5nOdKlhSb/KrT0m96GJgBrWZtKuqx/siYkOAiJgYEa8ANgEeqULazsDe3SpYUm9yRk1Sz8nMpyPi58Cj1azY1RHxV8CNEQHwGPBu4Erg+Ii4E7gbuKlbNUvqTZGZ3a5Bkjqquojg18DhmXlPt+uRpMH41aeknhIRU4B7gZ8a0iSVzhk1SZKkQjmjJkmSVCiDmiRJUqEMapIkSYUyqEmSJBXKoCZJklQog5okSVKh/j+owy72BkRtCgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 720x720 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "utc = TimeScalesFactory.getUTC()\n",
    "tai = TimeScalesFactory.getTAI()\n",
    "\n",
    "timerange = range(1957,2019)\n",
    "\n",
    "utc_date = [AbsoluteDate(t, 1,  1, 0, 0, 0.0, utc) for t in timerange]\n",
    "tai_date = [AbsoluteDate(t, 1,  1, 0, 0, 0.0, tai) for t in timerange]\n",
    "\n",
    "diff_utc = [t1.durationFrom(t2) for t1, t2 in zip(tai_date, utc_date)]\n",
    "# diff_tt = [t1.durationFrom(t2) for t1, t2 in zip(tai_date, utc_date)]\n",
    "\n",
    "plot(timerange, diff_utc, label='utc offset to TAI Scale', drawstyle='steps-mid')\n",
    "\n",
    "legend()\n",
    "ylabel('TAI - UTC in seconds at 1st Jan of year'); ylim(-40,5)\n",
    "xlabel('year');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "    "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
