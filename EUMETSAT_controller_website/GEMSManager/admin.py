from django.contrib import admin

from . import models

# Register your models here.

admin.site.register(models.GEMS_SetUp)
admin.site.register(models.GEMS_Event)