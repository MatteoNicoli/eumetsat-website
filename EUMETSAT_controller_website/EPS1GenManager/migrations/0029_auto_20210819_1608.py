# Generated by Django 3.2.3 on 2021-08-19 14:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EPS1GenManager', '0028_alter_eps_1g_operator_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eps_1g_operator',
            name='email_Mission',
        ),
        migrations.AlterField(
            model_name='eps_1g_operator',
            name='Mission',
            field=models.CharField(default='EPS', max_length=120),
        ),
        migrations.AlterField(
            model_name='eps_1g_operator',
            name='email',
            field=models.EmailField(default='<django.db.models.fields.charfield>.<django.db.models.fields.charfield>@external.eumetsat.int', max_length=254),
        ),
    ]
