# =============================================================================
# MODULE - django
# =============================================================================
from django.conf import settings
from django.db import models

# =============================================================================
# MODULE - useful
# =============================================================================
import os, sys
DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')

import pandas as pd

# Create your models here.

class S3_Satellite(models.Model):
    path_SCInfo = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','SCInfo.csv')
    if (os.path.isfile(path_SCInfo) == True):
        list_SCIDs = pd.read_csv(path_SCInfo, )
    #end if
    Mission = "S3"
    list_SC = list_SCIDs[
        ( list_SCIDs['Mission'] == Mission )
        ]
    list_SCIDs_tmp = list_SCIDs[
        ( list_SCIDs['Mission'] == Mission ) &
        ( list_SCIDs['Comment'] <= 2 )
        ].drop(columns='Mission').drop(columns='NORAD CAT ID').drop(columns='shortcut').drop(columns='Comment')
    SC_CHOICES = tuple(list_SCIDs_tmp.itertuples(index=False, name=None))
    # SC_CHOICES = (
    #     ('SENTINEL 3A', 'Sentinel 3A'),
    #     ('SENTINEL 3B', 'Sentinel 3B'),
    # )
    name = models.CharField(max_length=50, choices=SC_CHOICES)
#end class











class Document(models.Model):
    docfile = models.FileField(upload_to='S3Manager/schedule/')
    name_str = models.CharField(max_length=70, blank=True, null=True)
    date_str = models.CharField(max_length=70, blank=True, null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    def filename(self):
        return os.path.basename(self.docfile.name)
#end class











class S3_ShiftSetUp(models.Model):
    SC_CHOICES = S3_Satellite.SC_CHOICES
    # SC              = models.CharField(max_length=7, choices=SC_CHOICES)
    SC = models.ManyToManyField(S3_Satellite)
    
    Daily_Events    = models.BooleanField(default=False)

    #
    Date_Shift      = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    SHIFT_CHOICES   = (
        ('M', 'Morning'),
        ('E', 'Evening'),
        ('N', 'Night'),
    )
    Shift           = models.CharField(max_length=7, choices=SHIFT_CHOICES)
    Date_start      = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    Date_end        = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
#end class











class S3_Pass(models.Model):
    inShift = models.BooleanField(default=False)
    nOrbit  = models.IntegerField()
    AOS0    = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    AOSM    = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    AOS5    = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    LOS5    = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    LOSM    = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    LOS0    = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    Antenna = models.CharField(max_length=4)
#end class











class S3_Operator(models.Model):
    Name            = models.CharField(max_length=120, blank=True, null=True)
    Second_Name     = models.CharField(max_length=120, blank=True, null=True)
    Surname         = models.CharField(max_length=120, blank=True, null=True)
    Second_Surname  = models.CharField(max_length=120, blank=True, null=True)
    Type_CHOICES = (
        ('controller', 'Controller'),
        ('analyst', 'Analyst'),
        ('engineer', 'Engineer'),
    )
    Type            = models.CharField(max_length=10, choices=Type_CHOICES)
    Mission         = models.CharField(max_length=120, default='S3')
    Company         = models.CharField(max_length=120, blank=True, null=True)
    email           = models.EmailField(max_length=254, default='@external.eumetsat.int')
    email_Team      = models.EmailField(max_length=254, default='Controller.S3@eumetsat.int')
    OS_Name         = models.CharField(max_length=120, blank=True, null=True)
    Uberlog_ID      = models.CharField(max_length=120, blank=True, null=True)
    Uberlog_Paswd   = models.CharField(max_length=120, default='elog')
    ToUberlog_CHOICES = (
        ('import', 'Import'),
        ('submit', 'Submit'),
        ('csv', 'Download (.csv)'),
    )
    ToUberlog       = models.CharField(max_length=7, choices=ToUberlog_CHOICES, default='import')

    def __str__(self):
        Name = str( self.Name ).capitalize()
        if ( self.Second_Name != None ):
            Name = Name + ' ' + str( self.Second_Name ).capitalize()
        #end if
        Surname = str( self.Surname ).capitalize()
        if ( self.Second_Surname != None ):
            Surname = Surname + ' ' + str( self.Second_Surname ).capitalize()
        #end if
        NS = Name + ' ' + Surname
        return str( self.Type ).capitalize() + ' - ' + NS
    #end def
#end class












































# class Book(models.Model):
#     author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
#     co_authors = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='co_authored_by')