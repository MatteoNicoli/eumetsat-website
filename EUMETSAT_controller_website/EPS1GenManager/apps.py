from django.apps import AppConfig


class EPS1GenManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'EPS1GenManager'
