# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 15:06:49 2021

@author: Nicoli
"""
import os, sys

DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))


#
OS_Name = os.name
OS_UserName = os.getlogin()
UserName = OS_UserName

if (OS_Name=='nt'):
    path = os.path.join( 'C:', os.sep, "Users", OS_UserName )
elif (OS_Name=='posix'):
    path = os.path.join( os.sep, "home", OS_UserName )
#end if
DesktopPath = os.path.join(path, "Desktop")

from multiprocessing.pool import ThreadPool

# =============================================================================
# 
# =============================================================================
from static.py.Sup_fcns import *
from S6_Sup import *
from S6_csv import *






# =============================================================================
# 
# =============================================================================


UserName = 'Atzeni'
filePath = os.path.join("D:", os.sep, "EPS System Ops")



# =============================================================================
# 
# =============================================================================
Path_SupFile_List_LogBookIDs = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','LogBookID.csv')
if (os.path.isfile(Path_SupFile_List_LogBookIDs) == True):
    List_LogBookIDs = pd.read_csv(Path_SupFile_List_LogBookIDs, index_col=0)
#end if

Path_SupFile_List_ESOC_IDs = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','ESOC_antenna_ID.csv')
if (os.path.isfile(Path_SupFile_List_ESOC_IDs) == True):
    List_ESOC_IDs = pd.read_csv(Path_SupFile_List_ESOC_IDs, index_col=0)
#end if



# =============================================================================
# 
# =============================================================================
Datetime_UTC = datetime.datetime.utcnow()
Mission= 'S6'

Sh = 'M'
DateS = datetime.datetime(2021, 11, 2, 11, 00 )
DateE = datetime.datetime(2021, 11, 2, 20, 0, 0 )

#
Date = datetime.datetime(2021, 12, 1, 0, 0, 0 )
# =============================================================================
# Date = datetime.datetime(Datetime_UTC.year, Datetime_UTC.month, Datetime_UTC.day, 0, 0, 0 )
# =============================================================================
Sh = 'E'
hour_shift, hour_shift_2 = DateSolar( Date )
DateS, DateE = Date2Shift( Mission, Date, Sh, hour_shift, hour_shift_2)

#
# =============================================================================
# Shift_Info = Date_Shift( Mission, Datetime_UTC )
# Sh = Shift_Info[0]
# DateS = Shift_Info[1]
# DateE = Shift_Info[2]
# 
# =============================================================================


# =============================================================================
# 
# =============================================================================
SetUp = {
        'SC': ['S6 MICHAEL FREILICH'],
        'Daily_Events': True,
        'Date_Shift': datetime.date(2021, 10, 12),
        'Shift': Sh,
        'Date_start': DateS,
        'Date_end': DateE,
    }

ToUberlog = {
    'ToUberlog': 'csv',
    'Uberlog_ID': 'MatteoN',
    'Uberlog_Paswd': 'elog1',
    }
        

# =============================================================================
# 
# =============================================================================
DS = SetUp['Date_start']
DE = SetUp['Date_end']

# =============================================================================
# 
# =============================================================================
#FDF_event_S6 = S6_FDF_events(SetUp)

# =============================================================================
# pool = ThreadPool(processes=1)
# def ThreadPool_S6_FDF_events(SetUp):
#     vm.attachCurrentThread()
#     return S6_FDF_events(SetUp)
# #end def
# 
# result = pool.apply_async(
#     ThreadPool_S6_FDF_events,
#     (SetUp,)
#     )
# FDF_event_S6 = result.get()
# 
# 
# # =============================================================================
# # 
# # =============================================================================
# Batch_info = Events2csv(fcn_S6_events_csv, FDF_event_S6, SetUp, ToUberlog, List_LogBookIDs )
# =============================================================================
FDF_event_S6 = S6_FDF_events_gen(
    DIR,DIR_int,
    SetUp,
    )

#

Batch_info = Events2csv(
    fcn_S6_events_csv,
    FDF_event_S6,
    SetUp,
    ToUberlog,
    List_LogBookIDs,
    )

# =============================================================================
# 
# =============================================================================
file_Path_Name_csv = os.path.join( DesktopPath, Batch_info['Name']+'.csv' )
Batch_csv = Batch_info['Data']
Batch_csv.to_csv (
    r''+file_Path_Name_csv,
    #path_or_buf = response,
    index = False,
    header = False,
    )
