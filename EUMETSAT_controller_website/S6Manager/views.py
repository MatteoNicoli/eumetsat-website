# =============================================================================
# MODULE - django
# =============================================================================
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponse
from django.utils import timezone
from django.views.generic import View

# models & forms
from . import models, forms

# =============================================================================
# MODULE - useful
# =============================================================================

import os, sys
DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))

import datetime

# =============================================================================
# MODULE - customized
# =============================================================================
from static.py.Sup_fcns import *
from .static.S6Manager.py.S6_Sup import *
from .static.S6Manager.py.S6_csv import *

# Create your views here.


def InitialData_elog():
    now_local =datetime.datetime.now()
    now_local = timezone.now()
    Mission = models.S6_Satellite.Mission
    Shift_Info = Date_Shift(Mission, now_local)
    Sh = Shift_Info[0]
    date_s = Shift_Info[1]
    date_e = Shift_Info[2]

    initial_data_ToUberlog = {
        'ToUberlog': 'import', 
    }
    initial_data_S6 = {
        'SC': ['S6 MICHAEL FREILICH'],
        'Daily_Events': True,
        'Date_Shift': date_s.strftime("%Y-%m-%d"),
        'Shift': Sh,
        'Date_start': date_s.strftime("%Y-%m-%dT%H:%M:%S"),
        'Date_end': date_e.strftime("%Y-%m-%dT%H:%M:%S"),
    }

    dict_initial_data = {
        'Mission': Mission,
        'ToUberlog': initial_data_ToUberlog,
        'S6': initial_data_S6,
    }
    return dict_initial_data
#end def

class S6Manager_view(View):
    

    


    # =============================================================================
    # 
    # =============================================================================
    def get(self, request):
        dict_InitialData = InitialData_elog()
        form_ToUberlog = forms.S6_Operator_form(initial=dict_InitialData['ToUberlog'])
        form_S6 = forms.S6_ShiftSetUp_form(initial=dict_InitialData['S6'])

        if ( request.is_ajax() ):
            S6_Date_Shift_val=request.GET.get('S6_Date_Shift_val')
            S6_Shift_val=request.GET.get('S6_Shift_val')
            if ( S6_Date_Shift_val != None and S6_Shift_val != None ):
                strdate_s, strdate_e = strDate_Shift(dict_InitialData['Mission'], S6_Date_Shift_val, S6_Shift_val )
                # 
                context_ajax = {
                    'date_s': strdate_s,
                    'date_e': strdate_e,
                }
                return JsonResponse(context_ajax, status=200)
            #end if    
        #end if
        
        # template & context
        template = 'S6Manager/S6Manager.html'
        context = {
            'contx_form_ToUberlog': form_ToUberlog,
            'contx_form_S6': form_S6,
            # 'contx_form': my_form,
        }
        return render(request, template, context=context)
    #end def

    
    
    
    
    
    # =============================================================================
    # 
    # =============================================================================
    def post(self, request):
        # dict_initial_data = ShiftGen_Initial_data()
        form_ToUberlog = forms.S6_Operator_form(request.POST or None)
        form_S6 = forms.S6_ShiftSetUp_form(request.POST or None)

        if ( form_S6.is_valid() and form_ToUberlog.is_valid() ):
            print(form_ToUberlog.cleaned_data)
            print(form_S6.cleaned_data)
            # =============================================================================
            # 
            # =============================================================================
            FDF_event_S6 = S6_FDF_events_gen(
                DIR,DIR_int,
                form_S6.cleaned_data,
                )

            #
            path = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv' )

            Path_SupFile_List_LogBookIDs = os.path.join(path,'LogBookID.csv')
            if (os.path.isfile(Path_SupFile_List_LogBookIDs) == True):
                List_LogBookIDs = pd.read_csv(Path_SupFile_List_LogBookIDs, index_col=0)
            #end if
            
            Batch_info = Events2csv(
                fcn_S6_events_csv,
                FDF_event_S6,
                form_S6.cleaned_data,
                form_ToUberlog.cleaned_data,
                List_LogBookIDs,
                )
            
            #
            if ( form_ToUberlog.cleaned_data['ToUberlog'] == 'csv' ):
                # Create the HttpResponse object with the appropriate CSV header.
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename='+Batch_info['Name']+'.csv'
                
                # Create the CSV writer using the HttpResponse as the "file"
                Batch_csv = Batch_info['Data']
                Batch_csv.to_csv (
                    #r''+file_Path_Name_csv,
                    path_or_buf = response,
                    index = False,
                    header = False,
                    )
                return response
                # return redirect('S6elogbooGenerator')
            #end if
        #end if
        # Redirect to THIS view, assuming it lives in 'some app'
        return redirect('S6elogbooGenerator')
    #end def
#end class