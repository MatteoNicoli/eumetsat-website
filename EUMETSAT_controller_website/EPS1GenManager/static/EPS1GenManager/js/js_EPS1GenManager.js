$(document).ready(function(){

    $("#ckbx_EPS1_SideGrndCon").click(function(){
        EPS1_SideGrndCon=false
        if($(this).is(":checked")) {
            EPS1_SideGrndCon=true
        }

        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_SideGrndCon_val: EPS1_SideGrndCon,
            },
            success: function(response){
                $("#ckbx_EPS1_NOAA_BOS").attr('checked', EPS1_SideGrndCon)
            },
        });
    });











    $("#drpdw_EPS1_ToUberlog").change(function(){
        var drpdw_EPS1_ToUberlog = $(this).val();
        EPS1_ToUberlog=false
        if( drpdw_EPS1_ToUberlog == 'csv' ) {
            EPS1_ToUberlog=true
        }

        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_ToUberlog_val: EPS1_ToUberlog,
            },
            success: function(response){
                $("#str_EPS1_Uberlog_ID").attr("disabled", EPS1_ToUberlog);
                $("#str_EPS1_Uberlog_Paswd").attr("disabled", EPS1_ToUberlog);
            },
        });
    });











    $("#date_EPS1_Date_Shift").change(function(){
        var EPS1_Date_Shift = $(this).val();
        var EPS1_Shift = $("#drpdw_EPS1_Shift").val();
        
        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_Date_Shift_val: EPS1_Date_Shift,
                EPS1_Shift_val: EPS1_Shift,
            },
            success: function(response){
                $("#datetime_EPS1_Date_start").val(response.date_s);
                $("#datetime_EPS1_Date_end").val(response.date_e);
            },
        });
    });











    $("#drpdw_EPS1_Shift").change(function(){
        var EPS1_Date_Shift = $("#date_EPS1_Date_Shift").val();
        var EPS1_Shift = $(this).val();

        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_Date_Shift_val: EPS1_Date_Shift,
                EPS1_Shift_val: EPS1_Shift,
            },
            success: function(response){
                $("#datetime_EPS1_Date_start").val(response.date_s);
                $("#datetime_EPS1_Date_end").val(response.date_e);
            },
        });
    });









    

    $("#drpdw_EPS1_CXTTCFBK_Antenna").change(function(){
        var EPS1_CXTTCFBK_Antenna = $(this).val();
        CXTTCFBK_CoVis=false
        if( EPS1_CXTTCFBK_Antenna != 'FBK_' && EPS1_CXTTCFBK_Antenna != 'WAL_' ) {
            CXTTCFBK_CoVis=true
        }

        CXTTCFBK_Ext=false
        if( EPS1_CXTTCFBK_Antenna == 'PAR_' ) {
            CXTTCFBK_Ext=true
        }

        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_CXTTCFBK_Antenna_val: EPS1_CXTTCFBK_Antenna,
            },
            success: function(response){
                $("#ckbx_EPS1_CXTTCFBK_Covis").attr('disabled', CXTTCFBK_CoVis);
                $("#ckbx_EPS1_CXTTCFBK_Extend").attr('disabled', CXTTCFBK_Ext);
            },
        });
    });

});