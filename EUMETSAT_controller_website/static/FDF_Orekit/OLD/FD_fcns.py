# -*- coding: utf-8 -*-
"""
Created on Sat May 15 00:53:48 2021

@author: Nicoli
"""
# =============================================================================
# 
# =============================================================================
import requests
import numpy as np
import pandas as pd


# =============================================================================
# 
# =============================================================================
def fcn_SCIDs():
    data = [
        ('EPS1', 'M02', 29499),
        ('EPS1', 'M01', 38771),
        ('EPS1', 'M03', 43689),
        ('EPS1', 'N19', 33591),
        ('EPS1', 'N18', 28654),
        ('EPS1', '', 41240),
        ('EPS1', '', 39086),
        ('S3', 'S3A', 41335),
        ('S3', 'S3B', 43437),
        ('S6', 'S6', 46984),
        ]
    data_index   = [
        'METOP-A',
        'METOP-B',
        'METOP-C',
        'NOAA 19',
        'NOAA 18',
        'JASON 3',
        'SARAL',
        'SENTINEL 3A',
        'SENTINEL 3B',
        'S6 MICHAEL FREILICH',
        ]
    data_columns = ['Mission', "shortcut","NORAD CAT ID"]
    list_SCIDs = pd.DataFrame(data, index=data_index, columns=data_columns )
    return list_SCIDs
#end def


def FDF_AntLoc(list_file):
    
    file_coordsband = list_file[0]
    fp=open(file_coordsband, 'r' )
    lines=fp.readlines()
    fp.close()
    
    Mission = list(file_coordsband.split("."))[-1].upper()
    
    
    Antennas_name = []
    Antennas_data = []
    
    for i_row in range(6,len(lines),2):
        X = float(lines[i_row][55:67])
        Y = float(lines[i_row][67:80])
        Z = float(lines[i_row][80:93])
        
        antenna = {'SITE_NAME': lines[i_row][10:15],
            'Provider': Mission,
            'X': X,
            'Y': Y,
            'Z': Z,
            'VX': float(lines[i_row+1][55:67]),
            'VY': float(lines[i_row+1][67:80]),
            'VZ': float(lines[i_row+1][80:93]),
            }
        Antennas_name.append(lines[i_row][32:36])
        Antennas_data.append(antenna)
    #end for
    
    df1 = pd.DataFrame(Antennas_data,index=Antennas_name)
    
    
    
    maskfile = list_file[1]
    fp=open(maskfile, 'r' )
    lines=fp.readlines()
    fp.close()

    
    
    nAnt = int((len(lines)-1)/361.0)    
    Antennas_name = []
    Antennas_data = []
    
    flag_newAnt = True
    Az_lst = []
    El_lst = []
    AzEl_List = []
    for i_row in range(1,len(lines)):
        
        if ( flag_newAnt == True ):
            nrow = int(lines[i_row][:4])
            irow = 0
            ncol = int(lines[i_row][4:7])
            
            Antennas_name.append( lines[i_row][11:15] )
            Az_lst = []
            El_lst = []
            
            flag_newAnt = False
        else:
            irow = irow + 1
            
            if ( ncol == 1 ):
                Az_lst.append( float( lines[i_row][:7] ) )
                El_lst.append( float( lines[i_row][7:] ) )
            if ( ncol == 2 ):
                1
                
            if ( irow == nrow ):
                flag_newAnt = True
                AzEl_List.append( [Az_lst,El_lst] )
                Az_lst = []
                El_lst = []
        
    
    
    df2 = pd.DataFrame(AzEl_List, columns = ['Az_M', 'El_M'], index=Antennas_name)
    
    result = pd.concat([df1, df2], axis=1)
    
    return result
#end def























def getEUM_TLE(scid):
    EUM_TLE = "https://service.eumetsat.int/tle/index.html"
    
    if (scid==2):
        EUM_TLE_M02 = "https://service.eumetsat.int/tle/data_out/latest_m02_tle.txt"
        EUM_TLE = EUM_TLE_M02
    elif (scid==1):
        EUM_TLE_M01 = "https://service.eumetsat.int/tle/data_out/latest_m01_tle.txt"
        EUM_TLE = EUM_TLE_M01
    elif (scid==3):
        EUM_TLE_M03 = "https://service.eumetsat.int/tle/data_out/latest_m03_tle.txt"
        EUM_TLE = EUM_TLE_M03
    elif (scid==19):
        EUM_TLE_N19 = "https://service.eumetsat.int/tle/data_out/latest_n19_tle.txt"
        EUM_TLE = EUM_TLE_N19
    elif (scid==18):
        EUM_TLE_N18 = "https://service.eumetsat.int/tle/data_out/latest_n18_tle.txt"
        EUM_TLE = EUM_TLE_N18
    #end if
    
    f = requests.get(EUM_TLE)
    TLE = f.text
    tle_line1 = TLE[:69]
    tle_line2 = TLE[70:]
    
    tle = [tle_line1,tle_line2]
    return tle
#end def











