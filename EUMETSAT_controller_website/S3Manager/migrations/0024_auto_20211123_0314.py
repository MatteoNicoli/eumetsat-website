# Generated by Django 3.2.5 on 2021-11-23 02:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('S3Manager', '0023_alter_document_docfile'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='date',
            field=models.CharField(blank=True, max_length=70, null=True),
        ),
        migrations.AddField(
            model_name='document',
            name='name',
            field=models.CharField(blank=True, max_length=70, null=True),
        ),
    ]
