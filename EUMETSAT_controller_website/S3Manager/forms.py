# =============================================================================
# MODULE - django
# =============================================================================
from django import forms
from easy_select2 import select2_modelform, Select2Multiple

# models & forms
from . import models

# Create your forms here.


class DocumentForm(forms.ModelForm):
    docfile = forms.FileField(
        label='Select a file',
        required=False,
        # help_text='max. 42 megabytes'
    )
    
    class Meta:
        model = models.Document
        fields = [
            'docfile',
        ]
    #end class
#end class

class MyDateTimeInput(forms.DateTimeInput):
    input_type = 'date'
#end class

class S3_ShiftSetUp_form(forms.ModelForm):
    SC = forms.MultipleChoiceField(
        label='S/C',
        widget=Select2Multiple(
            select2attrs={
                'width': '250px',
                "id": "drpdw_S3_SC",
                "placeholder": "Select operational S/C",
            },
        ),
        choices=models.S3_ShiftSetUp.SC_CHOICES,
    )
    Daily_Events = forms.BooleanField(
        label='Daily Events',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 30px;',
                "id": "ckbx_S3_Daily_Events",
            },
        ),
    )
    Date_Shift = forms.DateField(
        label='Date Shift',
        widget=forms.DateInput(
            attrs={
                'type': 'date',
                'class': 'mydjf_datetimefield',
                "id": "date_S3_Date_Shift",
            }
        )
    )
    Shift = forms.CharField(
        label='Shift',
        widget=forms.Select(
            attrs={
                'style': 'width: 200px; height: 50px;',
                "id": "drpdw_S3_Shift",
            },
            choices=models.S3_ShiftSetUp.SHIFT_CHOICES,
        ),
    )
    Date_start = forms.DateTimeField(
        label='Date Start',
        widget=MyDateTimeInput(
            attrs={
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
                "id": "datetime_S3_Date_start",
            }
        )
    )
    Date_end = forms.DateTimeField(
        label='Date End',
        widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
                "id": "datetime_S3_Date_end",
            }
        )
    )


    class Meta:
        model = models.S3_ShiftSetUp
        fields = [
            'SC',
            'Daily_Events',
            'Date_Shift',
            'Shift',
            'Date_start',
            'Date_end',
        ]
        exclude = []
    #end class
#end class










class S3_Operator_form(forms.ModelForm):
    ToUberlog = forms.CharField(
        label='Batch',
        widget=forms.Select(
            attrs={
                "id": "drpdw_S3_ToUberlog",
            },
            choices=models.S3_Operator.ToUberlog_CHOICES,
        ),
    )
    Uberlog_ID      = forms.CharField(
        required=False,
        label='Uberlog Username',
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "id": "str_S3_Uberlog_ID",
            }
        )
    )
    Uberlog_Paswd   = forms.CharField(
        required=False,
        label='Uberlog Password',
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "id": "str_S3_Uberlog_Paswd",
            }
        )
    )

    class Meta:
        model = models.S3_Operator
        fields = [
            'ToUberlog',
            'Uberlog_ID',
            'Uberlog_Paswd',
        ]
    #end class
#end class







































# class AuthorWidget(s2forms.ModelSelect2Widget):
#     search_fields = [
#         "username__icontains",
#         "email__icontains",
#     ]


# class CoAuthorsWidget(s2forms.ModelSelect2MultipleWidget):
#     search_fields = [
#         "username__icontains",
#         "email__icontains",
#     ]


# class BookForm(forms.ModelForm):
#     class Meta:
#         model = models.Book
#         fields = "__all__"
#         widgets = {
#             "author": AuthorWidget,
#             "co_authors": CoAuthorsWidget,
#         }


# class BaseAutocompleteSelect(s2forms.ModelSelect2Widget):
#     class Media:
#         js = ("admin/js/vendor/jquery/jquery.min.js",)

#     def __init__(self, **kwargs):
#         super().__init__(kwargs)
#         self.attrs = {"style": "width: 300px"}

#     def build_attrs(self, base_attrs, extra_attrs=None):
#         base_attrs = super().build_attrs(base_attrs, extra_attrs)
#         base_attrs.update(
#             {"data-minimum-input-length": 0, "data-placeholder": self.empty_label}
#         )
#         return base_attrs

# class BookAutocompleteWidget(BaseAutocompleteSelect):
#     empty_label = "-- select book --"
#     search_fields = ("name__icontains",)
#     queryset = models.Book.objects.filter(
#         type="Romance"
#     ).order_by("id")

# class BookForm(forms.Form): 
#     book = forms.ModelChoiceField(
#         label="Book: ",
#         widget=BookAutocompleteWidget,
#         queryset=models.Book.objects.filter(
#             type="Romance"
#         )
#     )