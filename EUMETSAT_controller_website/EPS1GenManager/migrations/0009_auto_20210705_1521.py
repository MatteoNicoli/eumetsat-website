# Generated by Django 3.2.4 on 2021-07-05 15:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EPS1GenManager', '0008_alter_eps_1g_shiftsetup_date_start'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eps_1g_operator',
            name='ToUberlog',
            field=models.CharField(choices=[('import', 'Import Batch'), ('submit', 'Submit Batch'), ('csv', 'Generate Batch (.csv)')], default='', max_length=7),
        ),
        migrations.AlterField(
            model_name='eps_1g_shiftsetup',
            name='Date_start',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
