# Generated by Django 3.2.3 on 2021-09-01 18:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EPS1GenManager', '0042_alter_eps_1g_shiftsetup_cxttcfbk_c_xttc_fbk_antenna'),
    ]

    operations = [
        migrations.AddField(
            model_name='eps_1g_pass',
            name='NORAD_ID',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='eps_1g_pass',
            name='SC_NAME',
            field=models.CharField(blank=True, max_length=4),
        ),
    ]
