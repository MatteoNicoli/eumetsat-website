from django.apps import AppConfig


class GemsmanagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'GEMSManager'
