# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 17:36:43 2021

@author: Nicoli
"""

import requests
import zipfile
import io
import csv
from os import path
import os


# https://dmtool.eumetsat.int/cs/idcplg?IdcService=EUM_GET_FILE&dRevLabel=1&dDocName=1203117&allowInterrupt=1

My_DOCNAME = '1203117'
My_HOSTNAME = 'dmtool.eumetsat.int'

url="https://{HostName}/idcws/GenericSoapPort?wsdl".format(HostName=My_HOSTNAME)

headers = {'content-type': 'text/xml','charset':'UTF-8'}

body="""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ucm="http://www.oracle.com/UCM">
    <soapenv:Header/>
    <soapenv:Body>
    <ucm:GenericRequest webKey="cs">
        <ucm:Service IdcService="EUM_GET_FILE">
            <ucm:Document>
                <!--Zero or more repetitions:-->
                <ucm:Field name="dRevLabel">1</ucm:Field>
                <ucm:Field name="dDocName">{DocName}</ucm:Field>
                <ucm:Field name="allowInterrupt">1</ucm:Field>
            </ucm:Document>
        </ucm:Service>
    </ucm:GenericRequest>
    </soapenv:Body>
</soapenv:Envelope>""".format(DocName=My_DOCNAME)



username = 'Nicoli'
# =============================================================================
# username = 'Matteo.Nicoli@external.eumetsat.int'
# =============================================================================
password = '7ek3i!@#$8oU'

response=requests.post(
    url,
    auth=(username, password),
    data=body,
    headers=headers,
    )






# =============================================================================
# os.chdir(<DIRECTORY_PATH_TO_SAVE_THE_FILE>)
# zf = zipfile.ZipFile(io.BytesIO(response.content), 'r')
# filename=<FILENAME>+'.csv'
# zipfilename='<ZIPFILENAME>'
# 
# 
# 
# csv_file = open(filename, 'w')
# writer = csv.writer(csv_file, dialect=csv.excel, delimiter=',',quotechar='"',lineterminator='\n', quoting=csv.QUOTE_MINIMAL)
# for fileinfo in zf.infolist():
#     x=zf.read(fileinfo).decode('utf-8')
#     mylist=x.split('\n')
# for i in range(0,len(mylist)):
#     row=mylist[i].strip('"')
#     if row.strip() == "":
#         continue
#      else:
#         rowlist=row.replace("\"",'').split(',')
#         writer.writerow(rowlist) 
# csv_file.close()
# print(filename+' File created')
# 
# # get the path to the file in the current directory
# src = path.realpath(filename);
# # rename the original file
# os.rename(src,filename)
# # now put things into a ZIP archive
# root_dir,tail = path.split(src)
# # more fine-grained control over ZIP files
# with zipfile.ZipFile(zipfilename, "w") as newzip:
#    newzip.write(filename)
#  print('Zip file created')
# 
# =============================================================================
