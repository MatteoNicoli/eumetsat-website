from django import forms
from . import models

# Create your forms here.











class GEMS_SetUp_form(forms.ModelForm):
    #
    Response = forms.MultipleChoiceField(
        label='Response',
        required=False,
        widget=forms.CheckboxSelectMultiple(
            attrs={
                "id": "Response",
            },
        ),
        choices=models.GEMS_SetUp.Response_CHOICES,
    )
    
    class Meta:
        model = models.GEMS_SetUp
        fields = [
            'Response',
        ]
    #end class
#end class