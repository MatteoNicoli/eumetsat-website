# -*- coding: utf-8 -*-
"""
Created on Thu Aug 26 17:30:29 2021

@author: Nicoli
"""

from Orekit_function import *


nowUTC = datetime.datetime.utcnow()

#
SC = 'S6 MICHAEL FREILICH'
Antenna = 'FBK_'
DateStart = nowUTC
DateEnd = nowUTC + datetime.timedelta(hours=24)


# =============================================================================
# 
# =============================================================================
FDF_AntLoc_Tot = AntenneInfo()
utc, inertialFrame, station_frame, ElMask = Frames( FDF_AntLoc_Tot, Antenna)

#
FDF_events_S3A = FDF_PassGenerator(Antenna, SC, DateStart, DateEnd, utc, inertialFrame, station_frame, ElMask )

# =============================================================================
# SC = 'SENTINEL 3B'
# FDF_events_S3B = FDF_PassGenerator(Antenna, SC, DateStart, DateEnd, utc, inertialFrame, station_frame, ElMask )
# =============================================================================

