from django.db import models

# Create your models here.


class GEMS_SetUp(models.Model):
    url         = models.URLField(max_length=200, default='')
    Logbook_CHOICES = (
        ('M', 'Morning'),
        ('E', 'Evening'),
        ('N', 'Night'),
    )
    Logbook     = models.CharField(max_length=7, choices=Logbook_CHOICES)
    Merge       = models.BooleanField(default=False)
    Response_CHOICES = (
        ('logged', 'Logged'),
        ('email', 'e-mail'),
        ('call', 'Call'),
    )
    Response    = models.CharField(max_length=7, choices=Response_CHOICES)
    Description = models.TextField(blank=True, null=True)
    AR          = models.CharField(max_length=120, blank=True, null=True)
    emailFrom   = models.EmailField(max_length=254, blank=True, null=True)
    emailTo     = models.CharField(max_length=120, blank=True, null=True)
    emailCC     = models.CharField(max_length=120, blank=True, null=True)
#end class



class GEMS_Event(models.Model):
    url         = models.URLField(max_length=200, blank=True, null=True)
    Facility    = models.CharField(max_length=7, blank=True, null=True)
    Severity_CHOICES = (
        ('A', 'Alarm'),
        ('W', 'Warning'),
        ('I', 'Information'),
    )
    Severity    = models.CharField(max_length=7, choices=Severity_CHOICES, blank=True)
    Date_ST     = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    Date_ET     = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    str_Events  = models.TextField(blank=True, null=True)
    
    Logbook     = models.CharField(max_length=7, blank=True, null=True)
    Merge       = models.BooleanField(default=False)
    Response    = models.CharField(max_length=7, blank=True, null=True)
    Description = models.TextField(blank=True, null=True)
    AR          = models.CharField(max_length=120, blank=True, null=True)
    emailFrom   = models.EmailField(max_length=254, blank=True, null=True)
    emailTo     = models.CharField(max_length=120, blank=True, null=True)
    emailCC     = models.CharField(max_length=120, blank=True, null=True)
#end class