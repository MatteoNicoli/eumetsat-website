# =============================================================================
# MODULE - django
# =============================================================================
from django.urls import path, include
from . import views

# Create your urls here.


urlpatterns = [
    path('', views.S3Manager_view.as_view(), name='S3Manager'),
    path('S3elogbooGenerator', views.S3Manager_view.as_view(), name='S3elogbooGenerator'),
]