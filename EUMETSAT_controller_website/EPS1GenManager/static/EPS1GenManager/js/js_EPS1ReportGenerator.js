$(document).ready(function(){
    $("#drpdw_EPS1_RG_report").change(function(){
        var drpdw_EPS1_Report = $(this).val();
        drpdw_EPS1_report_CW=true
        drpdw_EPS1_report_SH=true
        if( drpdw_EPS1_Report == 'CW' ) {
            drpdw_EPS1_report_CW=false
        } else if ( drpdw_EPS1_Report == 'SH' ) {
            drpdw_EPS1_report_SH=false
        }

        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_RG_Report_val: drpdw_EPS1_Report,
            },
            success: function(response){
                $("#int_EPS1_RG_year").attr("disabled", drpdw_EPS1_report_CW);
                $("#int_EPS1_RG_CW").attr("disabled", drpdw_EPS1_report_CW);
                $("#date_EPS1_RG_Date_Shift").attr("disabled", drpdw_EPS1_report_SH);
                $("#drpdw_EPS1_RG_Shift").attr("disabled", drpdw_EPS1_report_SH);
            },
        });
    });











    $("#date_EPS1_RG_Date_Shift").change(function(){
        var EPS1_Date_Shift = $(this).val();
        var EPS1_Shift = $("#drpdw_EPS1_RG_Shift").val();
        
        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_RG_Date_Shift_val: EPS1_Date_Shift,
                EPS1_RG_Shift_val: EPS1_Shift,
            },
            success: function(response){
                $("#datetime_EPS1_RG_Date_start").val(response.date_s);
                $("#datetime_EPS1_RG_Date_end").val(response.date_e);
            },
        });
    });











    $("#drpdw_EPS1_RG_Shift").change(function(){
        var EPS1_Date_Shift = $("#date_EPS1_RG_Date_Shift").val();
        var EPS1_Shift = $(this).val();

        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_RG_Date_Shift_val: EPS1_Date_Shift,
                EPS1_RG_Shift_val: EPS1_Shift,
            },
            success: function(response){
                $("#datetime_EPS1_RG_Date_start").val(response.date_s);
                $("#datetime_EPS1_RG_Date_end").val(response.date_e);
            },
        });
    });









    

    $("#int_EPS1_RG_year").change(function(){
        var EPS1_RG_year =$(this).val();
        var EPS1_RG_CW =$("#int_EPS1_RG_CW").val();

        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_RG_Year_val: EPS1_RG_year,
                EPS1_RG_CW_val: EPS1_RG_CW,
            },
            success: function(response){
                $("#datetime_EPS1_RG_Date_start").val(response.date_s);
                $("#datetime_EPS1_RG_Date_end").val(response.date_e);
            },
        });
    });











    $("#int_EPS1_RG_CW").change(function(){
        var EPS1_RG_year =$('#int_EPS1_RG_year').val();
        var EPS1_RG_CW =$(this).val();

        $.ajax({
            url: '',
            type: 'get',
            data: {
                EPS1_RG_Year_val: EPS1_RG_year,
                EPS1_RG_CW_val: EPS1_RG_CW,
            },
            success: function(response){
                $("#datetime_EPS1_RG_Date_start").val(response.date_s);
                $("#datetime_EPS1_RG_Date_end").val(response.date_e);
            },
        });
    });
});