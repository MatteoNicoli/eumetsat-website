# =============================================================================
# MODULE - django
# =============================================================================
from django import forms
from django.forms.widgets import Input, Widget

from easy_select2 import select2_modelform, Select2Multiple

# models & forms
from . import models

import datetime

# Create your forms here.











class MyDateTimeInput(forms.DateTimeInput):
    input_type = 'date'

class EPS_1G_ShiftSetUp_form(forms.ModelForm):
    SideSpaCon = forms.BooleanField(
        label='SpaCon',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 44px;',
                "id": "ckbx_EPS1_SideSpaCon",
            },
        ),
    )
    SideGrndCon = forms.BooleanField(
        label='GrndCon',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 30px;',
                "id": "ckbx_EPS1_SideGrndCon",
            },
        ),
    )
    Antenna = forms.CharField(
        label='MetOp Antenna',
        widget=forms.Select(
            attrs={
                'style': 'width: 200px;',
                "id": "drpdw_EPS1_Antenna",
            },
            choices=models.EPS_1G_ShiftSetUp.Antenna_CHOICES,
        ),
    )
    SC = forms.MultipleChoiceField(
        label='S/C',
        widget=Select2Multiple(
            select2attrs={
                'width': '300px',
                "id": "drpdw_EPS1_SC",
                "placeholder": "Select operational S/C",
            },
        ),
        choices=models.EPS_1G_ShiftSetUp.SC_CHOICES,
    )
    Daily_Events = forms.BooleanField(
        label='Daily Events',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 30px;',
                "id": "ckbx_EPS1_Daily_Events",
            },
        ),
    )
    NOAA_BOS = forms.BooleanField(
        label='NOAA BOS',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 47px;',
                "id": "ckbx_EPS1_NOAA_BOS",
            },
        ),
    )
    Date_Shift = forms.DateField(
        label='Date Shift',
        required=False,
        widget=forms.DateInput(
            attrs={
                'type': 'date',
                'class': 'mydjf_datetimefield',
                "id": "date_EPS1_Date_Shift",
            }
        )
    )
    Shift = forms.CharField(
        label='Shift',
        required=False,
        widget=forms.Select(
            attrs={
                'style': 'width: 200px; height: 50px;',
                "id": "drpdw_EPS1_Shift",
            },
            choices=models.EPS_1G_ShiftSetUp.SHIFT_CHOICES,
        ),
    )
    Date_start = forms.DateTimeField(
        label='Date Start',
        widget=MyDateTimeInput(
            attrs={
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
                "id": "datetime_EPS1_Date_start",
            },
        ),
    )
    Date_end = forms.DateTimeField(
        label='Date End',
        # input_formats=['%Y-%m-%d %H:%M:%S'],
        widget=forms.DateTimeInput(
            # format='%Y-%m-%d %H:%M:%S',
            attrs={
                # 'class':'datetimefield',
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
                "id": "datetime_EPS1_Date_end",
            },
        ),
    )
    file_report = forms.BooleanField(
        label='Report (.docx)',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 30px;',
            },
        ),
    )

    class Meta:
        model = models.EPS_1G_ShiftSetUp
        fields = [
            'SideSpaCon',
            'SideGrndCon',
            'Antenna',
            'SC',
            'Daily_Events',
            'NOAA_BOS',
            'Date_Shift',
            'Shift',
            'Date_start',
            'Date_end',
            'file_report',
        ]
    #end class

    # def clean_SideSpaCon(self, *args, **kwargs):
    #     SideSpaCon = self.cleaned_data.get('SideSpaCon')
    #     SideGrndCon = self.cleaned_data.get('SideGrndCon')
    #     print(SideSpaCon)
    #     print('ciao',SideGrndCon)
    #     if ( SideSpaCon == False):
    #         raise forms.ValidationError('Side not setted')
    #     #end if
    #     return SideSpaCon
    # #end def

    # def clean_C_XTTC_FBK_nOrbit(self, *args, **kwargs):
    #     C_XTTC_FBK_nOrbit = self.cleaned_data.get('C_XTTC_FBK_nOrbit')
    #     print(C_XTTC_FBK_nOrbit)
    #     if ( C_XTTC_FBK_nOrbit != None ):
    #         if ( C_XTTC_FBK_nOrbit <= 0 and C_XTTC_FBK_nOrbit >= 99999 ):
    #             # raise forms.ValidationError()
    #             raise forms.ValidationError({"C_XTTC_FBK_nOrbit": 'n Orbit out of range'})
    #         #end if
    #     #end if
    #     return C_XTTC_FBK_nOrbit
    # #end def



    def clean(self, *args, **kwargs):
        #
        SideSpaCon = self.cleaned_data.get('SideSpaCon')
        SideGrndCon = self.cleaned_data.get('SideGrndCon')
        print(SideSpaCon)
        print('ciao',SideGrndCon)
        if ( SideSpaCon == False and SideGrndCon == False ):
            raise forms.ValidationError({'SideSpaCon': 'Side not setted'})
        #end if

        # 
        C_XTTC_FBK_nOrbit = self.cleaned_data.get('C_XTTC_FBK_nOrbit')
        if ( C_XTTC_FBK_nOrbit != None ):
            if ( C_XTTC_FBK_nOrbit <= 0 and C_XTTC_FBK_nOrbit >= 99999 ):
                # raise forms.ValidationError()
                raise forms.ValidationError({"C_XTTC_FBK_nOrbit": 'n Orbit out of range'})
            #end if
        #end if
        # return cleaned_data
    #end def
#end class











class EPS_1G_ShiftSetUp_MS_form(forms.ModelForm):
    #
    MS_Date_start = forms.DateTimeField(
        label='Date Start',
        required=False,
        widget=MyDateTimeInput(
            attrs={
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
            }
        )
    )
    MS_Date_end = forms.DateTimeField(
        label='Date End',
        required=False,
        # input_formats=['%Y-%m-%d %H:%M:%S'],
        widget=forms.DateTimeInput(
            # format='%Y-%m-%d %H:%M:%S',
            attrs={
                # 'class':'datetimefield',
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
            }
        )
    )
    MS_Antenna = forms.CharField(
        label='New Antenna',
        required=False,
        widget=forms.Select(
            attrs={
                'style': 'width: 200px;',
                "id": "drpdw_EPS1_MS_Antenna",
            },
            choices=models.EPS_1G_ShiftSetUp_MS.MS_Antenna_CHOICES,
        ),
    )
    # MS_SC = forms.CharField(
    #     label='S/C',
    #     required=False,
    #     widget=forms.Select(
    #         attrs={
    #             'style': 'width: 200px;',
    #             "id": "drpdw_EPS1_MS_SC",
    #         },
    #         choices=models.EPS_1G_ShiftSetUp_MS.MS_SC_CHOICES,
    #     ),
    # )
    MS_SC = forms.MultipleChoiceField(
        label='S/C affected',
        widget=Select2Multiple(
            select2attrs={
                'width': '300px',
                "id": "drpdw_EPS1_MS_SC",
                "placeholder": "Select S/C",
            },
        ),
        choices=models.EPS_1G_ShiftSetUp.SC_CHOICES,
    )

    class Meta:
        model = models.EPS_1G_ShiftSetUp_MS
        fields = [
            'MS_Date_start',
            'MS_Date_end',
            'MS_Antenna',
            'MS_SC',
        ]
    #end class
#end class











class EPS_1G_ShiftSetUp_CXTTCFBK_form(forms.ModelForm):
    #
    C_XTTC_FBK_Antenna = forms.CharField(
        label='Antenna',
        required=False,
        widget=forms.Select(
            attrs={
                'style': 'width: 200px;',
                "id": "drpdw_EPS1_CXTTCFBK_Antenna",
            },
            choices=models.EPS_1G_ShiftSetUp_CXTTCFBK.C_XTTC_FBK_Antenna_CHOICES,
        ),
    )
    C_XTTC_FBK_Covis = forms.BooleanField(
        label='CoVis',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 44px;',
                "id": "ckbx_EPS1_CXTTCFBK_Covis",
            },
        ),
    )
    C_XTTC_FBK_Extend = forms.BooleanField(
        label='Extend Vis.',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'style': 'width: 44px;',
                "id": "ckbx_EPS1_CXTTCFBK_Extend",
            },
        ),
    )
    C_XTTC_FBK_SC = forms.CharField(
        label='S/C',
        required=False,
        widget=forms.Select(
            attrs={
                'style': 'width: 200px;',
            },
            choices=models.EPS_1G_ShiftSetUp_CXTTCFBK.C_XTTC_FBK_SC_CHOICES,
        ),
    )
    C_XTTC_FBK_nOrbit = forms.IntegerField(
        label='Orbit Revolution',
        required=False,
        widget=forms.NumberInput(
            attrs={
                "placeholder": "n° Orbit",
                'style': 'width: 200px; height: 43px;',
            },
        ),
    )
    
    class Meta:
        model = models.EPS_1G_ShiftSetUp_CXTTCFBK
        fields = [
            'C_XTTC_FBK_Covis',
            'C_XTTC_FBK_SC',
            'C_XTTC_FBK_nOrbit',
            'C_XTTC_FBK_Antenna',
            'C_XTTC_FBK_Extend',
        ]
    #end class
#end class











class EPS_1G_Operator_form(forms.ModelForm):
    ToUberlog = forms.CharField(
        label='Batch',
        widget=forms.Select(
            attrs={
                "id": "drpdw_EPS1_ToUberlog",
            },
            choices=models.EPS_1G_Operator.ToUberlog_CHOICES,
        ),
    )
    Uberlog_ID      = forms.CharField(
        required=False,
        label='Uberlog Username',
        widget=forms.TextInput(
            attrs={
                "placeholder": "Username",
                "id": "str_EPS1_Uberlog_ID",
            }
        )
    )
    Uberlog_Paswd   = forms.CharField(
        required=False,
        label='Uberlog Password',
        widget=forms.PasswordInput(
            attrs={
                "placeholder": "Password",
                "id": "str_EPS1_Uberlog_Paswd",
            }
        )
    )

    class Meta:
        model = models.EPS_1G_Operator
        fields = [
            'ToUberlog',
            'Uberlog_ID',
            'Uberlog_Paswd',
        ]
    #end class











class EPS_1G_ReportSetUp_form(forms.ModelForm):
    Report_type = forms.CharField(
        label='Report type',
        required=False,
        widget=forms.Select(
            attrs={
                'style': 'width: 200px; height: 50px;',
                "id": "drpdw_EPS1_RG_report",
            },
            choices=models.EPS_1G_ReportSetUp.REPORT_CHOICES,
        ),
    )
    year = forms.IntegerField(
        label='Year',
        required=False,
        widget=forms.NumberInput(
            attrs={
                "placeholder": "n° Orbit",
                'style': 'width: 200px; height: 43px;',
                "id": "int_EPS1_RG_year",
            },
        ),
    )
    CW = forms.IntegerField(
        label='CW - Calendar Week',
        required=False,
        widget=forms.NumberInput(
            attrs={
                "placeholder": "n° Orbit",
                'style': 'width: 200px; height: 43px;',
                "id": "int_EPS1_RG_CW",
            },
        ),
    )
    Date_Shift = forms.DateField(
        label='Date Shift',
        required=False,
        widget=forms.DateInput(
            attrs={
                'type': 'date',
                'class': 'mydjf_datetimefield',
                "id": "date_EPS1_RG_Date_Shift",
            }
        )
    )
    Shift = forms.CharField(
        label='Shift',
        required=False,
        widget=forms.Select(
            attrs={
                'style': 'width: 200px; height: 50px;',
                "id": "drpdw_EPS1_RG_Shift",
            },
            choices=models.EPS_1G_ReportSetUp.SHIFT_CHOICES,
        ),
    )
    #
    Date_start = forms.DateTimeField(
        label='Date Start',
        widget=MyDateTimeInput(
            attrs={
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
                "id": "datetime_EPS1_RG_Date_start",
            }
        )
    )
    Date_end = forms.DateTimeField(
        label='Date End',
        # input_formats=['%Y-%m-%d %H:%M:%S'],
        widget=forms.DateTimeInput(
            # format='%Y-%m-%d %H:%M:%S',
            attrs={
                # 'class':'datetimefield',
                'type': 'datetime-local',
                "step": "1",
                'class': 'mydjf_datetimefield',
                "id": "datetime_EPS1_RG_Date_end",
            }
        )
    )

    class Meta:
        model = models.EPS_1G_ReportSetUp
        fields = [
            'Report_type',
            'year',
            'CW',
            'Date_Shift',
            'Shift',
            'Date_start',
            'Date_end',
        ]
    #end class
#end class























































class Product_form(forms.ModelForm):
    # title       = forms.CharField(
    #     label='',
    #     widget=forms.TextInput(
    #         attrs={
    #             "placeholder": "Your title",
    #         }
    #     )
    # )
    # featured = forms.BooleanField(
    #     widget=forms.CheckboxInput(
    #         attrs={
    #             'style': 'width: 30px;',
    #         },
    #     ),
    # )
    # description = forms.CharField(
    #     required=False,
    #     widget=forms.Textarea(
    #         attrs={
    #             "placeholder": "Comment",
    #         },
    #     )
    # )
    price = forms.DecimalField(
        initial = 199.99,
        widget=forms.NumberInput(
            attrs={
                'style': 'width: 300px;',
            },
        ),
    )
    # Shift = forms.CharField(
    #     widget=forms.Select(
    #         attrs={
    #             'style': 'width: 300px;',
    #         },
    #         choices=models.Product.SHIFT_CHOICES,
    #     ),
    # )
    # Date_start = forms.DateTimeField(
    #     input_formats=['%Y/%m-%d %H:%M:%S'],
    #     widget=forms.DateTimeInput(
    #         attrs={
    #             'type': 'datetime-local',
    #             # 'class': 'form-control datetimepicker-input',
    #             # 'data-target': '#datetimepicker1',
    #             "step": "1",
    #             'style': 'width: 300px;',
    #         }
    #     )
    # )

    class Meta:
        model = models.Product
        fields = [
            # 'title',
            # 'featured',
            # 'description',
            'price',
            # 'Shift',
            # 'Date_start',
        ]
    #end class








class Raw_Product_form(forms.Form):
    title       = forms.CharField(label='')
    description = forms.CharField(
                            required=False,
                            widget=forms.Textarea(
                                    attrs={
                                        
                                    }
                                )
                            )
    price       = forms.DecimalField(initial = 199)
    # date = forms.DateTimeField(
    #     input_formats=['%Y-%m-%d %H:%M:%S'],
    #     widget=DateTimeInput(
    #         attrs={
    #             'type': 'datetime-local',
    #             'class': 'form-control datetimepicker-input',
    #             'data-target': '#datetimepicker1',
    #         }
    #     )
    # )