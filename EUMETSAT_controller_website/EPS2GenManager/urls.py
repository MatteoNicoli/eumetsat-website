from django.urls import path
from . import views

# Create your urls here.

urlpatterns = [
    path('', views.EPS2GenManager_view, name='EPS2ShiftGenerator'),
]