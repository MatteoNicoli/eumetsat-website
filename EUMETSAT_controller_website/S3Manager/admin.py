# =============================================================================
# MODULE - django
# =============================================================================
from django.contrib import admin

# 
from easy_select2 import select2_modelform

# models & forms
from . import models

# Register your models here.

class NoteAdmin(admin.ModelAdmin):
    form = select2_modelform(models.S3_ShiftSetUp)

admin.site.register(models.S3_ShiftSetUp, NoteAdmin)
admin.site.register(models.S3_Pass)
admin.site.register(models.S3_Operator)
admin.site.register(models.S3_Satellite)
admin.site.register(models.Document)
# admin.site.register(models.Spacecraft_List, NoteAdmin)

# admin.site.register(models.Book)