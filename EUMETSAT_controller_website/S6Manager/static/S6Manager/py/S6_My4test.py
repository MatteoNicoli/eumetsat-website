# -*- coding: utf-8 -*-
"""
Created on Fri May 28 07:19:02 2021

@author: Nicoli
"""
import os, sys

DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))



OS_UserName = os.getlogin()
UserName = OS_UserName
DesktopPath = os.path.join("C:", os.sep,"Users", OS_UserName, "Desktop")

# =============================================================================
# 
# =============================================================================
from static.FDF_Orekit.Orekit_function import *
from static.py.Sup_fcns import *




from multiprocessing.pool import ThreadPool




# =============================================================================
# 
# =============================================================================








# =============================================================================
# from math import radians
# 
# 
# 
# a = 800000.0 + Constants.WGS84_EARTH_EQUATORIAL_RADIUS;
# e = 0.0001;
# i = radians(98.0);
# w = -90.0;
# raan = 0.0;
# v = 0.0;
# 
# inertialFrame = FramesFactory.getEME2000();
# initialDate = AbsoluteDate(2014, 1, 1, 0, 0, 0.0, TimeScalesFactory.getUTC());
# finalDate = initialDate.shiftedBy(5000.0);
# initialOrbit = KeplerianOrbit(a, e, i, w, raan, v, PositionAngle.TRUE, inertialFrame, initialDate, Constants.WGS84_EARTH_MU);
# initialState = SpacecraftState(initialOrbit, 1000.0);
# 
# rawDetector = NodeDetector(1e-6, 
#         initialState.getOrbit(), 
#         initialState.getFrame())
# =============================================================================



















nowUTC = datetime.datetime.utcnow()

#%
# =============================================================================
# TLE - Two Line Elements
# =============================================================================
#SPOT-5 
# =============================================================================
# tle_list2 = getEUM_TLE(1)
# =============================================================================

st = SpaceTrackClient('matteo.nicoli.17@gmail.com', 'KG*p_h_VzK7xz*z')
TLE_Spacetrack = st.tle_latest(norad_cat_id=[46984], ordinal=1, format='tle')
tle_list = list(TLE_Spacetrack.split("\n"))



# =============================================================================
# 
# =============================================================================
tle_line1 = tle_list[0]
tle_line2 = tle_list[1]
mytle = TLE(tle_line1,tle_line2)

TLE_nOrbit = mytle.getRevolutionNumberAtEpoch()
TLE_Date = mytle.getDate()
print(TLE_Date, TLE_nOrbit )
propagator = TLEPropagator.selectExtrapolator(mytle)



# =============================================================================
# 
# =============================================================================
utc = TimeScalesFactory.getUTC()
inertialFrame = FramesFactory.getEME2000()
ITRF = FramesFactory.getITRF(
                            IERSConventions.IERS_2010,
                            True,
                            )
earth = OneAxisEllipsoid(
                        Constants.WGS84_EARTH_EQUATORIAL_RADIUS, 
                        Constants.WGS84_EARTH_FLATTENING, 
                        ITRF,
                        )

sun = CelestialBodyFactory.getSun()
sunRadius = 696000000.0

# =============================================================================
# Location of the Antennas
# =============================================================================
DesktopPath = os.path.join( "C:", os.sep, "Users", OS_UserName, "Desktop" )

file_path = os.path.join( "H:", os.sep, "DesktopW10", "MyDocument", "MyFiles", "Orekit_FDF", "files", "stat" )

file1 = [ os.path.join( file_path, "coord.sband.eps" ), os.path.join( file_path, "masks.eps" )]
file2 = [ os.path.join( file_path, "coord.sband.ssc" ), os.path.join( file_path, "masks.ssc" ) ]
file3 = [ os.path.join( file_path, "coord.sband.esoc" ), os.path.join( file_path, "test_masks.esoc" ) ]
file4 = [ os.path.join( file_path, "coord.sband.hrpt" ), os.path.join( file_path, "masks.hrpt" ) ]
file5 = [ os.path.join( file_path, "coord.sband.ant" ), os.path.join( file_path, "masks.ant" ) ]

#
FDF_AntLoc_EPS = FDF_AntLoc(file1)
FDF_AntLoc_ESOC = FDF_AntLoc(file3)
FDF_AntLoc_SSC = FDF_AntLoc(file2)
# =============================================================================
# FDF_AntLoc_SSC = FDF_AntLoc(file3)
# FDF_AntLoc_ESOC = FDF_AntLoc(file2)
# =============================================================================

frames_FDF_AntLoc = [FDF_AntLoc_EPS, FDF_AntLoc_SSC, FDF_AntLoc_ESOC]
FDF_AntLoc = pd.concat(frames_FDF_AntLoc)#.reset_index().drop(columns='index')



# =============================================================================
# 
# =============================================================================

Antenna = 'FBK_'
position = Vector3D(
                    float(FDF_AntLoc.loc[Antenna,'X']),
                    float(FDF_AntLoc.loc[Antenna,'Y']),
                    float(FDF_AntLoc.loc[Antenna,'Z']),
                    )
ElMask = to_elevationmask(FDF_AntLoc.loc[Antenna,'Az_M'], FDF_AntLoc.loc[Antenna,'El_M'])


extrapDate = AbsoluteDate(nowUTC.year, nowUTC.month, nowUTC.day, nowUTC.hour, nowUTC.minute, 00.000, utc)

longitude = earth.transform(
                    position,
                    ITRF,
                    extrapDate,
                    ).getLongitude()
long_deg = longitude*180/np.pi
latitude = earth.transform(
                    position,
                    ITRF,
                    extrapDate,
                    ).getLatitude()
lat_deg = latitude*180/np.pi
altitude = earth.transform(
                    position,
                    ITRF,
                    extrapDate,
                    ).getAltitude()

station = GeodeticPoint(latitude, longitude, altitude)
station_frame = TopocentricFrame(earth, station, "station 1")



# =============================================================================
# 
# =============================================================================

class myContinueOnEvent(PythonEventHandler):

    def init(self, initialstate, target):
        pass

    def eventOccurred(self, s, T, increasing):
        return Action.CONTINUE

    def resetState(self, detector, oldState):
        return oldState


detector_ANX = NodeDetector(inertialFrame)
detector_ANX = detector_ANX.withHandler(ContinueOnEvent().of_(NodeDetector))

detector_ElMask = ElevationDetector(station_frame).withElevationMask(ElMask)
detector_ElMask = detector_ElMask.withHandler(ContinueOnEvent().of_(ElevationDetector))

detector_maxEl= ElevationExtremumDetector(station_frame)
detector_maxEl = detector_maxEl.withHandler(ContinueOnEvent().of_(ElevationDetector))


El_AOS0 = np.deg2rad(0.0)
detector_AOSLOS_0 = ElevationDetector(station_frame).withConstantElevation(float(El_AOS0))
detector_AOSLOS_0 = detector_AOSLOS_0.withHandler(ContinueOnEvent().of_(ElevationDetector))

El_AOS5 = np.deg2rad(5.0)
detector_AOSLOS_5 = ElevationDetector(station_frame).withConstantElevation(float(El_AOS5))
detector_AOSLOS_5 = detector_AOSLOS_5.withHandler(ContinueOnEvent().of_(ElevationDetector))


logger = EventsLogger()

logged_detector_AOSLOS_0 = logger.monitorDetector(detector_AOSLOS_0)
propagator.addEventDetector(logged_detector_AOSLOS_0)

logged_detector_AOSLOS_5 = logger.monitorDetector(detector_AOSLOS_5)
propagator.addEventDetector(logged_detector_AOSLOS_5)

logged_detector_ElMask = logger.monitorDetector(detector_ElMask)
propagator.addEventDetector(logged_detector_ElMask)

logged_detector_maxEl = logger.monitorDetector(detector_maxEl)
propagator.addEventDetector(logged_detector_maxEl)

logged_detector_ANX = logger.monitorDetector(detector_ANX)
propagator.addEventDetector(logged_detector_ANX)

# =============================================================================
# 
# =============================================================================
extrapDate = AbsoluteDate(nowUTC.year, nowUTC.month, nowUTC.day, nowUTC.hour, 0, 00.000, utc)
initial_date = extrapDate
final_date = initial_date.shiftedBy(60.0*60*10) #seconds



pool = ThreadPool(processes=1)
def propagate_piece(Dfrom, Dto):
    vm.attachCurrentThread()
    return propagator.propagate(Dfrom, Dto)
#end def
result = pool.apply_async(
    propagate_piece,
    (initial_date, final_date)
    )
result.get()
state = propagator.propagate(initial_date, final_date)

c
state.getDate()


mylog = logger.getLoggedEvents()


# =============================================================================
# 
# =============================================================================
start_time = None
result = pd.DataFrame()


result = []
def AzElDet(propagator, station_frame, inertialFrame, Date):
    pv = propagator.getPVCoordinates(Date, inertialFrame)
    az_tmp = station_frame.getAzimuth(
        pv.getPosition(),
        inertialFrame,
        Date,
        )
    deg_az = np.rad2deg(az_tmp)
    el_tmp = station_frame.getElevation(
        pv.getPosition(),
        inertialFrame,
        Date,
        )
    deg_el = np.rad2deg(el_tmp)
    return deg_az, deg_el
# end def






    
mylog_list = list(mylog)
nANX = 0
ANX = ''
for event in range(1,len(mylog_list)):
    idate = absolutedate_to_datetime( mylog_list[event].getState().getDate() )
    
    # ANX
    
    
                
    if ( event+6 < len(mylog_list) ):
        if ((mylog_list[event-1].getEventDetector().toString()).find(('NodeDetector')) != -1 and
            (mylog_list[event].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
            (mylog_list[event+1].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
            (mylog_list[event+2].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
            (mylog_list[event+4].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
            (mylog_list[event+5].getEventDetector().toString()).find(('ElevationDetector')) != -1 and
            (mylog_list[event+6].getEventDetector().toString()).find(('ElevationDetector')) != -1
            ):
# =============================================================================
#         i3date = absolutedate_to_datetime( mylog_list[event+6].getState().getDate() )
#         if (i3date <= idate+datetime.timedelta(minutes=20)):
# =============================================================================
            
            AOS0 = mylog_list[event].getState().getDate()
            AOSM = mylog_list[event+1].getState().getDate()
            AOS5 = mylog_list[event+2].getState().getDate()
            
            LOS5 = mylog_list[event+4].getState().getDate()
            LOSM = mylog_list[event+5].getState().getDate()
            LOS0 = mylog_list[event+6].getState().getDate()
            
            
            ANX = ''
            print(mylog_list[event-1].getEventDetector().toString())
            if ( (mylog_list[event-1].getEventDetector().toString()).find(('NodeDetector')) != -1 ):
                ANX = mylog_list[event-1].getState().getDate()
                print(ANX)
                nANX = nANX + 1
            
            
            # AOS 0
            deg_az_tmp_AOS0, deg_el_tmp_AOS0 = AzElDet(propagator, station_frame, inertialFrame, AOS0)
            
            # AOS M
            deg_az_tmp_AOSM, deg_el_tmp_AOSM = AzElDet(propagator, station_frame, inertialFrame, AOSM)
            
            # AOS 5
            deg_az_tmp_AOS5, deg_el_tmp_AOS5 = AzElDet(propagator, station_frame, inertialFrame, AOS5)
            
            # Mid
            Mid = ''
            deg_az_tmp_Mid = ''
            deg_el_tmp_Mid = ''
            if ( event+3 >= 0 ):
                if (mylog_list[event+3].getEventDetector().toString().find(('ElevationExtremumDetector')) != -1 ):
                    Mid = mylog_list[event+3].getState().getDate()
                    deg_az_tmp_Mid, deg_el_tmp_Mid = AzElDet(propagator, station_frame, inertialFrame, Mid)
            
            # LOS 5
            deg_az_tmp_LOS5, deg_el_tmp_LOS5 = AzElDet(propagator, station_frame, inertialFrame, LOS5)
            
            # LOS M
            deg_az_tmp_LOSM, deg_el_tmp_LOSM = AzElDet(propagator, station_frame, inertialFrame, LOSM)
            
            # LOS 0
            deg_az_tmp_LOS0, deg_el_tmp_LOS0 = AzElDet(propagator, station_frame, inertialFrame, LOS0)
            
            #
            i_result = [
                absolutedate_to_datetime(ANX),
                
                absolutedate_to_datetime(AOS0),
                deg_az_tmp_AOS0,
                absolutedate_to_datetime(AOSM),
                deg_az_tmp_AOSM,
                absolutedate_to_datetime(AOS5),
                deg_az_tmp_AOS5,
                
                absolutedate_to_datetime(Mid) - datetime.timedelta(seconds=4*60),
                absolutedate_to_datetime(Mid) - datetime.timedelta(seconds=3*60+30),
                
                absolutedate_to_datetime(Mid),
                deg_az_tmp_Mid,
                deg_el_tmp_Mid,
                
                absolutedate_to_datetime(Mid) + datetime.timedelta(seconds=3*60+30),
                absolutedate_to_datetime(Mid) + datetime.timedelta(seconds=4*60),
                
                absolutedate_to_datetime(LOS5),
                deg_az_tmp_LOS5,
                absolutedate_to_datetime(LOSM),
                deg_az_tmp_LOSM,
                absolutedate_to_datetime(LOS0),
                deg_az_tmp_LOS0,
                ]
            
            result.append(i_result)



datacolumns_Share = ['S/C','Pass','Dump','RNG&DOP', 'Antenna','nOrbit','ANX']
PassEvent = [
        ( "ANX" ),
        ( "AOS0" ),
        ( "AOS0_Az" ),
        ( "AOSM" ),
        ( "AOSM_Az" ),
        ( "AOS5" ),
        ( "AOS5_Az" ),
        ( "FillData_start" ),
        ( "UserData_start" ),
        ( "Mid" ),
        ( "Mid_Az" ),
        ( "Mid_el" ),
        ( "UserData_end" ),
        ( "FillData_end" ),
        ( "LOS5" ),
        ( "LOS5_Az" ),
        ( "LOSM" ),
        ( "LOSM_Az" ),
        ( "LOS0" ),
        ( "LOS0_Az" ),
        ]


FDF_events = pd.DataFrame (result, columns = PassEvent)
# =============================================================================
# FDF_events.columns = MultiIndex.from_tuples(All_List_Ant)
# =============================================================================
