# -*- coding: utf-8 -*-
"""
Created on Tue Oct 12 04:48:28 2021

@author: Nicoli
"""

import os

import pandas as pd

# =============================================================================
# 
# =============================================================================
OS_UserName = os.getlogin()
UserName = OS_UserName
DesktopPath = os.path.join("C:", os.sep,"Users", OS_UserName, "Desktop" )
PathS6 = os.path.join(DesktopPath, 'S6' )


Path_OLD = os.path.join(PathS6,  'entries (4).csv')

if (os.path.isfile(Path_OLD) == True):
    file_list = pd.read_csv(Path_OLD)
    
    data=[]
    for irow in range(0, file_list.shape[0]):
        
        text = file_list.loc[irow,['text']]['text']
        
        if ( text.find(('AOS')) != -1 and 
             text.find(('Pass anomalies Orbit')) == -1 ):
            AOS_start = text.find(('AOS: '))
            LOS_start = text.find(('LOS: '))
            AOS = text[AOS_start+len('AOS: '):AOS_start+len('AOS: ')+5]
            LOS = text[LOS_start+len('LOS: '):LOS_start+len('LOS: ')+5]
            data.append([AOS,LOS])
    
    col_name = [
        ('AOS'),
        ('LOS'),
        ]
    result = pd.DataFrame (data, columns = col_name)
    
    file_Path_Name_csv = os.path.join(PathS6, 'new_file.csv')
    result.to_csv (r''+file_Path_Name_csv, index = False, header=True)
    
#end if


