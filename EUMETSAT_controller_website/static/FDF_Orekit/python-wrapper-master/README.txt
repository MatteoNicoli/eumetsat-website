Orekit (ORbit Extrapolation KIT) is a free java library
providing basic space dynamics objects and services.

It is licensed by CS Systèmes d'Information under the
Apache License Version 2.0. A copy of this license is
provided in the LICENSE.txt file.

The python-wrapper project allows using orekit in a 
python environment. This repository contains some key files
if building the wrapper from scratch. For many applications
it is recommended to use pre-built conda packages through 
the anaconda python system. See the wiki for more details,

https://gitlab.orekit.org/orekit-labs/python-wrapper/-/wikis/home


