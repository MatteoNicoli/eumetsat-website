from django.apps import AppConfig


class EPS2GenManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'EPS2GenManager'
