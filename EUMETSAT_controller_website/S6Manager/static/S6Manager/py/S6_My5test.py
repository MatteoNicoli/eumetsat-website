# -*- coding: utf-8 -*-
"""
Created on Thu Aug 26 17:30:29 2021

@author: Nicoli
"""
import os, sys

DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))



OS_UserName = os.getlogin()
UserName = OS_UserName
DesktopPath = os.path.join("C:", os.sep,"Users", OS_UserName, "Desktop")

# =============================================================================
# 
# =============================================================================
from static.FDF_Orekit.Orekit_function import *
from static.py.Sup_fcns import *







# =============================================================================
# 
# =============================================================================

nowUTC = datetime.datetime.utcnow()

FDF_AntLoc_Tot = AntenneInfo()

SC = 'S6 MICHAEL FREILICH'
Antenna = 'FBK_'
utc, earth, inertialFrame, station_frame, ElMask = Frames( FDF_AntLoc_Tot, Antenna)

# =============================================================================
# 
# =============================================================================
list_SCIDs = fcn_SCIDs()
SCID = list_SCIDs.loc[SC, "NORAD CAT ID"]

mytle = TLEManager(SCID)
TLE_nOrbit = mytle.getRevolutionNumberAtEpoch()
TLE_Date = mytle.getDate()
Period = 2*np.pi/mytle.getMeanMotion()
print(TLE_Date )
print(TLE_nOrbit )


propagator = TLEPropagator.selectExtrapolator(mytle)














# =============================================================================
# 
# =============================================================================
logger = EventsLogger()



class myContinueOnEvent(PythonEventHandler):

    def init(self, initialstate, target):
        pass

    def eventOccurred(self, s, T, increasing):
        return Action.CONTINUE

    def resetState(self, detector, oldState):
        return oldState




# Node .EventSlopeFilter(rawDetector, FilterType.TRIGGER_ONLY_INCREASING_EVENTS)
# =============================================================================
# detector_ANX = NodeDetector(inertialFrame)
# detector_ANX = detector_ANX.withHandler(ContinueOnEvent().of_(NodeDetector))
# =============================================================================

rawDetector = NodeDetector(inertialFrame)
filter_ANX = EventSlopeFilter(rawDetector, FilterType.TRIGGER_ONLY_INCREASING_EVENTS)
detector_ANX = filter_ANX.withHandler(ContinueOnEvent().of_(NodeDetector ))
logged_detector_ANX = logger.monitorDetector(detector_ANX)
propagator.addEventDetector(logged_detector_ANX)


# Max Elevation
detector_maxEl= ElevationExtremumDetector(station_frame)
detector_maxEl = detector_maxEl.withHandler(ContinueOnEvent().of_(ElevationDetector))
logged_detector_maxEl = logger.monitorDetector(detector_maxEl)
propagator.addEventDetector(logged_detector_maxEl)




# Elevation 5 deg
El_AOS0 = np.deg2rad(0.0)
detector_AOSLOS_0 = ElevationDetector(station_frame).withConstantElevation(float(El_AOS0))
detector_AOSLOS_0 = detector_AOSLOS_0.withHandler(ContinueOnEvent().of_(ElevationDetector))
logged_detector_AOSLOS_0 = logger.monitorDetector(detector_AOSLOS_0)
propagator.addEventDetector(logged_detector_AOSLOS_0)


# Elevation mask
if (ElMask!=None):
    detector_ElMask = ElevationDetector(station_frame).withElevationMask(ElMask)
    detector_ElMask = detector_ElMask.withHandler(ContinueOnEvent().of_(ElevationDetector))
    logged_detector_ElMask = logger.monitorDetector(detector_ElMask)
    propagator.addEventDetector(logged_detector_ElMask)


# Elevation 5 deg
El_AOS5 = np.deg2rad(5.0)
detector_AOSLOS_5 = ElevationDetector(station_frame).withConstantElevation(float(El_AOS5))
detector_AOSLOS_5 = detector_AOSLOS_5.withHandler(ContinueOnEvent().of_(ElevationDetector))
logged_detector_AOSLOS_5 = logger.monitorDetector(detector_AOSLOS_5)
propagator.addEventDetector(logged_detector_AOSLOS_5)
























# =============================================================================
# 
# =============================================================================
DS = nowUTC
extrapDate = AbsoluteDate(DS.year, DS.month, DS.day, DS.hour, DS.minute, 00.000, utc)
initial_date = extrapDate
final_date = initial_date.shiftedBy(60.0*60*10) #seconds



TLEDate = absolutedate_to_datetime( TLE_Date )
DS = TLEDate-datetime.timedelta( minutes=102 )
initial_date = AbsoluteDate(DS.year, DS.month, DS.day, DS.hour, DS.minute, 00.000, utc)
final_datetime = nowUTC + datetime.timedelta(hours=30)
final_date = AbsoluteDate(
    final_datetime.year,
    final_datetime.month,
    final_datetime.day,
    final_datetime.hour,
    0,
    00.000,
    utc,
    )



# =============================================================================
# 
# =============================================================================
state = propagator.propagate(initial_date, final_date)
state.getDate()
mylog = logger.getLoggedEvents()


# =============================================================================
# 
# =============================================================================
start_time = None
result = pd.DataFrame()







    
mylog_list = list(mylog)
nANX = 0
ANX = ''
flag_ElevationDetector = False
for event in range(0,len(mylog_list)):
    idate = absolutedate_to_datetime( mylog_list[event].getState().getDate() )
    print(idate, mylog_list[event].getEventDetector().toString())
    # ANX
    
            



