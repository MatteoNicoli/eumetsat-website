# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 17:25:15 2021

@author: Nicoli
"""
# =============================================================================
# 
# =============================================================================
from openpyxl import load_workbook


from multiprocessing.pool import ThreadPool

# =============================================================================
# 
# =============================================================================
from static.FDF_Orekit.Orekit_function import *











# =============================================================================
# 
# =============================================================================
def i_Sentinel3_SB(sh, cell_Date, Name_Cell_Col, cel_row):
    cel_row3A=cel_row
    flag_3A = True
    S3A_list = []
    while (flag_3A ):
        cel_row3A = cel_row3A + 1
        Name_Cell = Name_Cell_Col+str(cel_row3A)
        Text_Cell = sh[Name_Cell]._value
        
        if ( Text_Cell != None):
            for icell_Date in cell_Date:
                Name_Cell_Date = icell_Date+str(cel_row3A)
                Text_Cell_Date = sh[Name_Cell_Date]._value
                
                Date = sh[icell_Date+'4']._value
                if ( Text_Cell_Date!=None and Text_Cell_Date!='' and Text_Cell_Date!='\n\n'  and Text_Cell_Date!='\n'):
                    
                    listtime = re.findall(r'\d+', str(Text_Cell_Date) )
                    
                    iDate = Date + datetime.timedelta(hours=int(listtime[0]))+ datetime.timedelta(minutes=int(listtime[1]))
                    S3A_list.append(
                        [
                            iDate,
                            Text_Cell,
                            ]
                        )
                        
                    if (len(listtime)>3 ):
                        iDate = Date + datetime.timedelta(hours=int(listtime[2]))+ datetime.timedelta(minutes=int(listtime[3]))
                        S3A_list.append(
                            [
                                iDate,
                                Text_Cell,
                                ]
                            )
                    if (len(listtime)>5 ):
                        iDate = Date + datetime.timedelta(hours=int(listtime[4]))+ datetime.timedelta(minutes=int(listtime[5]))
                        S3A_list.append(
                            [
                                iDate,
                                Text_Cell,
                                ]
                            )
            
        else:
            flag_3A = False
            
    PassEvent = [
        ('Date'),
        ('Antenna'),
        ]
    result = pd.DataFrame (S3A_list, columns = PassEvent)
    FDF_Pass_M = result.sort_values(by=['Date']).reset_index().drop(columns='index')
    
    FDF_Pass_M['Pass']=2
    Date_day = FDF_Pass_M.loc[0,['Date']]['Date'].day
    for irow in range(0,FDF_Pass_M.shape[0]):
        iDate_day = FDF_Pass_M.loc[irow,['Date']]['Date'].day
        if ( iDate_day != Date_day ):
            Date_day = FDF_Pass_M.loc[irow,['Date']]['Date'].day
            FDF_Pass_M.at[irow,'Pass']=1
    
    return FDF_Pass_M
    



def Sentinel3_SB( file_Path_Name_WS ):
    
    wb = load_workbook(file_Path_Name_WS, data_only = True)
    Worksheet = wb.sheetnames[1]
    sh = wb[Worksheet]
    
    
    Name_Cell_Col = 'A'
    cel_row = 0
    flag_Text = True
    cell_Date= ['F','G','H','I','J','K','L','M']
    while ( flag_Text ):
        cel_row = cel_row + 1
        Name_Cell = Name_Cell_Col+str(cel_row)
        Text_Cell = sh[Name_Cell]._value
        print(Text_Cell)
        if ( Text_Cell== 'Sentinel 3A' ):
            S3A_SB_events = i_Sentinel3_SB(sh, cell_Date, Name_Cell_Col, cel_row)
            print(S3A_SB_events)
        if ( Text_Cell== 'Sentinel 3B' ):
            S3B_SB_events = i_Sentinel3_SB(sh, cell_Date, Name_Cell_Col, cel_row)     
            flag_Text = False
    
    
    
        
    
    Sentine3_SB={
        'S3A': S3A_SB_events,
        'S3B': S3B_SB_events,
        }
    return Sentine3_SB







def S3_mergeWimpy(frames, DS_S, DE_E):
    FDF_Pass_M = pd.concat(frames)
# =============================================================================
#     FDF_Pass_M = result.sort_values(by=[('AOS0')]).reset_index().drop(columns='index')
# =============================================================================
    
    
# =============================================================================
#     indexNames = FDF_Pass_M[ FDF_Pass_M['AOS0'] < DS_S ].index
#     FDF_Pass_M.drop(indexNames , inplace=True)
#     indexNames = FDF_Pass_M[ FDF_Pass_M['AOS0'] > DE_E ].index
#     FDF_Pass_M.drop(indexNames , inplace=True)
#     FDF_Pass_M = FDF_Pass_M.reset_index()
#     FDF_Pass_M = FDF_Pass_M.drop(columns='index')
#     
# =============================================================================
    FDF_Pass_M = FDF_Pass_M[
        ( FDF_Pass_M['AOS0'] > DS_S ) &
        ( FDF_Pass_M['AOS0'] < DE_E )
        ].sort_values(by=[('AOS0')]).reset_index().drop(columns='index')
    
    
    return FDF_Pass_M
#end def



def Sentinel_3_SX( DS, DE, list_SCIDs, SC, FDF_AntLoc_Tot, List_ESOC_IDs, Sentine3_SB, FDF_S3A_S25):
    
    for iSP in range(0,Sentine3_SB.shape[0]):
        Ant_S = Sentine3_SB.loc[iSP,['ground_station']]['ground_station']
        Antenna_S = List_ESOC_IDs.loc[Ant_S,['ID']]['ID']
        
        utc, earth, inertialFrame, station_frame, ElMask = Frames( FDF_AntLoc_Tot, Antenna_S)
        
        #
        FDF_S3A_S, propagator = FDF_PassGenerator(Antenna_S, list_SCIDs, SC, DS, DE, utc, inertialFrame, station_frame, ElMask )
        
        Date_S = Sentine3_SB.loc[iSP,['telemetryReception_start']]['telemetryReception_start']
        DS_S = Date_S - datetime.timedelta(minutes=10)
        DE_E = Date_S + datetime.timedelta(minutes=10)
# =============================================================================
#         indexNames = FDF_S3A_S[ FDF_S3A_S['AOS0'] < DS_S ].index
#         FDF_S3A_S.drop(indexNames , inplace=True)
#         indexNames = FDF_S3A_S[ FDF_S3A_S['AOS0'] > DE_E ].index
#         FDF_S3A_S.drop(indexNames , inplace=True)
#         FDF_S3A_S = FDF_S3A_S.reset_index()
#         FDF_S3A_S = FDF_S3A_S.drop(columns='index')
# =============================================================================
        
        FDF_S3A_S = FDF_S3A_S[
            ( FDF_S3A_S['AOS0'] > DS_S ) &
            ( FDF_S3A_S['AOS0'] < DE_E )
            ].sort_values(by=[('AOS0')]).reset_index().drop(columns='index')
        
        c=FDF_S3A_S25[FDF_S3A_S25['ANX']==FDF_S3A_S.loc[0,['ANX']]['ANX']].index
        FDF_S3A_S.at[0,'nOrbit'] = FDF_S3A_S25.loc[c[0],['nOrbit']]['nOrbit']
        
        FDF_S3A_S['Pass'] = 'S'+str(Sentine3_SB.loc[iSP,['nSB']]['nSB']*True)
        FDF_S3A_S['DumpAnt'] = List_ESOC_IDs.loc[Ant_S,['ID2']]['ID2']
        
        FDF_S3A_S['satellite_id'] = Sentine3_SB.loc[iSP,['satellite_id']]['satellite_id']
        FDF_S3A_S['ground_station'] = Sentine3_SB.loc[iSP,['ground_station']]['ground_station']
        FDF_S3A_S['nSB'] = Sentine3_SB.loc[iSP,['nSB']]['nSB']
        FDF_S3A_S['activity_start'] = Sentine3_SB.loc[iSP,['activity_start']]['activity_start']
        FDF_S3A_S['activity_end'] = Sentine3_SB.loc[iSP,['activity_end']]['activity_end']
        FDF_S3A_S['tracking_start'] = Sentine3_SB.loc[iSP,['tracking_start']]['tracking_start']
        FDF_S3A_S['tracking_end'] = Sentine3_SB.loc[iSP,['tracking_end']]['tracking_end']
        FDF_S3A_S['telemetryReception_start'] = Sentine3_SB.loc[iSP,['telemetryReception_start']]['telemetryReception_start']
        FDF_S3A_S['telemetryReception_end'] = Sentine3_SB.loc[iSP,['telemetryReception_end']]['telemetryReception_end']
        FDF_S3A_S['telecommandUplink_start'] = Sentine3_SB.loc[iSP,['telecommandUplink_start']]['telecommandUplink_start']
        FDF_S3A_S['telecommandUplink_end'] = Sentine3_SB.loc[iSP,['telecommandUplink_end']]['telecommandUplink_end']
        
        FDF_S3A_S25 = FDF_S3A_S25.append(FDF_S3A_S)
        
        
    FDF_S3A_S25 = FDF_S3A_S25.sort_values(by=[('AOS0')]).reset_index().drop(columns='index')
    return FDF_S3A_S25










def fcn_Chronops_Mang (Wimpyfilepath,DS,DE):
    if ( os.path.isfile( Wimpyfilepath ) == True ):
        fp=open(Wimpyfilepath,'r')
        lines=fp.readlines()
        fp.close()
        
        def InfoChro_Si(string, header, body):
            start = header+body+'>'
            end = '</'+body+'>'
            ni_S = string.find( start )
            ni_E = string.find( end )
            Info = string[ni_S+len(start):ni_E]
            return Info
        
        data = []
        flag_1SB = False
        A_1_old = datetime.datetime(1900, 1, 1, 0, 0, 0, 0*1000)
        for i_row in range(0,len(lines)):
            if( lines[i_row] == '    <service_session>\n' ):
                SC = InfoChro_Si(lines[i_row + 2], '      <', 'satellite_id')
                GS = InfoChro_Si(lines[i_row + 3], '      <', 'ground_station')
                AS_1 = datetime.datetime.strptime( InfoChro_Si(lines[i_row + 4], '      <', 'activity_start'), '%Y-%m-%dT%H:%M:%S.%fZ')
                AE_1 = datetime.datetime.strptime( InfoChro_Si(lines[i_row + 5], '      <', 'activity_end'), '%Y-%m-%dT%H:%M:%S.%fZ')
                TS_1 = datetime.datetime.strptime( InfoChro_Si(lines[i_row + 6], '      <', 'tracking_start'), '%Y-%m-%dT%H:%M:%S.%fZ')
                TE_1 = datetime.datetime.strptime( InfoChro_Si(lines[i_row + 7], '      <', 'tracking_end'), '%Y-%m-%dT%H:%M:%S.%fZ')
                
                TMRS = 'n/a'
                TMRE = 'n/a'
                TCUS = 'n/a'
                TCUE = 'n/a'
                irow = i_row
                flag = True
                while ( flag == True ):
                    if ( lines[irow].find('<service_type>telemetryReception</service_type>') != -1 ):
                        TMRS = datetime.datetime.strptime( InfoChro_Si(lines[irow +1], '        <', 'activity_start').replace(" ", ""), '%Y-%m-%dT%H:%M:%S.%fZ')
                        TMRE = datetime.datetime.strptime( InfoChro_Si(lines[irow +2], '        <', 'activity_end').replace(" ", ""), '%Y-%m-%dT%H:%M:%S.%fZ')
                    if ( lines[irow].find('<service_type>telecommandUplink</service_type>') != -1 ):
                        TCUS = datetime.datetime.strptime( InfoChro_Si(lines[irow +1], '        <', 'activity_start').replace(" ", ""), '%Y-%m-%dT%H:%M:%S.%fZ')
                        TCUE = datetime.datetime.strptime( InfoChro_Si(lines[irow +2], '        <', 'activity_end').replace(" ", ""), '%Y-%m-%dT%H:%M:%S.%fZ')
                    
                    if ( lines[irow].find('/service_session>') != -1 ):
                        flag = False
                    irow = irow + 1
                
                A_1_new = datetime.datetime(AS_1.year, AS_1.month, AS_1.day, 0, 0, 0, 0*1000)
                nSB = False
                if (flag_1SB == False and A_1_new != A_1_old):
                    flag_1SB = True
                    nSB = True
                else:
                    flag_1SB = False
                
                A_1_old = A_1_new
                if (TCUE!='n/a'):
                    data.append([SC, GS, nSB, AS_1, AE_1, TS_1, TE_1, TMRS, TMRE, TCUS, TCUE])
        
        datacolumns = ['satellite_id',
                       'ground_station',
                       'nSB',
                       'activity_start', 'activity_end',
                       'tracking_start', 'tracking_end',
                       'telemetryReception_start', 'telemetryReception_end',
                       'telecommandUplink_start', 'telecommandUplink_end']
        Chronops_Pass = pd.DataFrame (data, columns = datacolumns)
        Chronops_Info = [True,Chronops_Pass]
        
# =============================================================================
#         if ( len(Chronops_Pass) == 0 ):
#             Chronops_Info[0] = False
#         else:
# =============================================================================
# =============================================================================
#             for i in range(0,Chronops_Pass.shape[0]-1):
#                 Chronops_Pass.loc[i,'tracking_start']
# =============================================================================
        Chronops_Pass = Chronops_Pass[
            ( Chronops_Pass['telemetryReception_start'] >= DS ) &
            ( Chronops_Pass['telemetryReception_end'] < DE )
            ].sort_values(by=[('activity_start')]).reset_index().drop(columns='index')
        #end if
    else:
        Chronops_Info = [False,0]
    #end if
    
    return Chronops_Pass
#end def










# =============================================================================
# DS = datetime.datetime.strptime(my_form_S3.cleaned_data['Date_start'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
# DE = datetime.datetime.strptime(my_form_S3.cleaned_data['Date_end'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
# =============================================================================
def S3_FDF_events(DIR,DIR_int,SetUp):
    # =============================================================================
    #     
    # =============================================================================
    DS = datetime.datetime.strptime(SetUp['Date_start'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    DE = datetime.datetime.strptime(SetUp['Date_end'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    
    # =============================================================================
    # 
    # =============================================================================
    path = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv' )
    

    Path_SupFile_List_ESOC_IDs = os.path.join(path,'ESOC_antenna_ID.csv')
    if (os.path.isfile(Path_SupFile_List_ESOC_IDs) == True):
        List_ESOC_IDs = pd.read_csv(Path_SupFile_List_ESOC_IDs, index_col=0)
    #end if

    path_SCInfo = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','SCInfo.csv')
    if (os.path.isfile(path_SCInfo) == True):
        list_SCIDs = pd.read_csv(path_SCInfo, index_col=0)
    #end if
    
    # =============================================================================
    # 
    # =============================================================================
    path_PLANVIEW = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','media','S3Manager','schedule' )
    list_files = os.listdir(path_PLANVIEW)
    file_data = []
    for file in list_files:
        name, ext = os.path.splitext( file )
        date_s = datetime.datetime.strptime( name[10:25], "%Y%m%dT%H%M%S" )
        date_e = datetime.datetime.strptime( name[26:41], "%Y%m%dT%H%M%S" )
        file_data.append([name,ext, date_s,date_e])
    #end for
    
    columnsname = ['file_name', 'file_ext', 'date_start', 'date_end']
    file_df = pd.DataFrame (file_data, columns = columnsname)
    
    Antenna = 'S25x'
    FDF_AntLoc_Tot = AntenneInfo()
    utc, earth, inertialFrame, station_frame, ElMask = Frames( FDF_AntLoc_Tot, Antenna)
    
    frames = []
    for i_SC in SetUp['SC']:
        SC_ID = list_SCIDs.loc[i_SC, "shortcut"]
        
        files_df = file_df[
            ( file_df['file_ext'] == '.'+SC_ID ) &
            ( file_df['date_start'] <= DS )
            ].sort_values(by=[('date_start')]).reset_index().drop(columns='index')
        i_file = files_df.loc[files_df.shape[0]-1,['file_name']]['file_name']+files_df.loc[files_df.shape[0]-1,['file_ext']]['file_ext']
        
        path_PLANVIEW_S3x = os.path.join(path_PLANVIEW,i_file)
        Chronops_Info_3x = fcn_Chronops_Mang (path_PLANVIEW_S3x,DS,DE)
        
        #
        FDF_S3x_S25, propagator = FDF_PassGenerator(Antenna, list_SCIDs, i_SC, DS, DE, utc, inertialFrame, station_frame, ElMask )
        FDF_S3x_S25['Pass'] = 'X'
        FDF_S3x_S25['DumpAnt'] = 'SG24'
        FDF_S3x_SX = Sentinel_3_SX( DS, DE, list_SCIDs, i_SC, FDF_AntLoc_Tot, List_ESOC_IDs, Chronops_Info_3x, FDF_S3x_S25)
        
        #
        frames.append(FDF_S3x_SX)
        
    #end for
    
    FDF_event_S3 = pd.concat(frames)
    FDF_event_S3 = FDF_event_S3[
        ( FDF_event_S3['AOS0'] >= DS ) &
        ( FDF_event_S3['AOS0'] < DE )
        ].sort_values(by=[('ANX')]).reset_index().drop(columns='index')
    
    
    return FDF_event_S3















def S3_FDF_events_gen(DIR,DIR_int,SetUp):
    pool = ThreadPool(processes=1)
    def ThreadPool_S3_FDF_events(DIR,DIR_int,SetUp):
        vm.attachCurrentThread()
        return S3_FDF_events(DIR,DIR_int,SetUp)
    #end def

    result = pool.apply_async(
        ThreadPool_S3_FDF_events,
        (DIR,DIR_int,SetUp,)
        )
    FDF_event_S3 = result.get()
    
    return FDF_event_S3