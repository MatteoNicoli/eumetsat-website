# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 10:28:17 2020

@author: Nicoli
"""
import os
import datetime
import calendar
#import xlwings as xw
#import openpyxl as opx
from openpyxl import load_workbook











def S36_Shift_List_Rec(now_local, UserName, file_Path_Name_Shift, Controller):
    #Worksheet = 'Original'
    Name_Cell_Col = 'A'
    Months_Day_Cell_Col = ['B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF']
    Month_Day_Cell_Col = Months_Day_Cell_Col[now_local.day-1]
    
    flag_CN = 0
    
    Controller_XTTC_PreBrief = [0,0,0]
    
    ControllerName = Controller['Name']+'. '+Controller['Surname']
    CN = Controller['Name']
    SN = Controller['Surname'].lower()
    flag_CN = 1
    Controller_XTTC_PreBrief[1] = 0
    Controller_XTTC_PreBrief[2] = 0
    
    
    if (flag_CN==1):
        wb = load_workbook(file_Path_Name_Shift, data_only = True)
        Worksheet = wb.sheetnames[-1]
        sh = wb[Worksheet]
        
        cel_row = 0
        flag_Name = 1
        while (flag_Name == 1):
            cel_row = cel_row + 1
            Name_Cell = Name_Cell_Col+str(cel_row)
            NameControllers = sh[Name_Cell]._value
            if ( NameControllers != None ):
                if (NameControllers.lower().find(SN) != -1 ):
                    flag_Name = 0
            if (cel_row > 40):
                flag_Name = 0
        
        Month_Day_Cell = Month_Day_Cell_Col+str(cel_row)
        Shift_Time = sh[Month_Day_Cell]._value    
        if (Shift_Time=='M'):
            Shift = 1
        elif (Shift_Time=='E'):
            Shift = 2
        elif (Shift_Time=='N1' or Shift_Time=='N2', Shift_Time=='N'):
            Shift = 3
        else:
            Shift = 0
        
        Shift_Side = sh[Month_Day_Cell].fill.start_color.index
        
        if (Shift_Side==(146, 208, 80) or Shift_Side==12):
            Side = 'S3'
        elif (Shift_Side==(0, 176, 240) or Shift_Side=='FF008000'):
            Side = 'S6'
        else:
            Side = 0
        
        S36_ShiftSide = [Shift, Side, CN, Shift_Time]
        
    else:
        S36_ShiftSide = [0, Side, 'Controller', 0]
    return S36_ShiftSide




