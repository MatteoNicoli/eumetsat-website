# =============================================================================
# MODULE - django
# =============================================================================
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponse
from django.utils import timezone
from django.views.generic import View
from django.views import generic

# 
from easy_select2 import select2_modelform

# models & forms
from . import models, forms

# =============================================================================
# MODULE - useful
# =============================================================================
import os, sys
DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))

import datetime
import csv

# =============================================================================
# MODULE - customized
# =============================================================================
from static.py.Sup_fcns import *
from .static.S3Manager.py.S3_Sup import *
from .static.S3Manager.py.S3_csv import *

# Create your views here.


def InitialData_elog():
    now_local =datetime.datetime.now()
    now_local = timezone.now()
    Mission = models.S3_Satellite.Mission
    list_SC = models.S3_Satellite.list_SC

    Shift_Info = Date_Shift( Mission, now_local)
    Sh = Shift_Info[0]
    date_s = Shift_Info[1]
    date_e = Shift_Info[2]

    initial_data_ToUberlog = {
        'ToUberlog': 'import', 
    }
    initial_data_S3 = {
        'SC': ['SENTINEL 3A', 'SENTINEL 3B'],
        'Daily_Events': True,
        'Date_Shift': date_s.strftime("%Y-%m-%d"),
        'Shift': Sh,
        'Date_start': date_s.strftime("%Y-%m-%dT%H:%M:%S"),
        'Date_end': date_e.strftime("%Y-%m-%dT%H:%M:%S"),
    }

    dict_initial_data = {
        'Mission': Mission,
        'list_SC': list_SC,
        'ToUberlog': initial_data_ToUberlog,
        'S3': initial_data_S3,
    }
    return dict_initial_data
#end def

class S3Manager_view(View):
    

    


    # =============================================================================
    # 
    # =============================================================================
    def get(self, request):
        dict_InitialData = InitialData_elog()
        form_ToUberlog = forms.S3_Operator_form(initial=dict_InitialData['ToUberlog'])
        form_S3 = forms.S3_ShiftSetUp_form(initial=dict_InitialData['S3'])
        form_Doc = forms.DocumentForm()
        documents = models.Document.objects.all()

        # model = models.Book
        # my_form = forms.BookForm()

        # print(models.S3_ShiftSetUp.SHIFT_CHOICES)

        if ( request.is_ajax() ):
            S3_Date_Shift_val=request.GET.get('S3_Date_Shift_val')
            S3_Shift_val=request.GET.get('S3_Shift_val')
            if ( S3_Date_Shift_val != None and S3_Shift_val != None ):
                strdate_s, strdate_e = strDate_Shift(dict_InitialData['Mission'], S3_Date_Shift_val, S3_Shift_val )
                # 
                context_ajax = {
                    'date_s': strdate_s,
                    'date_e': strdate_e,
                }
                return JsonResponse(context_ajax, status=200)
            #end if    
        #end if
        
        # template & context
        template = 'S3Manager/S3Manager.html'
        context = {
            'contx_form_ToUberlog': form_ToUberlog,
            'contx_form_S3': form_S3,
            'form': form_Doc,
            'documents': documents,
        }
        return render(request, template, context=context)
    #end def

    
    
    
    
    
    # =============================================================================
    # 
    # =============================================================================
    def post(self, request):

        if ( 'btn_S3_btnuploadfile' in request.POST ):
            form_Doc = forms.DocumentForm(request.POST, request.FILES)
            if ( form_Doc.is_valid() ):
                newdoc = models.Document(docfile = request.FILES['docfile'])
                newdoc_name, newdoc_ext = os.path.splitext(newdoc.docfile.name)
                list_SC = list_SC = models.S3_Satellite.list_SC
                list_SC_s = list_SC["shortcut"].tolist()

                if ( newdoc_ext[1:] in list_SC_s ):
                    documents = models.Document.objects.all()
                    flag = False
                    for document in documents:
                        document_name, document_ext = os.path.splitext(document.docfile.name)
                        doc_path = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','media', document.docfile.name)
                        
                        if ( document_ext == newdoc_ext and 
                             document_name == newdoc_name ):
                            document.delete()
                            os.remove( doc_path )
                            newdoc.date_str = datetime.datetime.utcnow().strftime("%Y-%j %H:%M:%S")
                            newdoc.name_str = newdoc_name + newdoc_ext
                            newdoc.save()
                            flag = True
                        #end if
                    #end for
                    if ( flag == False ):
                        newdoc.date_str = datetime.datetime.utcnow().strftime("%Y-%j %H:%M:%S")
                        newdoc.name_str = newdoc_name + newdoc_ext
                        newdoc.save()
                    #end if
                #end if
            #end if
        #end if

        if ( 'btn_S3_btnToUberlog' in request.POST ):
            form_ToUberlog = forms.S3_Operator_form(request.POST or None)
            form_S3 = forms.S3_ShiftSetUp_form(request.POST or None)

            if ( form_S3.is_valid() and form_ToUberlog.is_valid() ):
                print(form_ToUberlog.cleaned_data)
                print(form_S3.cleaned_data)
                # =============================================================================
                # 
                # =============================================================================
                FDF_event_S3 = S3_FDF_events_gen(
                    DIR,DIR_int,
                    form_S3.cleaned_data,
                    )

                #
                path = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv' )

                Path_SupFile_List_LogBookIDs = os.path.join(path,'LogBookID.csv')
                if (os.path.isfile(Path_SupFile_List_LogBookIDs) == True):
                    List_LogBookIDs = pd.read_csv(Path_SupFile_List_LogBookIDs, index_col=0)
                #end if
                
                Batch_info = Events2csv(
                    fcn_S3_events_csv,
                    FDF_event_S3,
                    form_S3.cleaned_data,
                    form_ToUberlog.cleaned_data,
                    List_LogBookIDs,
                )

                if ( form_ToUberlog.cleaned_data['ToUberlog'] == 'csv' ):
                    # Create the HttpResponse object with the appropriate CSV header.
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment; filename='+Batch_info['Name']+'.csv'
                    
                    # Create the CSV writer using the HttpResponse as the "file"
                    Batch_csv = Batch_info['Data']
                    Batch_csv.to_csv (
                        #r''+file_Path_Name_csv,
                        path_or_buf = response,
                        index = False,
                        header = False,
                        )
                    return response
                    # return redirect('S3elogbooGenerator')
                #end if
            #end if
        #end if
        # Redirect to THIS view, assuming it lives in 'some app'
        return redirect('S3elogbooGenerator')
    #end def
#end class













# class BookCreateView(generic.CreateView):
#     model = models.Book
#     form_class = forms.BookForm
#     success_url = "/"