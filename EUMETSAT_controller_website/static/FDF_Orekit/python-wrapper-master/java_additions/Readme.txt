The sources for the java classes that allows Python subclassing are no longer in a separate package
but integrated in a modified orekit source fork.

This fork is available at:
https://github.com/petrushy/Orekit

A compiled jar and some other artifacts are available at:
https://github.com/petrushy/orekit_python_artifacts/tree/version-10.1

