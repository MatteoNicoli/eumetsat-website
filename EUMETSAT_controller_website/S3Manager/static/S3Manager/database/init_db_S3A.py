# -*- coding: utf-8 -*-
"""
Created on Sat Dec 11 16:52:14 2021

@author: Nicoli
"""
import datetime
import pandas as pd
import sqlite3


connection = sqlite3.connect('S3A_pass.db')
with open('create_S3A_pass.sql') as f:
    connection.executescript(f.read())
connection.commit()
connection.close()



# =============================================================================
# 
# =============================================================================
con = sqlite3.connect('S3A_pass.db')
cur = con.cursor()
cur.execute(
    "INSERT INTO S3A_pass (Antenna, nOrbit, ANX) VALUES ('CDA2s','123456','2021-12-13T13:45:40.185')"
    )
con.commit()
con.close()

con = sqlite3.connect('S3A_pass.db')
cur = con.cursor()

# =============================================================================
# 
# =============================================================================
connection = sqlite3.connect('S3A_pass.db')
S3A_pass = pd.read_sql_query("SELECT * from S3A_pass", connection)

fmt = '%Y-%m-%dT%H:%M:%S.%f'
date_string = S3A_pass.loc[0,['ANX']]['ANX']
ANX = datetime.datetime.strptime( date_string, fmt )


S3A_pass.at[0,['AOS0']]='ciao'

S3A_pass.to_sql("S3A_pass", connection, if_exists="replace")
connection.close()

