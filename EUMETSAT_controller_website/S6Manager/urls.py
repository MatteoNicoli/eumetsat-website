# =============================================================================
# MODULE - django
# =============================================================================
from django.urls import path
from . import views

# Create your urls here.


urlpatterns = [
    path('', views.S6Manager_view.as_view(), name='S6Manager'),
    path('S6elogbooGenerator', views.S6Manager_view.as_view(), name='S6elogbooGenerator'),
]