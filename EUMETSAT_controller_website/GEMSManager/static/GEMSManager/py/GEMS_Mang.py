# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 08:47:19 2020

@author: Nicoli
"""
import datetime
import requests
from requests.auth import HTTPBasicAuth
from bs4 import BeautifulSoup
import pandas as pd




#==============================================================================
def acq_GEMS_Info(GEMS_URL):
    
    if (GEMS_URL[:len('https://masif.eumetsat.int/GEMS/')] == 'https://masif.eumetsat.int/GEMS/' ):
        authentication = HTTPBasicAuth('grndcon', 'newMASIFpw')
        s = requests.Session()
        res = s.get(GEMS_URL, auth=authentication)
        cookies = dict(res.cookies)
        res = s.post(GEMS_URL, auth=authentication,
                verify=False,
                cookies={'my': 'cookies'})
        soup = BeautifulSoup(res.content, 'html.parser')
        str_Table_GEMS_iFac = soup.find_all('table')
        Table_GEMS_Fac = pd.read_html(str(str_Table_GEMS_iFac))[0]
        
        
        
        def GEMS_value(str_text, indx_i):
            indx_GEMS_F = str_text.find(indx_i)
            lsit_str_F = []
            while ( indx_GEMS_F != -1 ):
                indx_GEMS_F2 = str_text[indx_GEMS_F:].find('&')
                istr_F = str_text[indx_GEMS_F+len(indx_i):indx_GEMS_F+indx_GEMS_F2]
                lsit_str_F.append(istr_F)
                indx_GEMS_F = str_text[indx_GEMS_F+indx_GEMS_F2:].find(indx_i)
            #end while
            str_F = '+'.join( lsit_str_F )
            return str_F
        #end def
        
        
        indx_ST = 'startTime='
        str_ST = GEMS_value(GEMS_URL, indx_ST)
        date_ST = datetime.datetime.strptime( str_ST, '%y.%j.%H.%M.%S')
        
        indx_ET = 'endTime='
        str_ET = GEMS_value(GEMS_URL, indx_ET)
        date_ET = datetime.datetime.strptime( str_ET, '%y.%j.%H.%M.%S')
        
        indx_F = 'facility='
        str_F = GEMS_value(GEMS_URL, indx_F)
        
        indx_Sev = 'severity='
        str_Sev = GEMS_value(GEMS_URL, indx_Sev)
        
        
        Table_GEMS_iFac = Table_GEMS_Fac.loc[:,[str_F+' History']][str_F+' History']
        c = Table_GEMS_iFac['Timestamp']
        cl = c.values.tolist()
        datetime_list = []
        
        for idate in range(0,len(cl)):
            i_datetime = datetime.datetime.strptime(cl[idate], '%y.%j.%H.%M.%S.%f')
            datetime_list.append( [i_datetime, i_datetime.strftime('%j')] )
        #end for
        datacolum=['Date','DOY']
        addTable_GEMS_iFac = pd.DataFrame(datetime_list, columns = datacolum)
        Table_GEMS_iFac = pd.concat([Table_GEMS_iFac, addTable_GEMS_iFac.reindex(Table_GEMS_iFac.index)], axis=1)
        
        GEMS_info_list = {
            'Valitity': True,
            'URL': GEMS_URL,
            'Facility': str_F,
            'Severity': str_Sev,
            'Date_ST': date_ST.strftime("%Y-%m-%dT%H:%M:%S"),
            'Date_ET': date_ET.strftime("%Y-%m-%dT%H:%M:%S"),
            'str_Events': str_Table_GEMS_iFac,
            }
    else:
        GEMS_info_list = {
            'Valitity': True,
        }
    #end if
    
    return GEMS_info_list
#end def
    
