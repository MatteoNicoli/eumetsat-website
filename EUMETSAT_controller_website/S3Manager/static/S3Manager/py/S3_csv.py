# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 16:07:44 2021

@author: matte
"""
# =============================================================================
# 
# =============================================================================
import datetime
import pandas as pd










# =============================================================================
# 
# =============================================================================
def fcn_S3_events_csv(FDF_Pass, SetUp, LogbookIDs ):
    DS = datetime.datetime.strptime(SetUp['Date_start'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    DE = datetime.datetime.strptime(SetUp['Date_end'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    
    
    #rgb(255,0,0)
    color_style = '<head><style>markgreen { background-color: lime; color: black;} markred {background-color: red;color: black;}</style></head>'
    color_style = ''
    def colortext(color, text):
        if (color=='red'):
            textcolor = '<span style="background-color:#FF0000">'+text+'</span>'
        if (color=='green'):
            textcolor = '<span style="background-color:#00FF00">'+text+'</span>'
        return textcolor
    #end def
    csv_DateT1 = datetime.datetime(1900, 1, 1, 00, 00, 0, 0*1000)
    csv_DateT = [csv_DateT1]
    csv_Date = ['#']
    csv_Sev = ['TRUE']
    csv_Type = ['D']
    csv_Group = ['']
    csv_Text = ['']
    csv_LogbookID= ['']
    
    SevText = 'Info'
    S3_SpaConLogBook = LogbookIDs.loc[ 'S3 Space' , 'ID' ]
    S3_GrndLogBook = LogbookIDs.loc[ 'S3 Ground' , 'ID' ]
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    if (SetUp['Daily_Events'] == True):
        i_D = datetime.datetime(DS.year, DS.month, DS.day, 1, 0, 0, 0*1000)
        flag = True
        
        TypeText = 'S3A/B'
        GroupSTCText = 'STC & NTC Monitoring'
        GroupDIFText = 'Dissemination Monitoring'
        
        
        while (flag):
            if (i_D>=DS and i_D<DE):
                #STC/NTC
                csv_DateT.append(i_D)
                csv_Date.append(i_D.strftime("%Y-%m-%dT%H:%M:%SZ"))
                csv_Sev.append(SevText)
                csv_Type.append(GroupSTCText)
                csv_Group.append(TypeText)
                
                #
                Date_STCNTC_1 = (i_D-datetime.timedelta(hours=24*2)).strftime("%d/%m/%Y")
                Date_STCNTC_2 = (i_D-datetime.timedelta(days=24)).strftime("%d/%m/%Y")
                STCText ='Production Date: '+Date_STCNTC_1+'<br>STC: '+colortext('green', 'OK')+' &nbsp '+colortext('red', 'GAPS')+'<br>'
                if (i_D.hour == 5 or i_D.hour==17):
                    STCText = STCText + '<br>Optical NTC: '+colortext('green', 'OK')+' &nbsp '+colortext('red', 'GAPS')+'<br><br><br>'+'Production Date: '+Date_STCNTC_2+'<br>Topolographical NTC: '+colortext('green', 'OK')+' &nbsp '+colortext('red', 'GAPS')+'<br>'
                #end if
                
                csv_Text.append('<html>'+color_style+'<body>'+STCText+'</body></html>')
                csv_LogbookID.append(S3_GrndLogBook)
                
                #DIF
                csv_DateT.append(i_D)
                csv_Date.append(i_D.strftime("%Y-%m-%dT%H:%M:%SZ"))
                csv_Sev.append(SevText)
                csv_Type.append(GroupDIFText)
                csv_Group.append(TypeText)
                csv_Text.append('<html>'+color_style+'<body>'+'SMART checks for NRT / STC / NTC /BUFR: '+colortext('green', 'OK')+' &nbsp '+colortext('red', 'GAPS')+''+'</body></html>')
                csv_LogbookID.append(S3_GrndLogBook)
            #end if
            
            if (i_D>=DE):
                flag = False
            #end if
            i_D = i_D + datetime.timedelta(hours=4)
        #end while
    #end if
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    for i_FDF_Pass_M in range(0,FDF_Pass.shape[0]):
        indx = i_FDF_Pass_M
        DumpAnt = FDF_Pass.loc[indx,('DumpAnt')]
        SC = FDF_Pass.loc[indx,('SC')]
        SC_str= 'S'+SC[-2:]
        nOrbit = FDF_Pass.loc[indx,('nOrbit')]
        nOrbit_str = str( nOrbit )
        nSB = FDF_Pass.loc[indx,('Pass')]
        
        if (nSB!='X'):
            #DumpAnt = SDumpAnt
            AOS = FDF_Pass.loc[indx,('AOS0')]
            Mid = FDF_Pass.loc[indx,('Mid')]
            LOS = FDF_Pass.loc[indx,('LOS0')]
            
            
            Text_Spa_S_AOS = '<b>AOS - '+AOS.strftime("%H:%M")+'</b><br>'
            Text_Spa_S_LOS = '<br><b>LOS - '+LOS.strftime("%H:%M")+'</b><br>'
            Text_Spa_S_1 = '<br>KMF unlocked: '+colortext('green', 'YES')+' &nbsp '+colortext('red', 'NO')+'<br>'
            Text_Spa_S_2 = 'Pre-pass checks: '+colortext('green', 'OK')+' &nbsp '+colortext('red', 'GAPS')+'<br>'
            Text_Spa_S_3 = 'Orbit #: '+nOrbit_str+'<br>'
            Text_Spa_S_4 = 'G/S: '+DumpAnt+'<br>'
            Text_Spa_S_5 = 'NOP: '+colortext('green', 'YES')+' &nbsp '+colortext('red', 'NO')+'<br>'
            Text_Spa_S_6 = 'CELT: '+colortext('green', 'YES')+' &nbsp '+colortext('red', 'NO')+'<br>'
            Text_Spa_S_7 = 'HKTM dump: '+colortext('green', 'YES')+' &nbsp '+colortext('red', 'NO')+'<br>'
            if ( nSB.find('1') != -1 ):
                Text_Spa_S_8 = 'Shift TC Timeout: YES<br>'
            else:
                Text_Spa_S_8 = 'Shift TC Timeout: NO<br>'
            Text_Spa_S_9 = 'Scheduled Activities: NO YES<br>'
            Text_Spa_S_10 = 'S/C: '+colortext('green', 'OK')+' &nbsp '+colortext('red', 'GAPS')+'<br>'
            Text_Spa_S_11 = 'TM Drops: '+colortext('green', 'NO')+' &nbsp '+colortext('red', 'YES')+''
            Text_Spa_S = Text_Spa_S_AOS + Text_Spa_S_LOS + Text_Spa_S_1+Text_Spa_S_2+Text_Spa_S_3+Text_Spa_S_4+Text_Spa_S_5+Text_Spa_S_6+Text_Spa_S_7+Text_Spa_S_8+Text_Spa_S_9+Text_Spa_S_10+Text_Spa_S_11
            #SpaCon
            csv_DateT.append(LOS)
            csv_Date.append(LOS.strftime("%Y-%m-%dT%H:%M:%SZ"))
            csv_Sev.append(SevText)
            csv_Type.append('Routine S-Band pass')
            csv_Group.append(SC_str)
            csv_Text.append('<html>'+color_style+'<body>'+Text_Spa_S+'</body></html>')
            csv_LogbookID.append(S3_SpaConLogBook)
            
            
            
            
# =============================================================================
#             i_D_G = Mid-datetime.timedelta(minutes=5)#+datetime.timedelta(minutes=28)
#             #i_D_S = Mid-datetime.timedelta(minutes=3)+datetime.timedelta(minutes=28)
#             i_D_S = Mid-datetime.timedelta(minutes=5)+datetime.timedelta(minutes=15)
# =============================================================================
            
            i_D = AOS+datetime.timedelta(minutes=20)
            
            csv_DateT.append(i_D)
            csv_Date.append(i_D.strftime("%Y-%m-%dT%H:%M:%SZ"))
            csv_Sev.append(SevText)
            csv_Type.append('S/B replayed')
            csv_Group.append(SC_str)
            Text = 'Orbit #: '+nOrbit_str
            csv_Text.append('<html>'+color_style+'<body>'+Text+'</body></html>')
            csv_LogbookID.append(S3_SpaConLogBook)
            
# =============================================================================
#             #i_D = Mid-datetime.timedelta(minutes=3)+datetime.timedelta(minutes=28)
#             csv_DateT.append(i_D_S)
#             csv_Date.append(i_D_S.strftime("%Y-%m-%dT%H:%M:%SZ"))
#             csv_Sev.append(SevText)
#             csv_Type.append('X/B replayed')
#             csv_Group.append(SC_str)
#             Text = 'Orbit #: '+nOrbit_str
#             csv_Text.append('<html>'+color_style+'<body>'+Text+'</body></html>')
#             csv_LogbookID.append(S3_SpaConLogBook)
#             
#             # GrndCon
#             csv_DateT.append(i_D_G)
#             csv_Date.append(i_D_G.strftime("%Y-%m-%dT%H:%M:%SZ"))
#             csv_Sev.append(SevText)
#             csv_Type.append('X/B Dump')
#             csv_Group.append(SC_str)
#             Text = 'Orbit #: '+nOrbit_str+'<br>X/B Dump Rxd'
#             csv_Text.append('<html>'+color_style+'<body>'+Text+'</body></html>')
#             csv_LogbookID.append(S3_GrndLogBook)
# =============================================================================
        else:
            #DumpAnt = XDumpAnt
            StartDumpTime = FDF_Pass.loc[indx,('AOS5')] + (FDF_Pass.loc[indx,('LOS5')]-FDF_Pass.loc[indx,('AOS5')])/2 - datetime.timedelta(seconds=420/2)
            
            i_D_G = StartDumpTime#+datetime.timedelta(minutes=28)
            #i_D_S = Mid-datetime.timedelta(minutes=3)+datetime.timedelta(minutes=28)
            
            Mid = FDF_Pass.loc[indx,('Mid')]
            i_D_G = Mid-datetime.timedelta(minutes=5)#+datetime.timedelta(minutes=28)
            #i_D_S = Mid-datetime.timedelta(minutes=3)+datetime.timedelta(minutes=28)
            
            
            
            i_D_S = Mid-datetime.timedelta(minutes=5)+datetime.timedelta(minutes=15)
            
            #SpaCon
            csv_DateT.append(i_D_S)
            csv_Date.append(i_D_S.strftime("%Y-%m-%dT%H:%M:%SZ"))
            csv_Sev.append(SevText)
            csv_Type.append('X/B replayed')
            csv_Group.append(SC_str)
            Text = 'Orbit #: '+nOrbit_str
            csv_Text.append('<html>'+color_style+'<body>'+Text+'</body></html>')
            csv_LogbookID.append(S3_SpaConLogBook)
            
            # GrndCon
            csv_DateT.append(i_D_G)
            csv_Date.append(i_D_G.strftime("%Y-%m-%dT%H:%M:%SZ"))
            csv_Sev.append(SevText)
            csv_Type.append('X/B Dump')
            csv_Group.append(SC_str)
            Text = 'Orbit #: '+nOrbit_str+'<br>X/B Dump Rxd'
            csv_Text.append('<html>'+color_style+'<body>'+Text+'</body></html>')
            csv_LogbookID.append(S3_GrndLogBook)
        #end if
    #end for
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    datacolum = ['Date_T','Date_str','Sev','Type','Group','Text','LogBook_ID']
    data = {'Date_T': csv_DateT,
            'Date_str': csv_Date,
            'Sev': csv_Sev,
            'Type': csv_Type,
            'Group': csv_Group,
            'Text': csv_Text,
            'LogBook_ID': csv_LogbookID
            }
    
    Data_csv_vet = pd.DataFrame (data, columns = datacolum ).sort_values(by=['Date_T']).reset_index().drop(columns='index')
    
    
    filename = 'S3_Batch_'+DS.strftime('%Y_%j_%H_%M_%S')
    return Data_csv_vet, filename
#end def










