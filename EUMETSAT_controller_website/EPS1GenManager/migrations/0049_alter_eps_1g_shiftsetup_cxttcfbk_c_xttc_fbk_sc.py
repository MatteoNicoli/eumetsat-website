# Generated by Django 3.2.5 on 2021-11-20 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EPS1GenManager', '0048_auto_20211120_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eps_1g_shiftsetup_cxttcfbk',
            name='C_XTTC_FBK_SC',
            field=models.CharField(choices=[('METOP-A', 'MetOp-A'), ('METOP-B', 'MetOp-B'), ('METOP-C', 'MetOp-C'), ('NOAA 19', 'N19'), ('NOAA 18', 'N18')], max_length=7),
        ),
    ]
