# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 17:36:58 2021

@author: Nicoli
"""
# =============================================================================
# 
# =============================================================================
import datetime
import calendar
import pandas as pd
import requests






# =============================================================================
# 
# =============================================================================
def Solar_Legal_Clock( anno ):
    month = calendar.monthcalendar(anno, 3)
    giorno_di_marzo = max(month[-1][calendar.SUNDAY], month[-2][calendar.SUNDAY])
    month = calendar.monthcalendar(anno, 10)
    giorno_di_ottobre = max(month[-1][calendar.SUNDAY], month[-2][calendar.SUNDAY])
    return giorno_di_marzo, giorno_di_ottobre
# end def










def DateSolar( Date ):
    D_M, D_O = Solar_Legal_Clock(Date.year)
    Date_M = datetime.datetime(Date.year, 3, D_M, 0, 0, 0, 0*1000)
    Date_M1 = datetime.datetime(Date.year, 3, D_M, 0, 0, 0, 0*1000) - datetime.timedelta( hours=24 )
    Date_O = datetime.datetime(Date.year, 10, D_O, 0, 0, 0, 0*1000)
    Date_O1 = datetime.datetime(Date.year, 10, D_O, 0, 0, 0, 0*1000) - datetime.timedelta( hours=24 )
    
    hour_shift = datetime.timedelta( hours=1 )
    if ( Date >= Date_M and Date < Date_O ):
        hour_shift = datetime.timedelta( hours=2 )
    #end if
    
    if ( Date == Date_M1 ):
        hour_shift_2 = hour_shift + datetime.timedelta( hours=1 )
    elif ( Date == Date_O1 ):
        hour_shift_2 = hour_shift - datetime.timedelta( hours=1 )
    else:
        hour_shift_2 = hour_shift
    #end if

    return hour_shift, hour_shift_2
#end def











def Date2Shift(Mission, Date, Sh, hour_shift, hour_shift_2):
    
    ESstart = 0
    ESend = 0
    if (Mission=='EPS1'):
        ESstart = 30
        ESend = 15
    #end if
    
    if ( Sh == 'M' ):
        date_s = datetime.datetime(Date.year, Date.month, Date.day, 6, 0, 0, 0*1000)
        date_e = datetime.datetime(Date.year, Date.month, Date.day, 12, 0, 0, 0*1000) + datetime.timedelta(minutes=ESstart)
    elif ( Sh == 'E' ):
        date_s = datetime.datetime(Date.year, Date.month, Date.day, 12, 00, 0, 0*1000) + datetime.timedelta(minutes=ESstart)
        date_e = datetime.datetime(Date.year, Date.month, Date.day, 20, 0, 0, 0*1000) + datetime.timedelta(minutes=ESend)
    elif ( Sh == 'N' ):
        date_s = datetime.datetime(Date.year, Date.month, Date.day, 20, 0, 0, 0*1000) + datetime.timedelta(minutes=ESend)
        date_e = datetime.datetime(Date.year, Date.month, Date.day, 6, 0, 0, 0*1000) + datetime.timedelta(hours=24)
    #end if
    
    date_s = date_s - hour_shift
    if ( Sh == 'N' ):
        date_e = date_e - hour_shift_2
    else:
        date_e = date_e - hour_shift
    
    return date_s, date_e
#end def











def Date_Shift( Mission, Datetime_UTC ):
    Date = datetime.datetime(Datetime_UTC.year, Datetime_UTC.month, Datetime_UTC.day, 0, 0, 0, 0*1000)
    hour_shift, hour_shift_2 = DateSolar(Date)

    Datetime_local = Datetime_UTC + hour_shift

    if ( Datetime_local.hour >= 5 and Datetime_local.hour < 11 ):
        Sh = 'M'
    elif( Datetime_local.hour >= 11 and Datetime_local.hour <= 19 ):
        Sh = 'E'
    else:
        Sh = 'N'
    #end if

    date_s, date_e = Date2Shift(Mission, Date, Sh, hour_shift, hour_shift_2)

    Shift_Info = [ Sh, date_s, date_e ]
    return Shift_Info
#end def











def strDate_Shift(Mission, strDate, Sh ):
    Date = datetime.datetime.strptime(strDate, "%Y-%m-%d")
    
    hour_shift, hour_shift_2 = DateSolar(Date)
    date_s, date_e = Date2Shift(Mission, Date, Sh, hour_shift, hour_shift_2)

    strdate_s = date_s.strftime("%Y-%m-%dT%H:%M:%S")
    strdate_e = date_e.strftime("%Y-%m-%dT%H:%M:%S")
    return strdate_s, strdate_e
#end def











def strYearCW2Date(year, CW ):
    d = str(year)+"-W"+str(CW)
    date_s = datetime.datetime.strptime(d + '-1', '%G-W%V-%u')
    date_e = date_s + datetime.timedelta(hours=7*24) #- datetime.timedelta(seconds=1)

    strdate_s = date_s.strftime("%Y-%m-%dT%H:%M:%S")
    strdate_e = date_e.strftime("%Y-%m-%dT%H:%M:%S")
    return strdate_s, strdate_e
#end def











def Events2csv(fcn_MPF, MPF_Pass, SetUp, ToUberlog, LogbookIDs ):
    
    Batch_csv, filename = fcn_MPF( MPF_Pass, SetUp, LogbookIDs )
    
    #
    DS = SetUp['Date_start']
    DE = SetUp['Date_end']
    
    #
# =============================================================================
#     indexNames = Batch_csv[ Batch_csv['Date_T'] < DS ].index
#     Batch_csv.drop(indexNames , inplace=True)
#     indexNames = Batch_csv[ Batch_csv['Date_T'] > DE ].index
#     Batch_csv.drop(indexNames , inplace=True)
#     Batch_csv = Batch_csv.reset_index()
#     Batch_csv = Batch_csv.drop(columns='index')
# =============================================================================
    # Batch_csv = Batch_csv[ 
    #     ( Batch_csv['Date_T'] > DS ) &
    #     ( Batch_csv['Date_T'] < DE )
    #     ]
    # Batch_csv = Batch_csv.reset_index()
    # Batch_csv = Batch_csv.drop(columns='index')
    
    
    #
    Batch_csv = Batch_csv.drop(columns='Date_T')
    
    Batch_info = {
        'flag': False,
        'Name': False,
        'Data': Batch_csv,
        }
    
    if ( ToUberlog['ToUberlog'] == 'csv' ):
        Batch_info = {
            'flag': True,
            'Name': filename,
            'Data': Batch_csv,
            }
        
# =============================================================================
#         Batch_csv.to_csv (
#             #r''+file_Path_Name_csv,
#             path_or_buf = response,
#             index = False,
#             header = False,
#             )
# =============================================================================
    else:
        #
        Batch = Batch_csv.T.to_dict().values()
        Auth = ( 
            ToUberlog['Uberlog_Username'],
            ToUberlog['Uberlog_passwd'],
            )
        batch_name = "Batch_" + DS.strftime("%Y-%j-%H:%M:%S")
        
        POST_Batch(Auth, Batch, batch_name)
    #end if
    
    return Batch_info
#end def



















# =============================================================================
# API uberlog entries action
# =============================================================================
def GET_Entries(Auth, SearchCriteria):
    URL = "http://uberlog.opscloud.eumetsat.int:8080/uberlog/api/v2/entries/?"+('&'.join( SearchCriteria ))
    
    # sending get request and saving the response as response object
    r = requests.get(
        url = URL,
        auth=Auth,
        )
    
    # extracting data in json format
    data = r.json()
    
    return data
#end def

def GET_Entrie(Auth, entrie_id):
    URL = "http://uberlog.opscloud.eumetsat.int:8080/uberlog/api/v2/entries/"
    
    # sending get request and saving the response as response object
    r = requests.get(
        url = URL+str(entrie_id),
        auth=Auth,
        )
    
    # extracting data in json format
    data = r.json()
    
    return data
#end def

def POST_Entrie(Auth, payload):
    URL = "http://uberlog.opscloud.eumetsat.int:8080/uberlog/api/v2/entries"
# =============================================================================
#     payload ={
#         "logbook":      "5b0e8a94a585c614bac88a0c",
#         "severity":     "Entry",
#         "text":         "Hello World<br>ciao",
#         "eventDate":    "2021-04-08T09:41:04Z",
#         "group":        "PASS-002"
#         }
# =============================================================================
    
    # sending post request and saving response as response object
    r = requests.post(
        url = URL,
        auth=Auth,
        json=payload,
        )
#end def


# =============================================================================
# API uberlog batchs action
# =============================================================================
def GET_Batchs(Auth):
    URL = "http://uberlog.opscloud.eumetsat.int:8080/uberlog/api/v2/batches/"
        
    # sending get request and saving the response as response object
    r = requests.get(
        url = URL,
        auth=Auth,
        )
    
    # extracting data in json format
    data = r.json()
    
    return data
#end def

def POST_Batch(Auth, Batch, batch_name):
    URL = "http://uberlog.opscloud.eumetsat.int:8080/uberlog/api/v2/batches"
    payload = {
        "autoTime": "D",
        "favorite": "true",
        "name":     batch_name,
        "entries":  Batch,
        }
    
    # sending post request and saving response as response object
    r = requests.post(
        url = URL,
        auth=Auth,
        json=payload,
        )
#end def

def DELETE_Batch(Auth, batch_id):
    URL = "http://uberlog.opscloud.eumetsat.int:8080/uberlog/api/v2/batches/"
    
    # sending delete request and saving the response as response object
    r = requests.delete(
        url = URL+str(batch_id),
        auth=Auth,
        )
#end def
    