# =============================================================================
# MODULE - django
# =============================================================================
from django.urls import path
from . import views

# =============================================================================
# MODULE - django-ajax
# =============================================================================
# from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

# Create your urls here.

urlpatterns = [
    path('', views.EPS1elogbookGenerator_view.as_view(), name='EPS1Manager'),
    # path('EPS1elogbookGenerator', views.EPS1elogbookGenerator_view2, name='EPS1elogbooGenerator'),
    path('EPS1elogbookGenerator', views.EPS1elogbookGenerator_view.as_view(), name='EPS1elogbooGenerator'),
    path('EPS1ReportGenerator', views.EPS1ReportGenerator_view.as_view(), name='EPS1ReportGenerator'),
]
# urlpatterns += patterns('',
#    url(r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),)
	
# urlpatterns += staticfiles_urlpatterns()