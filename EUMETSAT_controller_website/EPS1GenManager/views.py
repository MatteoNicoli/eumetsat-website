# =============================================================================
# MODULE - django
# =============================================================================
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from django.http import JsonResponse, HttpResponseRedirect, Http404, HttpResponse
from django.utils import timezone

# models & forms
from . import forms
from . import models

# =============================================================================
# MODULE - useful
# =============================================================================
import os, sys
DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))

import datetime
import csv

# =============================================================================
# MODULE - customized
# =============================================================================
from static.py.Sup_fcns import *

# Create your views here.

# def EPS1GenManager_view(request):
#     obj = models.Product.objects.get(id=1)
    
#     context = {
#         'contx_object': obj,
#     }
#     return render(request, 'EPS1GenManager/EPS1GenManager.html', context)





# def EPS1GenManager_view(request):
#     form = forms.Product_form(request.POST or None)
#     if form.is_valid():
#         form.save()

#     context = {
#         'contx_form': form,
#     }
#     return render(request, 'EPS1GenManager/EPS1GenManager.html', context)





# def EPS1GenManager_view(request):
#     my_form = forms.Raw_Product_form()
#     if ( request.method == "POST" ):
#         my_form = forms.Raw_Product_form(request.POST)
#         if ( my_form.is_valid() ):
#             print(my_form.cleaned_data)
#             #models.Product.objects.create(**my_form.cleaned_data)

#             # Redirect to THIS view, assuming it lives in 'some app'
#             #return HttpResponseRedirect(reverse("EPS1GenManager.views.EPS1GenManager_view"))
#         else:
#             print(my_form.errors)
#     context = {
#         'contx_form': my_form,
#     }
#     return render(request, 'EPS1GenManager/EPS1GenManager.html', context)






# def EPS1GenManager_view(request):
#     initial_data = {
#         'Shift': 'E', 
#     }
#     initial_data_ToUberlog = {
#         'ToUberlog': 'import', 
#     }

#     now_utc =datetime.datetime.now()
#     now_utc = timezone.now()
#     # print(now_local)
#     Shift_Info = Date_Shift(now_utc)
#     Sh = Shift_Info[2]
#     date_s = Shift_Info[3]
#     date_e = Shift_Info[4]

#     initial_data_EPS1G = {
#         'Daily_Events': True,
#         'Date_Shift': date_s.strftime("%Y-%m-%d"),
#         'Shift': Sh,
#         'Date_start': date_s.strftime("%Y-%m-%dT%H:%M:%S"),
#         'Date_end': date_e.strftime("%Y-%m-%dT%H:%M:%S"),
#     }




#     # obj = models.Product.objects.get(id=1)
    
#     # obj = get_object_or_404( models.Product, id=id)
    
#     # try:
#     #     obj = models.Product.objects.get(id=id)
#     # except:
#     #     raise Htt404

#     # my_form = forms.Product_form(request.POST or None, initial=initial_data, instance=obj)
    
#     my_form = forms.Product_form(request.POST or None, )
#     my_form_ToUberlog = forms.EPS_1G_Operator_form(request.POST or None, initial=initial_data_ToUberlog)
#     my_form_EPS1G = forms.EPS_1G_ShiftSetUp_form(request.POST or None, initial=initial_data_EPS1G)
    

#     if ( my_form_EPS1G.is_valid() and my_form_ToUberlog.is_valid() ):
#         print(my_form_ToUberlog.cleaned_data)
#         print(my_form_EPS1G.cleaned_data)
#         print(my_form_EPS1G.cleaned_data['Date_start'].strftime("%Y-%j-%H:%M:%S"))
        
#         # my_form.save()
#         # my_form = forms.Product_form()
        

#         if ( my_form_ToUberlog.cleaned_data['ToUberlog'] == 'csv' ):
#             # Students name
#             NAME = ['Riya','Suzzane','George','Zoya','Smith','Henry']
#             # QUIZ Subject
#             SUBJECT = ['CHE','PHY','CHE','BIO','ENG','ENG']
#             # Create the HttpResponse object with the appropriate CSV header.
#             response = HttpResponse(content_type='text/csv')
#             response['Content-Disposition'] = 'attachment; filename=quiz.csv'
#             # Create the CSV writer using the HttpResponse as the "file"
#             writer = csv.writer(response)
#             writer.writerow(['Student Name', 'Quiz Subject'])
#             for (name, sub) in zip(NAME, SUBJECT):
#                 writer.writerow([name, sub])
#             return response
#         else:
#             # Redirect to THIS view, assuming it lives in 'some app'
#             return HttpResponseRedirect(request.path)
#         #end if 
        
#     else:
#         print('ciao')
#         print(my_form.errors)
#     #end if
    
#     context = {
#         'contx_form': my_form,
#         'contx_form_ToUberlog': my_form_ToUberlog,
#         'contx_form_EPS1G': my_form_EPS1G,
#     }
#     return render(request, 'EPS1GenManager/EPS1GenManager.html', context=context)








def ShiftGen_Initial_data():
    initial_data_ToUberlog = {
        'ToUberlog': 'import', 
    }

    now_utc =datetime.datetime.now()
    now_utc = timezone.now()
    Mission = models.EPS1_Satellite.Mission

    Shift_Info = Date_Shift(Mission, now_utc)
    Sh = Shift_Info[0]
    date_s = Shift_Info[1]
    date_e = Shift_Info[2]

    initial_data_EPS1 = {
        'Antenna': 'CDA1',
        'SC': ['METOP-A','METOP-B','METOP-C'],
        'Daily_Events': True,
        'Date_Shift': date_s.strftime("%Y-%m-%d"),
        'Shift': Sh,
        'Date_start': date_s.strftime("%Y-%m-%dT%H:%M:%S"),
        'Date_end': date_e.strftime("%Y-%m-%dT%H:%M:%S"),
    }
    MS_Antenna = 'CDA2'
    if ( initial_data_EPS1['Antenna']=='CDA2' ):
        MS_Antenna = 'CDA1'
    #end if
    initial_data_MS = {
        'MS_Antenna': MS_Antenna,
        'MS_SC': ['METOP-A','METOP-B','METOP-C', 'NOAA 19','NOAA 18'],
    }
    initial_data_CXTTCFBK = {
        'C_XTTC_FBK_Antenna': 'FBK_',
        'C_XTTC_FBK_Extend': True,
    }

    dict_initial_data = {
        'Mission': Mission,
        'ToUberlog': initial_data_ToUberlog,
        'EPS1G': initial_data_EPS1,
        'MS': initial_data_MS,
        'CXTTCFBK': initial_data_CXTTCFBK,
    }
    return dict_initial_data
#end def










def EPS1elogbookGenerator_view2(request):
    dict_initial_data = ShiftGen_Initial_data()

    form = forms.Product_form(request.POST or None, )
    form_ToUberlog = forms.EPS_1G_Operator_form(request.POST or None, initial=dict_initial_data['ToUberlog'])
    form_EPS1 = forms.EPS_1G_ShiftSetUp_form(request.POST or None, initial=dict_initial_data['EPS1G'])
    form_EPS1_MS = forms.EPS_1G_ShiftSetUp_MS_form(request.POST or None, initial=dict_initial_data['MS'])
    form_EPS1_CXTTCFBK = forms.EPS_1G_ShiftSetUp_CXTTCFBK_form(request.POST or None,)

    if ( request.is_ajax() ):
        EPS1_Date_Shift_val=request.GET.get('EPS1_Date_Shift_val')
        EPS1_Shift_val=request.GET.get('EPS1_Shift_val')
        EPS1_CXTTCFBK_Antenna_val = request.GET.get('EPS1_CXTTCFBK_Antenna_val')

        #
        if ( EPS1_Date_Shift_val != None and EPS1_Shift_val != None ):
            strdate_s, strdate_e = strDate_Shift(dict_initial_data['Mission'], EPS1_Date_Shift_val, EPS1_Shift_val )
            # ajax context
            context_ajax = {
                'date_s': strdate_s,
                'date_e': strdate_e,
            }
            return JsonResponse(context_ajax, status=200)
        #end if
    #end if

    if ( form_EPS1.is_valid() and form_ToUberlog.is_valid() and form_EPS1_MS.is_valid() and form_EPS1_CXTTCFBK.is_valid() ):
            print(form_ToUberlog.cleaned_data)
            print(form_EPS1.cleaned_data)
            print(form_EPS1_MS.cleaned_data)
            print(form_EPS1_CXTTCFBK.cleaned_data)
            print(form_EPS1.cleaned_data['Date_start'].strftime("%Y-%j-%H:%M:%S"))
            
            if ( form_ToUberlog.cleaned_data['ToUberlog'] == 'csv' ):
                # Create the HttpResponse object with the appropriate CSV header.
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename='+filename+'.csv'

                # Students name
                NAME = ['Riya','Suzzane','George','Zoya','Smith','Henry']
                # QUIZ Subject
                SUBJECT = ['CHE','PHY','CHE','BIO','ENG','ENG']
                # Create the CSV writer using the HttpResponse as the "file"
                writer = csv.writer(response)
                writer.writerow(['Student Name', 'Quiz Subject'])
                for (name, sub) in zip(NAME, SUBJECT):
                    writer.writerow([name, sub])

                return response
            #end if
            # Redirect to THIS view, assuming it lives in 'some app'
            return redirect('EPS1elogbooGenerator')
    
    # template & context
    template = 'EPS1GenManager/EPS1ShiftGenerator.html'
    context = {
        'contx_form': form,
        'contx_form_ToUberlog': form_ToUberlog,
        'contx_form_EPS1G': form_EPS1,
        'contx_form_EPS1G_MS': form_EPS1_MS,
        'contx_form_EPS1G_CXTTCFBK': form_EPS1_CXTTCFBK,
    }
    return render(request, template, context=context)
#end def





class EPS1elogbookGenerator_view(View):
    initial_data = {
        'Shift': 'E', 
    }

    # =============================================================================
    # GET def
    # =============================================================================
    def get(self, request):
        dict_InitialData = ShiftGen_Initial_data()

        form_ToUberlog = forms.EPS_1G_Operator_form(initial=dict_InitialData['ToUberlog'])
        form_EPS1 = forms.EPS_1G_ShiftSetUp_form(initial=dict_InitialData['EPS1G'])
        form_EPS1_MS = forms.EPS_1G_ShiftSetUp_MS_form(initial=dict_InitialData['MS'])
        form_EPS1_CXTTCFBK = forms.EPS_1G_ShiftSetUp_CXTTCFBK_form(initial=dict_InitialData['CXTTCFBK'])
        
        if ( request.is_ajax() ):
            EPS1_Date_Shift_val=request.GET.get('EPS1_Date_Shift_val')
            EPS1_Shift_val=request.GET.get('EPS1_Shift_val')
            # EPS1_CXTTCFBK_Antenna_val = request.GET.get('EPS1_CXTTCFBK_Antenna_val')

            #
            if ( EPS1_Date_Shift_val != None and EPS1_Shift_val != None ):
                strdate_s, strdate_e = strDate_Shift(dict_InitialData['Mission'], EPS1_Date_Shift_val, EPS1_Shift_val )
                # ajax context
                context_ajax = {
                    'date_s': strdate_s,
                    'date_e': strdate_e,
                }
                return JsonResponse(context_ajax, status=200)
            #end if
        #end if

        # template & context
        template = 'EPS1GenManager/EPS1ShiftGenerator.html'
        context = {
            'contx_form_ToUberlog': form_ToUberlog,
            'contx_form_EPS1G': form_EPS1,
            'contx_form_EPS1G_MS': form_EPS1_MS,
            'contx_form_EPS1G_CXTTCFBK': form_EPS1_CXTTCFBK,
        }
        return render(request, template, context=context)
    #end def











    # =============================================================================
    # POST def
    # =============================================================================
    def post(self, request):
        if ( 'btn_EPS1_btnToUberlog' in request.POST ):
            # form = forms.Product_form(request.POST or None, )
            form_ToUberlog = forms.EPS_1G_Operator_form(request.POST or None, )
            form_EPS1 = forms.EPS_1G_ShiftSetUp_form(request.POST or None, )
            form_EPS1_MS = forms.EPS_1G_ShiftSetUp_MS_form(request.POST or None,)
            form_EPS1_CXTTCFBK = forms.EPS_1G_ShiftSetUp_CXTTCFBK_form(request.POST or None,)

            if ( form_EPS1.is_valid() and form_ToUberlog.is_valid() and form_EPS1_MS.is_valid() and form_EPS1_CXTTCFBK.is_valid() ):
                print(form_ToUberlog.cleaned_data)
                print(form_EPS1.cleaned_data)
                print(form_EPS1_MS.cleaned_data)
                print(form_EPS1_CXTTCFBK.cleaned_data)
                print(form_EPS1.cleaned_data['Date_start'].strftime("%Y-%j-%H:%M:%S"))
                
                # my_form.save()
                # my_form = forms.Product_form()
                
                if ( form_ToUberlog.cleaned_data['ToUberlog'] == 'csv' ):
                    # Create the HttpResponse object with the appropriate CSV header.
                    response = HttpResponse(content_type='text/csv')
                    response['Content-Disposition'] = 'attachment; filename='+"EPS1"+'.csv'

                    # Students name
                    NAME = ['Riya','Suzzane','George','Zoya','Smith','Henry']
                    # QUIZ Subject
                    SUBJECT = ['CHE','PHY','CHE','BIO','ENG','ENG']
                    # Create the CSV writer using the HttpResponse as the "file"
                    writer = csv.writer(response)
                    writer.writerow(['Student Name', 'Quiz Subject'])
                    for (name, sub) in zip(NAME, SUBJECT):
                        writer.writerow([name, sub])
                    


                    return response
                #end if
            #end if
        #end if

        return redirect('EPS1elogbooGenerator')
    #end def
#end class









































def ReportGen_Initial_data():
    initial_data_ToUberlog = {
        'ToUberlog': 'import', 
    }

    now_utc =datetime.datetime.now()
    now_utc = timezone.now()
    Mission = 'EPS'

    Shift_Info = Date_Shift(Mission, now_utc)
    Sh = Shift_Info[0]
    date_s = Shift_Info[1]
    # date_e = Shift_Info[2]

    initial_data_EPS1_RG = {
        'year': date_s.strftime("%Y"),
        'CW': date_s.isocalendar()[1],
        'Date_Shift': date_s.strftime("%Y-%m-%d"),
        'Shift': Sh,
        # 'Date_start': date_s.strftime("%Y-%m-%dT%H:%M:%S"),
        # 'Date_end': date_e.strftime("%Y-%m-%dT%H:%M:%S"),
    }
    dict_initial_data = {
        'Mission': Mission,
        'ToUberlog': initial_data_ToUberlog,
        'EPS1_RG': initial_data_EPS1_RG,
    }
    return dict_initial_data
#end def











class EPS1ReportGenerator_view(View):
    # =============================================================================
    # 
    # =============================================================================
    def get(self, request):
        dict_InitialData = ReportGen_Initial_data()
        
        form_ToUberlog = forms.EPS_1G_Operator_form(initial=dict_InitialData['ToUberlog'])
        form_EPS1_RG = forms.EPS_1G_ReportSetUp_form(initial=dict_InitialData['EPS1_RG'])

        # ajax
        if ( request.is_ajax() ):
            EPS1_RG_Date_Shift_val=request.GET.get('EPS1_RG_Date_Shift_val')
            EPS1_RG_Shift_val=request.GET.get('EPS1_RG_Shift_val')

            EPS1_RG_year_val=request.GET.get('EPS1_RG_Year_val')
            EPS1_RG_CW_val=request.GET.get('EPS1_RG_CW_val')
            # ajax context
            if ( EPS1_RG_year_val != None and EPS1_RG_CW_val != None ):
                strdate_s, strdate_e = strYearCW2Date(EPS1_RG_year_val, EPS1_RG_CW_val )
                #
                context_ajax = {
                    'date_s': strdate_s,
                    'date_e': strdate_e,
                }
                return JsonResponse(context_ajax, status=200)
            #end if
            #
            if ( EPS1_RG_Date_Shift_val != None and EPS1_RG_Shift_val != None ):
                strdate_s, strdate_e = strDate_Shift(dict_InitialData['Mission'], EPS1_RG_Date_Shift_val, EPS1_RG_Shift_val )
                # ajax context
                context_ajax = {
                    'date_s': strdate_s,
                    'date_e': strdate_e,
                }
                return JsonResponse(context_ajax, status=200)
            #end if
        #end if

        # template & context
        template = 'EPS1GenManager/EPS1ReportGenerator.html'
        context = {
            'contx_form_ToUberlog': form_ToUberlog,
            'contx_form_EPS1_RG': form_EPS1_RG,
        }
        return render(request, template, context=context)
    #end def











    # =============================================================================
    # 
    # =============================================================================
    def post(self, request):
        form_ToUberlog = forms.EPS_1G_Operator_form(request.POST or None, )
        form_EPS1_RG = forms.EPS_1G_ReportSetUp_form(request.POST or None, )

        if ( form_EPS1_RG.is_valid() and form_ToUberlog.is_valid() ):
            print(form_ToUberlog.cleaned_data)
            
            # my_form.save()
            # my_form = forms.Product_form()
            

            if ( form_ToUberlog.cleaned_data['ToUberlog'] == 'csv' ):
                # Students name
                NAME = ['Riya','Suzzane','George','Zoya','Smith','Henry']
                # QUIZ Subject
                SUBJECT = ['CHE','PHY','CHE','BIO','ENG','ENG']
                # Create the HttpResponse object with the appropriate CSV header.
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename=quiz.csv'
                # Create the CSV writer using the HttpResponse as the "file"
                writer = csv.writer(response)
                writer.writerow(['Student Name', 'Quiz Subject'])
                for (name, sub) in zip(NAME, SUBJECT):
                    writer.writerow([name, sub])
                return response
            else:
                # Redirect to THIS view, assuming it lives in 'some app'
                # return HttpResponseRedirect(request.path)
                return redirect('EPS1ReportGenerator')
            #end if
        #end if
        return redirect('EPS1ReportGenerator')
    #end def

#end class