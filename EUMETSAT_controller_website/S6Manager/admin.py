# =============================================================================
# MODULE - django
# =============================================================================
from django.contrib import admin

# 
from easy_select2 import select2_modelform

# models & forms
from . import models

# Register your models here.

class NoteAdmin(admin.ModelAdmin):
    form = select2_modelform(models.S6_ShiftSetUp)

admin.site.register(models.S6_ShiftSetUp, NoteAdmin)
admin.site.register(models.S6_Pass)
admin.site.register(models.S6_Operator)
admin.site.register(models.S6_Satellite)