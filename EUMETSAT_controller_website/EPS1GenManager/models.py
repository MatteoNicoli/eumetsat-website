# =============================================================================
# MODULE - django
# =============================================================================
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# =============================================================================
# MODULE - useful
# =============================================================================
import os, sys
DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')

import datetime
import pandas as pd

# Create your models here.






class EPS1_Satellite(models.Model):
    path_SCInfo = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','SCInfo.csv')
    if (os.path.isfile(path_SCInfo) == True):
        list_SCIDs = pd.read_csv(path_SCInfo, )
    #end if
    Mission = "EPS1"
    list_SCIDs_tmp = list_SCIDs[
        ( list_SCIDs['Mission'] == Mission ) &
        ( list_SCIDs['Comment'] <= 2 )
        ].drop(columns='Mission').drop(columns='NORAD CAT ID').drop(columns='shortcut').drop(columns='Comment')
    SC_CHOICES = tuple(list_SCIDs_tmp.itertuples(index=False, name=None))
    # SC_CHOICES = (
    #     ('METOP-A', 'MetOp-A'),
    #     ('METOP-B', 'MetOp-B'),
    #     ('METOP-C', 'MetOp-C'),
    #     ('NOAA 19', 'N19'),
    #     ('NOAA 18', 'N18'),
    # )
    name = models.CharField(max_length=50, choices=SC_CHOICES)

class EPS_1G_ShiftSetUp(models.Model):
    SideSpaCon     = models.BooleanField(blank=True, default=False)
    SideGrndCon    = models.BooleanField(blank=True, default=False)
    Antenna_CHOICES = (
        ('CDA1', 'CDA 1'),
        ('CDA2', 'CDA 2'),
    )
    Antenna = models.CharField(max_length=7, choices=Antenna_CHOICES)
    
    SC_CHOICES = EPS1_Satellite.SC_CHOICES
    SC = models.ManyToManyField(EPS1_Satellite)

    Daily_Events    = models.BooleanField(default=False)
    NOAA_BOS        = models.BooleanField(default=False)

    #
    Date_Shift      = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    SHIFT_CHOICES   = (
        ('M', 'Morning'),
        ('E', 'Evening'),
        ('N', 'Night'),
    )
    Shift           = models.CharField(max_length=7, choices=SHIFT_CHOICES)
    Date_start      = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    Date_end        = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    file_report     = models.BooleanField(default=False)
#end class











class EPS_1G_ShiftSetUp_MS(models.Model):
    MS_Date_start   = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    MS_Date_end     = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    MS_SC_CHOICES = EPS1_Satellite.SC_CHOICES
    MS_SC     = models.CharField(max_length=7, choices=MS_SC_CHOICES,  )

    MS_Antenna_CHOICES = (
        ('CDA1', 'CDA 1'),
        ('CDA2', 'CDA 2'),
        ('PAR_', 'PAR - PUNTARNS'),
        ('MAD_', 'MAD - MADRID'),
        ('ISS_', 'ISS - INUVIK'),
        ('FBK_', 'FBK - FAIRBANK'),
        ('WAL_', 'WAL - WALLOPS'),
    )
    MS_Antenna = models.CharField(max_length=7, choices=MS_Antenna_CHOICES,  )
#end class











class EPS_1G_ShiftSetUp_CXTTCFBK(models.Model):
    C_XTTC_FBK_Covis = models.BooleanField(blank=True, default=False)
    C_XTTC_FBK_SC_CHOICES = EPS1_Satellite.SC_CHOICES
    # C_XTTC_FBK_SC     = models.CharField(max_length=7, choices=C_XTTC_FBK_SC_CHOICES)
    C_XTTC_FBK_SC = models.ManyToManyField(EPS1_Satellite)

    C_XTTC_FBK_nOrbit = models.IntegerField(blank=True, null=True , validators=[MinValueValidator(0), MaxValueValidator(99999)])
    C_XTTC_FBK_Antenna_CHOICES = (
        ('PAR_', 'PAR - PUNTARNS'),
        ('MAD_', 'MAD - MADRID'),
        ('ISS_', 'ISS - INUVIK'),
        ('FBK_', 'FBK - FAIRBANK'),
        ('WAL_', 'WAL - WALLOPS'),
    )
    C_XTTC_FBK_Antenna = models.CharField(max_length=7, choices=C_XTTC_FBK_Antenna_CHOICES)
    C_XTTC_FBK_Extend = models.BooleanField(blank=True, default=False)
#end class











class EPS_1G_Pass(models.Model):
    inShift     = models.BooleanField(default=False)
    NORAD_ID    = models.IntegerField(blank=True, null=True )
    SC_NAME     = models.CharField(max_length=4, blank=True)
    SCID        = models.CharField(max_length=4, blank=True)
    nOrbit      = models.IntegerField(blank=True, null=True)
    Antenna     = models.CharField(max_length=4)
    ANX         = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    AOS0        = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    AOSM        = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    AOS5        = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    Mid         = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    LOS5        = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    LOSM        = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    LOS0        = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    

    def __str__(self):
        return str( self.SCID ) + ' - ' + str( self.nOrbit ) + ' - ' + str( self.Antenna ) 
    #end def
#end class











class EPS_1G_Operator(models.Model):
    Name            = models.CharField(max_length=120, blank=True, null=True)
    Second_Name     = models.CharField(max_length=120, blank=True, null=True)
    Surname         = models.CharField(max_length=120, blank=True, null=True)
    Second_Surname  = models.CharField(max_length=120, blank=True, null=True)
    Type_CHOICES = (
        ('controller', 'Controller'),
        ('analyst', 'Analyst'),
        ('engineer', 'Engineer'),
    )
    Type            = models.CharField(max_length=10, choices=Type_CHOICES)
    Mission         = models.CharField(max_length=120, default='EPS')
    Company         = models.CharField(max_length=120, blank=True, null=True)
    email           = models.EmailField(max_length=254, default='@external.eumetsat.int')
    email_Team      = models.EmailField(max_length=254, default='controller.LEO@eumetsat.int')

    #
    OS_Name         = models.CharField(max_length=120, blank=True, null=True)
    Uberlog_ID      = models.CharField(max_length=120, blank=True, null=True)
    Uberlog_Paswd   = models.CharField(max_length=120, default='elog')
    ToUberlog_CHOICES = (
        ('import', 'Import'),
        ('submit', 'Submit'),
        ('csv', 'Dowload (.csv)'),
    )
    ToUberlog       = models.CharField(max_length=7, choices=ToUberlog_CHOICES, default='import')
    BatchsDelete    = models.BooleanField(blank=True, default=False)

    def __str__(self):
        Name = str( self.Name ).capitalize()
        if ( self.Second_Name != None ):
            Name = Name + ' ' + str( self.Second_Name ).capitalize()
        #end if
        Surname = str( self.Surname ).capitalize()
        if ( self.Second_Surname != None ):
            Surname = Surname + ' ' + str( self.Second_Surname ).capitalize()
        #end if
        NS = Name + ' ' + Surname
        return str( self.Type ).capitalize() + ' - ' + NS
    #end def
#end class











class EPS_1G_ReportSetUp(models.Model):
    REPORT_CHOICES = (
        ('CW', 'CW'),
        ('SH', 'Shift'),
    )
    Report_type   = models.CharField(max_length=7, choices=REPORT_CHOICES, blank=True)
    year          = models.IntegerField(blank=True, null=True )
    CW            = models.IntegerField(blank=True, null=True )
    #
    Date_Shift    = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    SHIFT_CHOICES = (
        ('M', 'Morning'),
        ('E', 'Evening'),
        ('N', 'Night'),
    )
    Shift         = models.CharField(max_length=7, choices=SHIFT_CHOICES)
    Date_start    = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    Date_end      = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
#end def




















































class Product(models.Model):
    title       = models.CharField(max_length=120)
    description = models.TextField(blank=True, null=True)
    price       = models.DecimalField(decimal_places=2, max_digits=1000)
    summary     = models.TextField()
    featured    = models.BooleanField(default=False)

    date_local = datetime.datetime.now
    date_utc = datetime.datetime.utcnow
    #delta_utc_local = 2
    #if (date_local.hour>=6  and date_local.hour<12):
    #    date_s = datetime.datetime(date_utc.year, date_utc.month, date_utc.day, 6, 0, 0, 0*1000) - datetime.timedelta(seconds=delta_utc_local)
    #    date_e = datetime.datetime(date_utc.year, date_utc.month, date_utc.day, 12, 29, 59, 0*1000) - datetime.timedelta(seconds=delta_utc_local)
    #elif (date_local.hour>=12  and date_local.hour<20):
    #    date_s = datetime.datetime(date_utc.year, date_utc.month, date_utc.day, 12, 30, 0, 0*1000) - datetime.timedelta(seconds=delta_utc_local)
    #    date_e = datetime.datetime(date_utc.year, date_utc.month, date_utc.day, 20, 14, 59, 0*1000) - datetime.timedelta(seconds=delta_utc_local)
    #else:
    #    date_s = datetime.datetime(date_utc.year, date_utc.month, date_utc.day, 20, 15, 0, 0*1000) - datetime.timedelta(seconds=delta_utc_local)
    #    date_e = datetime.datetime(date_utc.year, date_utc.month, date_utc.day, 5, 59, 59, 0*1000) + datetime.timedelta(hours=24) - datetime.timedelta(seconds=delta_utc_local)
    # end if
    date_s = date_utc

    SHIFT_CHOICES = (
        ('M', 'Morning'),
        ('E', 'Evening'),
        ('N', 'Night'),
    )
    Shift       = models.CharField(max_length=7, choices=SHIFT_CHOICES)
    Date_start  = models.DateTimeField(auto_now=False, auto_now_add=False, default=date_s, blank=True)
    Date_end    = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null= True)
    email       = models.EmailField(max_length=254, default='controller.LEO@eumetsat.int')
    url         = models.URLField(max_length=200, default='')