# -*- coding: utf-8 -*-
"""
Created on Fri Nov 12 22:21:47 2021

@author: Nicoli
"""
import os, sys

DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))

from multiprocessing.pool import ThreadPool

# =============================================================================
# 
# =============================================================================
from static.FDF_Orekit.Orekit_function import *











# =============================================================================
# 
# =============================================================================
def S6_FDF_events(SetUp):
    # =============================================================================
    #     
    # =============================================================================
    DS = datetime.datetime.strptime(SetUp['Date_start'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    DE = datetime.datetime.strptime(SetUp['Date_end'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    
    # =============================================================================
    # 
    # =============================================================================
    path_SCInfo = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','SCInfo.csv')
    if (os.path.isfile(path_SCInfo) == True):
        list_SCIDs = pd.read_csv(path_SCInfo, index_col=0)
    #end if
    
    
    # =============================================================================
    #     
    # =============================================================================
    FDF_AntLoc_Tot = AntenneInfo()

    #
    frames = []
    for i_SC in SetUp['SC']:
        Antenna = 'FBK_'
        utc, earth, inertialFrame, station_frame, ElMask = Frames( FDF_AntLoc_Tot, Antenna)
        FDF_event_S6_FBK, propagator = FDF_PassGenerator(Antenna, list_SCIDs, i_SC, DS, DE, utc, inertialFrame, station_frame, ElMask )
    
        Antenna = 'KIRx'
        utc, earth, inertialFrame, station_frame, ElMask = Frames( FDF_AntLoc_Tot, Antenna)
        FDF_event_S6_KIR, propagator = FDF_PassGenerator(Antenna, list_SCIDs, i_SC, DS, DE, utc, inertialFrame, station_frame, ElMask )
        frames.append(FDF_event_S6_KIR)
        frames.append(FDF_event_S6_FBK)
    #end for
    # =============================================================================
    # 
    # =============================================================================
    
    result_test = pd.concat(frames)

    FDF_event_S6 = result_test.sort_values('TC_Duration', ascending=False).drop_duplicates('ANX').sort_values(by=[('ANX')]).reset_index().drop(columns='index')
    FDF_event_S6 = FDF_event_S6[
        ( FDF_event_S6['AOS0'] >= DS ) &
        ( FDF_event_S6['AOS0'] < DE )
        ].sort_values(by=[('ANX')]).reset_index().drop(columns='index')
    
    return FDF_event_S6

















def S6_FDF_events_gen(DIR,DIR_int,SetUp):
    pool = ThreadPool(processes=1)
    def ThreadPool_S6_FDF_events(SetUp):
        vm.attachCurrentThread()
        return S6_FDF_events(SetUp)
    #end def

    result = pool.apply_async(
        ThreadPool_S6_FDF_events,
        (SetUp,)
        )
    FDF_event_S6 = result.get()
    
    return FDF_event_S6