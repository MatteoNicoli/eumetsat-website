# =============================================================================
# MODULE - django
# =============================================================================
from django.contrib import admin
# 
from easy_select2 import select2_modelform

# models & forms
from . import models

# Register your models here.

class EPS1_elog_Admin(admin.ModelAdmin):
    form = select2_modelform(models.EPS_1G_ShiftSetUp)

class EPS1_MSelog_Admin(admin.ModelAdmin):
    form = select2_modelform(models.EPS_1G_ShiftSetUp_MS)

admin.site.register(models.EPS_1G_ShiftSetUp, EPS1_elog_Admin)
admin.site.register(models.EPS_1G_ShiftSetUp_MS, EPS1_MSelog_Admin)
admin.site.register(models.EPS_1G_ShiftSetUp_CXTTCFBK)
admin.site.register(models.EPS_1G_Pass)
admin.site.register(models.EPS_1G_Operator)
admin.site.register(models.EPS_1G_ReportSetUp)
admin.site.register(models.EPS1_Satellite)

admin.site.register(models.Product)