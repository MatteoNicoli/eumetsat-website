DROP TABLE IF EXISTS S3A_pass;

CREATE TABLE S3A_pass (
    Antenna TEXT,
    nOrbit TEXT,
    ANX TEXT,
    AOS0 TEXT,
    AOS0_Az TEXT,
    AOSM TEXT,
    AOSM_Az TEXT,
    AOS5 TEXT,
    AOS5_Az TEXT,
    Mid TEXT,
    Mid_Az TEXT,
    Mid_el TEXT,
    LOS5 TEXT,
    LOS5_Az TEXT,
    LOSM TEXT,
    LOSM_Az TEXT,
    LOS0 TEXT,
    LOS0_Az TEXT,
    TM0_Duration TEXT,
    TM_Duration TEXT,
    TC_Duration TEXT
);

INSERT INTO S3A_pass (Antenna, nOrbit, ANX) VALUES (
    'CDA1',
    '12345',
    '2021-12-13T13:45:40.185'
);