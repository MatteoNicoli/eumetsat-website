function getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
    // Get first day of year
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return [d.getUTCFullYear(), weekNo];
}

function updateClock(){
    var now = new Date();
    var utc = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
    var dname = utc.getDay(),
        mo = utc.getMonth(),
        dnum = utc.getDate(),
        yr = utc.getFullYear(),
        hou = utc.getHours(),
        min = utc.getMinutes(),
        sec = utc.getSeconds(),
        nweek = getWeekNumber(utc)[1]

    var start = new Date(utc.getFullYear(), 0, 0);
    var diff = (utc - start) + ((start.getTimezoneOffset() - utc.getTimezoneOffset()) * 60 * 1000);
    var oneDay = 1000 * 60 * 60 * 24;
    var DOY = 'DOY: ' + Math.floor(diff / oneDay);
    var CW =  'CW: ' + nweek;



        Number.prototype.pad = function(digits){
            for(var n = this.toString(); n.length < digits; n = 0 + n);
            return n;
        }

        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var week = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var ids = [ "CW", "DOY", "dayname", "month", "daynum", "year", "hour", "minutes", "seconds"];
        
        var values = [ CW, DOY, week[dname], dnum.pad(2), months[mo], yr, hou.pad(2), min.pad(2), sec.pad(2)];
        for(var i = 0; i < ids.length; i++)
        document.getElementById(ids[i]).firstChild.nodeValue = values[i];
    }

function initClock(){
    updateClock();
    window.setInterval("updateClock()", 1);
}

initClock()