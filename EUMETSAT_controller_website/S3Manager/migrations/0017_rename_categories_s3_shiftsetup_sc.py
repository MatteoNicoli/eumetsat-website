# Generated by Django 3.2.5 on 2021-11-20 14:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('S3Manager', '0016_remove_s3_shiftsetup_sc'),
    ]

    operations = [
        migrations.RenameField(
            model_name='s3_shiftsetup',
            old_name='categories',
            new_name='SC',
        ),
    ]
