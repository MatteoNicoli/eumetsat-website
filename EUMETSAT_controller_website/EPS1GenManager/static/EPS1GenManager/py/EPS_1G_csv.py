# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 21:07:12 2021

@author: Nicoli
"""

#%
# =============================================================================
# 
# =============================================================================
#import urllib
import datetime

import pandas as pd


#%
# =============================================================================
# 
# =============================================================================
def fcn_EPS_events_csv( FDF_events_EPS, Opts, LogbookIDs ):
    SetUp = Opts['SetUp']
    SetUp_MS = Opts['SetUp_MS']
    SetUp_CXTTCFBK = Opts['SetUp_CXTTCFBK']
    
    FDF_events_Metop = FDF_events_EPS['MetOp']
    FDF_events_N_BOS = FDF_events_EPS['NOAA_BOS']
    FDF_event_CXTTCFBK = FDF_events_EPS['CXTTCFBK']
    
    
    DS = SetUp['Date_start']
    DE = SetUp['Date_end']
    
    
    if ( SetUp['SideSpaCon'] == True and SetUp['SideGrndCon'] == True ):
        LogbookID = LogbookIDs.loc[ 'EPS_Combine' , 'ID' ]
    elif ( SetUp['SideGrndCon'] == True ):
        LogbookID = LogbookIDs.loc[ 'EPS_SpaCon' , 'ID' ]
    elif ( SetUp['SideSpaCon'] == True ):
        LogbookID = LogbookIDs.loc[ 'EPS_GrndCon' , 'ID' ]
    #end if
    
    
    
    iSev = 'Entry'
    
    flag_metop_list1=1
    flag_metop_list2=1
    flag_metop_list3=1
    metop_list=[]
    
    i_space = '&#09'
    i_space = '&nbsp'
    space = i_space*4
    space = (i_space+' ')*4*2
    
    
    
    csv_DateT = [datetime.datetime(1900, 1, 1, 00, 00, 0, 0*1000)]
    csv_Date = ['#']
    csv_Sev = ['TRUE']
    csv_Type = ['D']
    csv_Group = ['']
    csv_Text = ['']
    csv_LogbookID= ['']
    
    
    
    
    
    
    
    
    #
    if ( SetUp['Daily_Events'] == True ):
        metop_list.sort()
        
        SHO_Sev = 'SHO completed'
        SHO_Sev_Title = '<b><u>SHO Complete. The following checks have been completed:</u></b><br>'
        SHO_Sev_SC = 'Spacecraft: <br><ul>'
        for i_metop in range(0,len(metop_list)):
            SHO_Sev_SC = SHO_Sev_SC + '<li>M0'+str(metop_list[i_metop])+' CFS/PLM/IN: /0/0</li>'
        #end for
        
        SHO_Sev_SC = SHO_Sev_SC + '</ul>'
        SHO_Sev_EARSJASONSARAL = 'EARS: <br>JASON: <br>SARAL: <br>'
        SHO_Sev_End = 'SOIs to be performed on shift: <br>Schedule checked: '
        
        
        if ( SetUp['SideSpaCon'] == True and SetUp['SideGrndCon'] == True ):
            SHO_Sev = SHO_Sev_Title + SHO_Sev_SC + 'Ground Stations: <br><ul><li>Globals: </li><li>PMON: </li><li>PI: </li><li>FEP: </li><li>PGF: </li></ul>MCS: <br>LACOM:     LAMON: <br>Data Processing: <br>'+SHO_Sev_EARSJASONSARAL+'SAFs: <br>GEMS Filters: <br>'+SHO_Sev_End
        elif ( SetUp['SideGrndCon'] == True ):
            SHO_Sev = SHO_Sev_Title + 'Ground Stations: <br><ul><li>Globals: </li><li>PMON: </li><li>PI: </li><li>FEP: </li><li>PGF: </li></ul>MCS: <br>LACOM:     LAMON: <br>Data Processing: <br>SAFs: <br>GEMS Filters: <br>'+SHO_Sev_End
        elif ( SetUp['SideSpaCon'] == True ):
            SHO_Sev = SHO_Sev_Title + SHO_Sev_SC + SHO_Sev_EARSJASONSARAL + SHO_Sev_End
        #end if
        
        csv_DateT.append(DS)
        csv_Date.append( DS.strftime("%Y-%m-%dT-%H:%M:%SZ") )
        csv_Sev.append(iSev)
        csv_Type.append('CR')
        csv_Group.append('SHO')
        csv_Text.append('<html><body><p>'+SHO_Sev+'</p></body></html>')
        csv_LogbookID.append(LogbookID)
        
        csv_DateT.append(DE)
        csv_Date.append( DE.strftime("%Y-%m-%dT-%H:%M:%SZ") )
        csv_Sev.append(iSev)
        csv_Type.append('CR')
        csv_Group.append('SHO')
        csv_Text.append('<html><body><p>'+SHO_Sev+'</p></body></html>')
        csv_LogbookID.append(LogbookID)
        
        #
        ndays = (DE-DS).days
        for idays in range(0,ndays+1+2):
            iDate = datetime.datetime(DS.year, DS.month, DS.day, 0, 0, 0, 0*1000) + datetime.timedelta( hours = idays*24 )
            
            if ( iDate.weekday()==1-1 ):
                iDate_2 = iDate + datetime.timedelta( hours = 5 ) + datetime.timedelta( minutes = 30 )
                if ( SetUp['SideSpaCon'] == True ):
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CDA')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'RNG/DOP Calibration<br>CDA1/TTC1: Range 38.000 Time Delay 129000'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                    
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CDA')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'RNG/DOP Calibration<br>CDA2/TTC1: Range 38.000 Time Delay 129000'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                
                elif ( SetUp['SideGrndCon'] == True ):
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CDA')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'RNG/DOP Calibration<br>CDA1/TTC1'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                    
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CDA')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'RNG/DOP Calibration<br>CDA2/TTC1'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                #end if
            elif ( iDate.weekday()==3-1 ):
                if ( SetUp['SideSpaCon'] == True ):
                    iDate_2 = iDate + datetime.timedelta( hours = 16 ) + datetime.timedelta( minutes = 0 )
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('JASON')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'J3 pointing file check'+'</p></body></html>')
                    csv_LogbookID.append( LogbookIDs.loc[ 'JASON' , 'ID' ] )
                    
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('JASON')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'J3 pass planning file check'+'</p></body></html>')
                    csv_LogbookID.append( LogbookIDs.loc[ 'JASON' , 'ID' ] )
                #end if
            elif ( iDate.weekday()==5-1 ):
                if ( SetUp['SideSpaCon'] == True ):
                    iDate_2 = iDate + datetime.timedelta( hours = 6 ) + datetime.timedelta( minutes = 30 )
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('DIF')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'Weekly UNS check'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                #end if
                if ( SetUp['SideSpaCon'] == True ):
                    iDate_2 = iDate + datetime.timedelta( hours = 8 ) + datetime.timedelta( minutes = 10 )
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('ALL METOP')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'Delay MMAMs'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                #end if
            elif ( iDate.weekday()==6-1 ):
                iDate_2 = iDate + datetime.timedelta( hours = 23 ) + datetime.timedelta( minutes = 59 )
                if ( SetUp['SideSpaCon'] == True ):
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CR')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'Print schedule'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                    
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CR')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'Load CHRONOPS'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                    
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CR')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'Check ESTRACK Pass List'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                    
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CR')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'Load countdown tool'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                #end if
                if ( SetUp['SideGrndCon'] == True ):
                    csv_DateT.append(iDate_2)
                    csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                    csv_Sev.append(iSev)
                    csv_Type.append('CR')
                    csv_Group.append('Daily Event')
                    csv_Text.append('<html><body><p>'+'Print WOTIS file'+'</p></body></html>')
                    csv_LogbookID.append(LogbookID)
                #end if
            #end if
            
            iDate_2 = iDate + datetime.timedelta( hours = 7 ) + datetime.timedelta( minutes = 50 )
            if ( SetUp['SideSpaCon'] == True ):
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('ALL METOP')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'OBT to UTC Check'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
            #end if
            if ( SetUp['SideGrndCon'] == True ):
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('PGF')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'PGF Aux file Check'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
                
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('CDA')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'CDA short period pointing file Check'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
            #end if
            
            iDate_2 = iDate + datetime.timedelta( hours = 8 ) + datetime.timedelta( minutes = 20 )
            if ( SetUp['SideSpaCon'] == True ):
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('ALL METOP')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'MMAMs'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
            #end if
            
            iDate_2 = iDate + datetime.timedelta( hours = 13 ) + datetime.timedelta( minutes = 0 )
            if ( SetUp['SideGrndCon'] == True ):
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('CDA')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'CDA long period pointing file Check'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
            #end if
            
            iDate_2 = iDate + datetime.timedelta( hours = 17 ) + datetime.timedelta( minutes = 0 )
            if ( SetUp['SideSpaCon'] == True ):
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('ALL METOP')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'OBT to UTC Check'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
            #end if
            
            iDate_2 = iDate + datetime.timedelta( hours = 17 ) + datetime.timedelta( minutes = 15 )
            if ( SetUp['SideGrndCon'] == True ):
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('PGF')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'PGF Aux file Check'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
            #end if
            
            iDate_2 = iDate + datetime.timedelta( hours = 18 ) + datetime.timedelta( minutes = 0 )
            if ( SetUp['SideGrndCon'] == True ):
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('CDA')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'CDA short period pointing file Check'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
            #end if
            
            iDate_2 = iDate + datetime.timedelta( hours = 19 ) + datetime.timedelta( minutes = 0 )
            if ( SetUp['SideSpaCon'] == True ):
                csv_DateT.append(iDate_2)
                csv_Date.append( iDate_2.strftime("%Y-%m-%dT-%H:%M:%SZ") )
                csv_Sev.append(iSev)
                csv_Type.append('ALL METOP')
                csv_Group.append('Daily Event')
                csv_Text.append('<html><body><p>'+'MMAMs'+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
            #end if
        #end for
    #end if
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    for wrow in range(0,FDF_events_N_BOS.shape[0]):
        SC = FDF_events_N_BOS.loc[wrow,['SC']]['SC']
        AOSM = FDF_events_N_BOS.loc[wrow,['AOSM']]['AOSM']
        LOSM = FDF_events_N_BOS.loc[wrow,['LOSM']]['LOSM']
        nOrbit = FDF_events_N_BOS.loc[wrow,['nOrbit']]['nOrbit']
        str_nOrbit = str(nOrbit)
        CDAn = FDF_events_N_BOS.loc[wrow,['Antenna']]['Antenna']
        
        AzNOAA = FDF_events_N_BOS.loc[wrow,['AOSM_Az']]['AOSM_Az']
        nextAz = AzNOAA
        Azstr = str(AzNOAA)
        if (AzNOAA==260 ):
            nextAz = AzNOAA-360
            Azstr=str(nextAz)+'° or '+str(nextAz+360)+'°'
        elif (AzNOAA>260):
            nextAz = AzNOAA-360
            Azstr=str(nextAz)+'°'
        
        Azstrsin = str(abs(nextAz))
        Azstrsin1 = Azstr[0:1]
        if (Azstrsin1 != '-'):
            Azstrsin = ''+Azstrsin
        else:
            Azstrsin = Azstrsin1+Azstrsin
        
        AOSlineText='AOS '+space+'#'+nOrbit+' '+space+CDAn+'/TTC1 '+space+'Az/ACU: '+Azstrsin+'.000°('+Azstr+' expected)/2LT '+space+'TM:  OK '+space+'TC: OK'
        
        
        str_GAC=''
        for i_frty in range(0,len( FDF_events_N_BOS.loc[wrow,['Dump_Start']]['Dump_Start'] )):
            GAC_type = FDF_events_N_BOS.loc[wrow,['GDS_GAC']]['GDS_GAC'][1][i_frty]
            GAC_Frame = FDF_events_N_BOS.loc[wrow,['GDS_GAC']]['GDS_GAC'][2][i_frty]
            GAC_Frame_0 = '0'*len(GAC_Frame)
            GACStart = FDF_events_N_BOS.loc[wrow,['Dump_Start']]['Dump_Start'][i_frty]
            GACEnd = FDF_events_N_BOS.loc[wrow,['Dump_End']]['Dump_End'][i_frty]
            
            i_str_GAC_S = GACStart.strftime("%H:%M:%S") + ' - '+'GAC Start '+space+'Frames Expected: '+GAC_Frame+' ['+GAC_type+']'+'<br>'
            i_str_GAC_E = GACEnd.strftime("%H:%M:%S") + ' - '+'GAC End '+space+i_space+'Frames Received: '+GAC_Frame_0+' ['+GAC_type+']'+'<br>'
            
            str_GAC = str_GAC+i_str_GAC_S+i_str_GAC_E
        LOSlineText='LOS'
        
        iDate = AOSM
        str_AOS = AOSM.strftime("%H:%M:%S") + ' - '+AOSlineText
        
        AOS5 = FDF_events_N_BOS.loc[wrow,['AOS5']]['AOS5']
        str_AOS5 = AOS5.strftime("%H:%M:%S") + ' - Carrier Up<br>'
        LOS5 = FDF_events_N_BOS.loc[wrow,['LOS5']]['LOS5']
        str_LOS5 = LOS5.strftime("%H:%M:%S") + ' - Carrier Down<br>'
        #str_GAC_S = Date2HHMM(GACStart) + ' - '+'GAC Start '+space+'Frames Expected: '+GAC_Frame+' ['+GAC_type+']'
        #str_GAC_E = Date2HHMM(GACEnd) + ' - '+'GAC End '+space+i_space+'Frames Received: '+GAC_Frame_0+' ['+GAC_type+']'
        
        str_LOS = LOSM.strftime("%H:%M:%S") + ' - '+LOSlineText
        iText = str_AOS+'<br>' + str_AOS5 + str_GAC + str_LOS5 + str_LOS
        
        csv_DateT.append( iDate )
        csv_Date.append( iDate.strftime("%Y-%m-%dT-%H:%M:%SZ") )
        csv_Sev.append(iSev)
        csv_Type.append('')
        csv_Group.append('N'+SC[-2:])
        csv_Text.append('<html><body><p>'+iText+'</p></body></html>')
        csv_LogbookID.append(LogbookID)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    maxlennOrbit = len(str(max(FDF_events_EPS.loc[:,['CDA_nOrbit']]['CDA_nOrbit'])))
    
    
    for wrow in range(0,FDF_Pass_M.shape[0]):
        AOS = FDF_Pass_M.loc[wrow,['CDA_AOS']]['CDA_AOS']
        LOS = FDF_Pass_M.loc[wrow,['CDA_LOS']]['CDA_LOS']
        Metop = int(FDF_Pass_M.loc[wrow,['M01']]['M01'])
        
        if (flag_metop_list1==1 and Metop==1):
            flag_metop_list1 = 0
            metop_list.append(Metop)
        if (flag_metop_list2==1 and Metop==2):
            flag_metop_list2 = 0
            metop_list.append(Metop)
        if (flag_metop_list3==1 and Metop==3):
            flag_metop_list3 = 0
            metop_list.append(Metop)
        
        
        
        intnOrbit = int(FDF_Pass_M.loc[wrow,['CDA_nOrbit']]['CDA_nOrbit'])
        nOrbit = str(intnOrbit)
        
        CDAn = str(FDF_Pass_M.loc[wrow,['CDAn']]['CDAn'])
        if (CDAn=='AUTO'):
            CDAn = str(FDF_Pass_M.loc[wrow,['PGF_MPF_ACQ_SITE']]['PGF_MPF_ACQ_SITE'])
        
        if (AOS >= DS and AOS <= DE):
            if ( Metop == 19 or Metop == 18 ):
                if (SpaGrndCon==1 or SpaGrndCon==3):
                    NOAA_BOS = FDF_Pass_M.loc[wrow,['NOAA_BOS']]['NOAA_BOS']
                    
                    if (NOAA_BOS == 'YES'):
                        
                        AzNOAA = FDF_Pass_M.loc[wrow,['CDA_AOS_Azi']]['CDA_AOS_Azi']
                        nextAz = AzNOAA
                        Azstr = str(AzNOAA)
                        
                            
                        if (AzNOAA==260 ):
                            nextAz = AzNOAA-360
                            Azstr=str(nextAz)+'° or '+str(nextAz+360)+'°'
                        elif (AzNOAA>260):
                            nextAz = AzNOAA-360
                            Azstr=str(nextAz)+'°'
                        
                        Azstrsin = str(abs(nextAz))
                        Azstrsin1 = Azstr[0:1]
                        if (Azstrsin1 != '-'):
                            Azstrsin = ''+Azstrsin
                        else:
                            Azstrsin = Azstrsin1+Azstrsin
                        
                        AOSlineText='AOS '+space+'#'+nOrbit+' '+space+CDAn+'/TTC1 '+space+'Az/ACU: '+Azstrsin+'.000°('+Azstr+' expected)/2LT '+space+'TM:  OK '+space+'TC: OK'
                        MtOp=str(Metop)
                        
                        str_GAC=''
                        for i_frty in range(0,len( FDF_Pass_M.loc[wrow,['Dump_Start']]['Dump_Start'] )):
                            GAC_type = FDF_Pass_M.loc[wrow,['GDS_GAC']]['GDS_GAC'][1][i_frty]
                            GAC_Frame = FDF_Pass_M.loc[wrow,['GDS_GAC']]['GDS_GAC'][2][i_frty]
                            GAC_Frame_0 = '0'*len(GAC_Frame)
                            GACStart = FDF_Pass_M.loc[wrow,['Dump_Start']]['Dump_Start'][i_frty]
                            GACEnd = FDF_Pass_M.loc[wrow,['Dump_End']]['Dump_End'][i_frty]
                            
                            i_str_GAC_S = GACStart.strftime("%H:%M:%S") + ' - '+'GAC Start '+space+'Frames Expected: '+GAC_Frame+' ['+GAC_type+']'+'<br>'
                            i_str_GAC_E = GACEnd.strftime("%H:%M:%S") + ' - '+'GAC End '+space+i_space+'Frames Received: '+GAC_Frame_0+' ['+GAC_type+']'+'<br>'
                            
                            str_GAC = str_GAC+i_str_GAC_S+i_str_GAC_E
                        LOSlineText='LOS'
                        
                        iDate = AOS
                        str_AOS = AOS.strftime("%H:%M:%S") + ' - '+AOSlineText
                        
                        AOS5 = FDF_Pass_M.loc[wrow,['CDA_AOS5']]['CDA_AOS5']
                        str_AOS5 = AOS5.strftime("%H:%M:%S") + ' - Carrier Up<br>'
                        LOS5 = FDF_Pass_M.loc[wrow,['CDA_LOS5']]['CDA_LOS5']
                        str_LOS5 = LOS5.strftime("%H:%M:%S") + ' - Carrier Down<br>'
                        #str_GAC_S = Date2HHMM(GACStart) + ' - '+'GAC Start '+space+'Frames Expected: '+GAC_Frame+' ['+GAC_type+']'
                        #str_GAC_E = Date2HHMM(GACEnd) + ' - '+'GAC End '+space+i_space+'Frames Received: '+GAC_Frame_0+' ['+GAC_type+']'
                        
                        str_LOS = LOS.strftime("%H:%M:%S") + ' - '+LOSlineText
                        iText = str_AOS+'<br>' + str_AOS5 + str_GAC + str_LOS5 + str_LOS
                        
                        csv_DateT.append(iDate)
                        csv_Date.append(datatime2str_csv(iDate))
                        csv_Sev.append(iSev)
                        csv_Type.append('')
                        csv_Group.append('N'+MtOp)
                        csv_Text.append('<html><body><p>'+iText+'</p></body></html>')
                        csv_LogbookID.append(LogbookID)
                        
                        
            else:
                AOSM=FDF_Pass_M.loc[wrow,['CDA_AOS5']]['CDA_AOS5']
                RPF=AOSM+datetime.timedelta(seconds=3*60+45)
                MtOp='0'+str(Metop)
                
                DEFPass = FDF_Pass_M.loc[wrow,['TM_Format']]['TM_Format']
                AOCSPass = FDF_Pass_M.loc[wrow,['AOCS']]['AOCS']
                
                
                SpanOr = i_space*(maxlennOrbit-len(nOrbit))
                
                
                
                
                
                iDEFPass = ''
                #print (Metop, DEFPass)
                if (DEFPass == 'DEF_ROUT' ):
                    iDEFPass = ' - '+DEFPass
                    RPFlineText = 'SVM and TEXTR received and reset'
                else:
                    RPFlineText="All RPF's received and reset"
                iAOCSPass = ''
                if (AOCSPass == 'AOCS' ):
                    iAOCSPass = ' - '+AOCSPass
                    
                TMF_AOCS = SpanOr + iDEFPass + iAOCSPass
                
                Det = ''
                
                #==================================================================
                #
                
                
                
                if (SpaGrndCon==1 or SpaGrndCon==2):
                    Det = Det + ''+TMF_AOCS
                
                
                AOSlineText='AOS '+space+'#'+nOrbit+Det+' '+space+CDAn+'/TTC1 '+space+'TM: OK '+space+'TC: OK'
                
                #==================================================================
                #    
                nextAz=FDF_Pass_M.loc[wrow,['Next_Az_MetOp']]['Next_Az_MetOp']
                nextAzstr=str(nextAz)
                if (nextAz==260 ):
                    nextAz = nextAz-360
                    nextAzstr=str(nextAz)+'° or '+str(nextAz+360)+'°'
                elif (nextAz>260):
                    nextAz = nextAz-360
                    nextAzstr=str(nextAz)+'°'
                
                nextAzstrsin = str(abs(nextAz))
                
                
                nextAzstrsin1 = nextAzstr[0:1]
                if (nextAzstrsin1 != '-'):
                    nextAzstrsin = ''+nextAzstrsin
                else:
                    nextAzstrsin = nextAzstrsin1+nextAzstrsin
                #
                RD=FDF_Pass_M.loc[wrow,['RNGDOP']]['RNGDOP']
                
                
                if (SpaGrndCon==1):
                    LOSlineText='LOS '+space+'XBAND: OK '+space+'TC Count: 000/255 '+space+'R/D: '+str(RD)+'/'+str(RD)+' '+space+'Az/ACU: '+nextAzstrsin+'.000°('+nextAzstr+' expected)/2LT'
                elif (SpaGrndCon==2):
                    LOSlineText='LOS '+space+'XBAND: OK '+space+'TC Count: 000/255 '+space+'R/D: '+str(RD)+'/'+str(RD)+''
                elif (SpaGrndCon==3):
                    LOSlineText='LOS '+space+'Az/ACU: '+nextAzstrsin+'.000°('+nextAzstr+' expected)/2LT'
                    
                
                
                iDate = AOS
                str_AOS = Date2HHMM(AOS) + ' - '+AOSlineText
                str_reset = Date2HHMM(RPF) + ' - '+RPFlineText
                str_LOS = Date2HHMM(LOS) + ' - '+LOSlineText
                
                #==================================================================
                if (XTTC_FBK_AllInfo[0]==True and len(XTTC_FBK_AllInfo)>1):
                    for i_XTTC_FBK_info in range(1,len(XTTC_FBK_AllInfo)):
                        XTTC_FBK_info = XTTC_FBK_AllInfo[i_XTTC_FBK_info]
                        if (XTTC_FBK_info[0]==1):
                            if (XTTC_FBK_info[1]==False):
                                if (SpaGrndCon==1 or SpaGrndCon==2):
                                    iText = str_AOS+'<br>'+str_reset+'<br>'+str_LOS
                                else:
                                    iText = str_AOS+'<br>'+str_LOS
                            else:
                                if (intnOrbit!=XTTC_FBK_info[4]):
                                    if (SpaGrndCon==1 or SpaGrndCon==2):
                                        iText = str_AOS+'<br>'+str_reset+'<br>'+str_LOS
                                    else:
                                        iText = str_AOS+'<br>'+str_LOS
                                else:
                                    if (SpaGrndCon==3):
                                        iText = str_AOS+'<br>'+str_LOS
                        else:
                            if (SpaGrndCon==1 or SpaGrndCon==2):
                                iText = str_AOS+'<br>'+str_reset+'<br>'+str_LOS
                            else:
                                iText = str_AOS+'<br>'+str_LOS
                else:
                    if (SpaGrndCon==1 or SpaGrndCon==2):
                        iText = str_AOS+'<br>'+str_reset+'<br>'+str_LOS
                    else:
                        iText = str_AOS+'<br>'+str_LOS
                csv_DateT.append(iDate)
                csv_Date.append(datatime2str_csv(iDate))
                csv_Sev.append(iSev)
                csv_Type.append('')
                csv_Group.append('M'+MtOp)
                csv_Text.append('<html><body><p>'+iText+'</p></body></html>')
                csv_LogbookID.append(LogbookID)
    








































 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

            
    
    datacolum = ['Date_T','Date_str','Sev','Type','Group','Text','LogBook_ID']
    data = {'Date_T': csv_DateT,
            'Date_str': csv_Date,
            'Sev': csv_Sev,
            'Type': csv_Type,
            'Group': csv_Group,
            'Text': csv_Text,
            'LogBook_ID': csv_LogbookID
            }
    
    Data_csv_vet = pd.DataFrame ( data, columns = datacolum).sort_values( by=['Date_T'] ).reset_index().drop( columns='index' )
    
    
    filename = EPS_csv_filname( SetUp, SetUp_MS, SetUp_CXTTCFBK)
    return Data_csv_vet, filename











def EPS_csv_filname( SetUp, SetUp_MS, SetUp_CXTTCFBK):
    if ( SetUp['SideSpaCon'] == True and SetUp['SideGrndCon'] == True ):
        strEPSSide = 'EPS_Combine'
    elif ( SetUp['SideGrndCon'] == True ):
        strEPSSide = 'EPS_GrndCon'
    elif ( SetUp['SideSpaCon'] == True ):
        strEPSSide = 'EPS_SpaCon'
    #end if
    strInfo = ''
    if ( SetUp['NOAA_BOS'].strftime("%Y-%j-%H:%M:%S") == True ):
        strInfo = strInfo+'_NOAA_BOS'
    #end if
    if ( SetUp['Daily_Events'].strftime("%Y-%j-%H:%M:%S") == True ):
        strInfo = strInfo+'_DailyEvents'
    #end if
    if ( SetUp_MS.cleaned_data['MS_Date_start'].strftime("%Y-%j-%H:%M:%S") != None or 
         SetUp_MS.cleaned_data['MS_Date_end'].strftime("%Y-%j-%H:%M:%S") != None ):
        strInfo = strInfo+'_MissionSwap'
    #end if
    if (  ):
        strInfo = strInfo+'_XTTCFBK'
    #end if
    strShiftfilename_from = SetUp['Date_start'].strftime("%Y_%j_%H_%M_%S")
    strShiftfilename_to = SetUp['Date_end'].strftime("%Y_%j_%H_%M_%S")
    
    filename ='uberlogBatch_EPS1_from_'+strShiftfilename_from+'_to_'+strShiftfilename_to+'_'+strEPSSide+''+strInfo
    return filename