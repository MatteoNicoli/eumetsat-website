# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 15:52:14 2021

@author: Nicoli
"""
import os, sys
DIR = os.path.abspath('.')
DIR_int = DIR.find('EUMETSAT_controller_website')
list_down_DIR = DIR[DIR_int:].split(os.sep)
new_up_DIR = (os.sep).join(['..']*( len(list_down_DIR) -1))
sys.path.insert(0, os.path.abspath(new_up_DIR))


#
OS_Name = os.name
OS_UserName = os.getlogin()
UserName = OS_UserName

if (OS_Name=='nt'):
    path = os.path.join( 'C:', os.sep, "Users", OS_UserName )
elif (OS_Name=='posix'):
    path = os.path.join( os.sep, "home", OS_UserName )
#end if
DesktopPath = os.path.join(path, "Desktop")

# =============================================================================
# 
# =============================================================================
from static.py.Sup_fcns import *
from S3_Sup import *
from S3_csv import *











# =============================================================================
# 
# =============================================================================


UserName = 'Atzeni'
filePath = os.path.join("D:", os.sep, "EPS System Ops")






# =============================================================================
# 
# =============================================================================

path = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv' )

Path_SupFile_List_LogBookIDs = os.path.join(path,'LogBookID.csv')
if (os.path.isfile(Path_SupFile_List_LogBookIDs) == True):
    List_LogBookIDs = pd.read_csv(Path_SupFile_List_LogBookIDs, index_col=0)
#end if

Path_SupFile_List_ESOC_IDs = os.path.join(path,'ESOC_antenna_ID.csv')
if (os.path.isfile(Path_SupFile_List_ESOC_IDs) == True):
    List_ESOC_IDs = pd.read_csv(Path_SupFile_List_ESOC_IDs, index_col=0)
#end if







# =============================================================================
# 
# =============================================================================
Mission= 'S3'
Datetime_UTC = datetime.datetime.now()
Shift_Info = Date_Shift( Mission, Datetime_UTC )

DateS = datetime.datetime(2021, 10, 30, 18, 00 )
DateE = datetime.datetime(2021, 10, 30, 4, 0, 0 )

DateS = Shift_Info[1]
DateE = Shift_Info[2]


SetUp = {
        'SC': ['SENTINEL 3A','SENTINEL 3B'],
        'Daily_Events': True,
        'Date_Shift': datetime.date(2021, 10, 12),
        'Shift': 'N',
        'Date_start': DateS,
        'Date_end': DateE,
    }

ToUberlog = {
    'ToUberlog': 'csv',
    'Uberlog_ID': 'MatteoN',
    'Uberlog_Paswd': 'elog1',
    }
        


# =============================================================================
# 
# =============================================================================


#FDF_event_S3 = S3_FDF_events(DIR,DIR_int,SetUp)
FDF_event_S3 = S3_FDF_events_gen(
    DIR,DIR_int,
    SetUp,
    )


# =============================================================================
# 
# =============================================================================
Batch_info = Events2csv(fcn_S3_events_csv, FDF_event_S3, SetUp, ToUberlog, List_LogBookIDs )


file_Path_Name_csv = os.path.join( DesktopPath, Batch_info['Name']+'.csv' )
Batch_csv = Batch_info['Data']
Batch_csv.to_csv (
    r''+file_Path_Name_csv,
    #path_or_buf = response,
    index = False,
    header = False,
    )