# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 22:10:59 2021

@author: Nicoli
"""
# =============================================================================
# 
# =============================================================================
import datetime

# =============================================================================
# 
# =============================================================================
from static.FDF_Orekit.Orekit_function import *
from NOAA_BOS_info import *












# =============================================================================
# 
# =============================================================================
def EPS_FDF_events(SetUp_Overall):
# =============================================================================
#     
# =============================================================================
    SetUp = SetUp_Overall['SetUp']
    SetUp_MS = SetUp_Overall['SetUp_MS']
    SetUp_CXTTCFBK = SetUp_Overall['SetUp_CXTTCFBK']
    
    # =============================================================================
    #     
    # =============================================================================
    DS = datetime.datetime.strptime(SetUp['Date_start'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    DE = datetime.datetime.strptime(SetUp['Date_end'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")+ datetime.timedelta(minutes=3*110)
    
    # =============================================================================
    # 
    # =============================================================================
    path_SCInfo = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','static','csv','SCInfo.csv')
    if (os.path.isfile(path_SCInfo) == True):
        list_SCIDs = pd.read_csv(path_SCInfo, index_col=0)
    #end if
    
# =============================================================================
#     
# =============================================================================\
    FDF_AntLoc_Tot = AntenneInfo()
        
    Ant_1 = 'CDA1'
    utc, earth, inertialFrame, station_frame_1, ElMask_1 = Frames( FDF_AntLoc_Tot, Ant_1)
    
    Ant_2 = 'CDA2'
    utc, earth, inertialFrame, station_frame_2, ElMask_2 = Frames( FDF_AntLoc_Tot, Ant_2)
    
# =============================================================================
#     
# =============================================================================
    #
    frames_1 = []
    frames_2 = []
    for i_SC in SetUp['SC']:
        # CDA1
        FDF_events_1_Mx, propagator = FDF_PassGenerator(
            Ant_1,
            list_SCIDs,
            i_SC,
            SetUp['Date_start'],
            DE,
            utc, inertialFrame, station_frame_1, ElMask_1,
            )
        frames_1.append(FDF_events_1_Mx)
        
        # CDA2
        FDF_events_2_Mx, propagator = FDF_PassGenerator(
            Ant_2,
            list_SCIDs,
            i_SC,
            SetUp['Date_start'],
            DE,
            utc, inertialFrame, station_frame_2, ElMask_2,
            )
        frames_2.append(FDF_events_2_Mx)
    #end for
    
    FDF_events_1_M = pd.concat(frames_1).sort_values(by=['ANX']).reset_index().drop(columns='index')
    FDF_events_2_M = pd.concat(frames_2).sort_values(by=['ANX']).reset_index().drop(columns='index')
    FDF_events_M={
        'CDA1': FDF_events_1_M,
        'CDA2': FDF_events_2_M,
        }
    
# =============================================================================
# 
# =============================================================================
    filePath_NOAA_week_OLD = os.path.join(DIR[:DIR_int],'EUMETSAT_controller_website','EPS1GenManager','static','EPS1GenManager', "csv", "NOAA_BOS.csv")
    
    # CDA1
    FDF_events_1_N19, propagator = FDF_PassGenerator(
        Ant_1,
        list_SCIDs,
        'NOAA 19',
        SetUp['Date_start'],
        DE,
        utc, inertialFrame, station_frame_1, ElMask_1,
        )
    FDF_events_1_N18, propagator = FDF_PassGenerator(
        Ant_1,
        list_SCIDs,
        'NOAA 18',
        SetUp['Date_start'],
        DE,
        utc, inertialFrame, station_frame_1, ElMask_1,
        )
    FDF_frames_1 = [FDF_events_1_N19,FDF_events_1_N18]
    FDF_Pass_1_N = pd.concat(FDF_frames_1).sort_values(by=['ANX']).reset_index().drop(columns='index')
    
    FDF_events_1_N = NOAA_BOS_shift(FDF_Pass_1_N, SetUp['Date_start'], DE, filePath_NOAA_week_OLD)
    
    FDF_events_1_N_BOS = FDF_events_1_N[
        ( FDF_events_1_N['NOAA_BOS'] == 'YES' )
        ].sort_values(by=[('ANX')]).reset_index().drop(columns='index')
    
    # CDA2
    FDF_events_2_N19, propagator = FDF_PassGenerator(
        Ant_2,
        list_SCIDs,
        'NOAA 19',
        SetUp['Date_start'],
        DE,
        utc, inertialFrame, station_frame_2, ElMask_2,
        )
    FDF_events_2_N18, propagator = FDF_PassGenerator(
        Ant_2,
        list_SCIDs,
        'NOAA 18',
        SetUp['Date_start'],
        DE,
        utc, inertialFrame, station_frame_2, ElMask_2,
        )
    FDF_frames_2 = [FDF_events_2_N19,FDF_events_2_N18]
    FDF_Pass_2_N = pd.concat(FDF_frames_2).sort_values(by=['ANX']).reset_index().drop(columns='index')

    FDF_events_2_N = NOAA_BOS_shift(FDF_Pass_2_N, SetUp['Date_start'], DE, filePath_NOAA_week_OLD)

    FDF_events_2_N_BOS = FDF_events_2_N[
        ( FDF_events_2_N['NOAA_BOS'] == 'YES' )
        ].sort_values(by=[('ANX')]).reset_index().drop(columns='index')
    
    #
    FDF_events_N={
        'CDA1': FDF_events_1_N,
        'CDA2': FDF_events_2_N,
        }
    FDF_events_N_BOS={
        'CDA1': FDF_events_1_N_BOS,
        'CDA2': FDF_events_2_N_BOS,
        }
    
# =============================================================================
#     
# =============================================================================
    PassEvent = [
        ("SC"),
        ( "Antenna" ),
        ( 'nOrbit' ),
        ( "ANX" ),
        ( "AOS0" ),
        ( "AOS0_Az" ),
        ( "AOSM" ),
        ( "AOSM_Az" ),
        ( "AOS5" ),
        ( "AOS5_Az" ),
        ( "Mid" ),
        ( "Mid_Az" ),
        ( "Mid_el" ),
        ( "LOS5" ),
        ( "LOS5_Az" ),
        ( "LOSM" ),
        ( "LOSM_Az" ),
        ( "LOS0" ),
        ( "LOS0_Az" ),
        ( "TM0_Duration" ),
        ( "TM_Duration" ),
        ( "TC_Duration" ),
        ]
    
    
    FDF_event_CXTTCFBK = pd.DataFrame ([], columns = PassEvent)
    if ( len(SetUp_CXTTCFBK)!=0 ):
        for i_CXTTCFBK in SetUp_CXTTCFBK:
            nOrbit = i_CXTTCFBK['C_XTTC_FBK_nOrbit']
            if ( nOrbit != None ):
                utc, earth, inertialFrame, station_frame, ElMask = Frames( FDF_AntLoc_Tot, i_CXTTCFBK['C_XTTC_FBK_Antenna'])
                FDF_events_CXTTCFBK, propagator = FDF_PassGenerator(
                    i_CXTTCFBK['C_XTTC_FBK_Antenna'],
                    list_SCIDs,
                    i_CXTTCFBK['C_XTTC_FBK_SC'],
                    SetUp['Date_start'],
                    DE,
                    utc, inertialFrame, station_frame, ElMask,
                    )
                FDF_event_CXTTCFBK = FDF_events_CXTTCFBK[
                    ( FDF_events_CXTTCFBK['nOrbit'] == nOrbit )
                    ]
            #end if
        #end for
    #end if
# =============================================================================
#     
# =============================================================================
    FDF_events={
        'MetOp': FDF_events_M,
        'NOAA': FDF_events_N,
        'NOAA_BOS': FDF_events_N_BOS,
        'CXTTCFBK': FDF_event_CXTTCFBK,
        }
    return FDF_events
#end def