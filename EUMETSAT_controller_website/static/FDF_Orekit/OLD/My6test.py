# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 22:55:39 2021

@author: Nicoli
"""

from Orekit_function import *

SetUp = {
        'SideSpaCon': True,
        'SideGrndCon': True,
        'Antenna': 'CDA1',
        'SC': 'All',
        'Daily_Events': True,
        'NOAA_BOS': True,
        'Date_Shift': datetime.date(2021, 8, 19),
        'Shift': 'N',
        'Date_start': datetime.datetime(2021, 9, 9, 18, 15 ),
        'Date_end': datetime.datetime(2021, 9, 10, 3, 59, 59 ),
        'file_report': False,
    }
SetUp_MS = {
        'MS_Date_start': None,
        'MS_Date_end': None,
    }
SetUp_CXTTCFBK = {
        'C_XTTC_FBK_SC': 'METOP-A',
        'C_XTTC_FBK_nOrbit': None,
        'C_XTTC_FBK_Antenna': 'CDA1'
    }

ToUberlog = {
    'ToUberlog': 'import',
    'Uberlog_ID': 'MatteoN',
    'Uberlog_Paswd': 'elog',
    }





# =============================================================================
# 
# =============================================================================
def EPS1_FDFPassGenerator(SetUp, SetUp_MS, SetUp_CXTTCFBK):
    FDF_AntLoc_Tot = AntenneInfo()
    
    utc, inertialFrame, station_frame_1, ElMask_1 = Frames( FDF_AntLoc_Tot, 'CDA1')
    utc, inertialFrame, station_frame_2, ElMask_2 = Frames( FDF_AntLoc_Tot, 'CDA2')
    
    Antenna_M = SetUp['Antenna']
    Antenna_N = 'CDA2'
    station_frame_M = station_frame_1
    ElMask_M = ElMask_1
    station_frame_N = station_frame_2
    ElMask_N = ElMask_2
    if ( Antenna_M == Antenna_N ):
        Antenna_N = 'CDA1'
        station_frame_M = station_frame_2
        ElMask_M = ElMask_2
        station_frame_N = station_frame_1
        ElMask_N = ElMask_1
    #end if
    
    if (SetUp['SC']=='All'):
        SCs = [
               'METOP-A',
               'METOP-B',
               'METOP-C',
               'NOAA 19',
               'NOAA 18',
               ]
    elif (SetUp['SC']=='M-All'):
        SCs = [
               'METOP-A',
               'METOP-B',
               'METOP-C',
               ]
    elif (SetUp['SC']=='N-All'):
        SCs = [
               'NOAA 19',
               'NOAA 18',
               ]
    else:
         SCs = [
               SetUp['SC'],
               ]
    #end if
    
    FDF_Pass_list = []
    FDF_Pass_list_N = []
    for iSC in SCs:
        Antenna = Antenna_M
        station_frame = station_frame_M
        ElMask = ElMask_M
        if ( iSC.lower().find(('NOAA').lower()) != -1 ):
            Antenna = Antenna_N
            station_frame = station_frame_N
            ElMask = ElMask_N
        #end if
        
        iFDF_events = FDF_PassGenerator(
            Antenna,
            iSC,
            SetUp['Date_start'],
            SetUp['Date_end'],
            utc, inertialFrame, station_frame, ElMask,
            )
        
        if ( iSC.lower().find(('NOAA').lower()) != -1 ):
            FDF_Pass_list_N = FDF_Pass_list_N + [iFDF_events]
        else:
            FDF_Pass_list = FDF_Pass_list + [iFDF_events]
    
    result_M = pd.concat(FDF_Pass_list)
    FDF_Pass_M = result_M.sort_values(by=['ANX'])
    FDF_Pass_M = FDF_Pass_M.reset_index()
    FDF_Pass_M = FDF_Pass_M.drop(columns='index')
    
    result_N = pd.concat(FDF_Pass_list_N)
    FDF_Pass_N = result_N.sort_values(by=['ANX'])
    FDF_Pass_N = FDF_Pass_N.reset_index()
    FDF_Pass_N = FDF_Pass_N.drop(columns='index')
    
    FDF_Pass ={
        'flag': True,
        'METOP': FDF_Pass_M,
        'NOAA': FDF_Pass_N,
        }
    return FDF_Pass
#end if
st = SpaceTrackClient('matteo.nicoli.17@gmail.com', 'KG*p_h_VzK7xz*z')
TLE_Spacetrack = st.tle_latest(norad_cat_id=29499, ordinal=1, format='tle')
tle_list = list(TLE_Spacetrack.split("\n"))

FDF_Pass = EPS1_FDFPassGenerator(SetUp, SetUp_MS, SetUp_CXTTCFBK)