# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 22:08:19 2021

@author: Nicoli
"""
# =============================================================================
# 
# =============================================================================
import datetime
import pandas as pd










# =============================================================================
# 
# =============================================================================
def fcn_S6_events_csv(FDF_Pass, SetUp, LogbookIDs ):
    DS = datetime.datetime.strptime(SetUp['Date_start'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    DE = datetime.datetime.strptime(SetUp['Date_end'].strftime("%Y-%m-%dT%H:%M:%S"), "%Y-%m-%dT%H:%M:%S")
    
    csv_DateT1 = datetime.datetime(1900, 1, 1, 00, 00, 0, 0*1000)
    csv_DateT = [csv_DateT1]
    csv_Date = ['#']
    csv_Sev = ['TRUE']
    csv_Type = ['D']
    csv_Group = ['']
    csv_Text = ['']
    csv_LogbookID= ['']
    
    SevText = 'Info'
    SpaConLogBook = LogbookIDs.loc[ 'S6 Space' , 'ID' ]
    GrndLogBook = LogbookIDs.loc[ 'S6 Ground' , 'ID' ]
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    if (SetUp['Daily_Events'] == True):
        #
        i_D = datetime.datetime(DS.year, DS.month, DS.day, 0, 0, 0, 0*1000)
        flag = True
        
        TypeSTCText = 'STC & NTC Monitoring'
        TypeDIFText = 'Dissemination Monitoring'
        
        while (flag):
            if (i_D>=DS and i_D<DE):
                # DIF
                csv_DateT.append(i_D)
                csv_Date.append(i_D.strftime("%Y-%m-%dT%H:%M:%SZ"))
                csv_Sev.append(SevText)
                csv_Type.append(TypeDIFText)
                csv_Group.append('Dissemination')
                csv_Text.append('<html><body>'+'SMART checked for NRT-STC-NTC-ADF_RINEX_RONTC'+'</body></html>')
                csv_LogbookID.append(GrndLogBook)
                
                if ( i_D.hour == 0 ):
                    #DIF
                    csv_DateT.append(i_D)
                    csv_Date.append(i_D.strftime("%Y-%m-%dT%H:%M:%SZ"))
                    csv_Sev.append(SevText)
                    csv_Type.append('Other Activity')
                    csv_Group.append('')
                    csv_Text.append('<html><body>'+'Daily pass confirmation. FBK called and email sent'+'</body></html>')
                    csv_LogbookID.append(GrndLogBook)
                #end if
                
                if ( i_D.hour == 2 ):
                    #DIF
                    csv_DateT.append(i_D)
                    csv_Date.append(i_D.strftime("%Y-%m-%dT%H:%M:%SZ"))
                    csv_Sev.append(SevText)
                    csv_Type.append('Other Activity')
                    csv_Group.append('')
                    csv_Text.append('<html><body>'+'Facility: GEMS<br>Activity: Post Pass Reports checked for DOY '+i_D.strftime('%j')+'</body></html>')
                    csv_LogbookID.append(GrndLogBook)
                #end if
                
                # STC & NTC
                if ( i_D.hour == 1 or i_D.hour == 5 or i_D.hour == 9 or i_D.hour == 13 or i_D.hour == 17 or i_D.hour == 21 ):
                    STCText = 'Production date:<br>STC: OK'
                    if (i_D.hour == 5 or i_D.hour == 17 ):
                        STCText = STCText + '<br>NTC: OK<br>RONTC: OK'
                    #end if
                    #STC/NTC
                    csv_DateT.append(i_D)
                    csv_Date.append(i_D.strftime("%Y-%m-%dT%H:%M:%SZ"))
                    csv_Sev.append(SevText)
                    csv_Type.append(TypeSTCText)
                    csv_Group.append('STC/NTC Production')
                    csv_Text.append('<html><body>'+STCText+'</body></html>')
                    csv_LogbookID.append(GrndLogBook)
                
            #end if
            
            if (i_D>=DE):
                flag = False
            #end if
            i_D = i_D + datetime.timedelta(hours=1)
        #end while
    #end if
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    for i_FDF_Pass_M in range(0,FDF_Pass.shape[0]):
        nOrbit = FDF_Pass.loc[i_FDF_Pass_M,('nOrbit')]
        nOrbit_str = str( nOrbit )
        AOS = FDF_Pass.loc[i_FDF_Pass_M,('AOSM')]
        LOS = FDF_Pass.loc[i_FDF_Pass_M,('LOS0')]
        GS = FDF_Pass.loc[i_FDF_Pass_M,('Antenna')]
        
        
            
            
        #SpaCon pass
        iD_S = AOS
        csv_DateT.append(iD_S)
        csv_Date.append(iD_S.strftime("%Y-%m-%dT%H:%M:%SZ"))
        csv_Sev.append(SevText)
        csv_Type.append('Pass Information')
        csv_Group.append('S-Band Pass')
        
        Text = '<b>AOS:</b> '+AOS.strftime("%H:%M")+'<br>'
        Text = Text + '<b>LOS:</b> '+LOS.strftime("%H:%M")+'<br>'
        Text = Text + '<b>GS:</b> '+GS[:-1]+'<br>'
        Text = Text + '<b>Orbit:</b> '+nOrbit_str+'<br>'
        Text = Text + '<b>Pass type:</b> '+'<br>'
        Text = Text + '<br>'
        Text = Text + 'Activity: <br>'
        Text = Text + 'NOP: <br>'
        Text = Text + 'Syslog: <br>'
        Text = Text + 'OBC-MM dump: '
        csv_Text.append('<html><body>'+Text+'</body></html>')
        csv_LogbookID.append(SpaConLogBook)
        
        iD_SR = LOS + datetime.timedelta(minutes=45)
        if ( GS=='FBK' ):
            iD_SR = LOS + datetime.timedelta(minutes=55)
        #end if
        
        # SpaCon replay
        csv_DateT.append(iD_SR)
        csv_Date.append(iD_SR.strftime("%Y-%m-%dT%H:%M:%SZ"))
        csv_Sev.append(SevText)
        csv_Type.append('X/B replayed')
        csv_Group.append('')
        Text = 'X/B HKTM replayed<br>Orbit: '+nOrbit_str
        csv_Text.append('<html><body>'+Text+'</body></html>')
        csv_LogbookID.append(SpaConLogBook)
        
        # GrndCon replay
        iD_GR = LOS + datetime.timedelta(minutes=5)
        if ( GS=='FBK' ):
            iD_GR = LOS + datetime.timedelta(minutes=30)
        #end if
        csv_DateT.append(iD_GR)
        csv_Date.append(iD_GR.strftime("%Y-%m-%dT%H:%M:%SZ"))
        csv_Sev.append(SevText)
        csv_Type.append('X/B Dump Monitoring')
        csv_Group.append('X/B Dump')
        Text = 'Orbit #: '+nOrbit_str+'<br>Type: Routine + Archive<br>Status: OK'
        csv_Text.append('<html><body>'+Text+'</body></html>')
        csv_LogbookID.append(GrndLogBook)
    #end for
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    datacolum = ['Date_T','Date_str','Sev','Type','Group','Text','LogBook_ID']
    data = {'Date_T': csv_DateT,
            'Date_str': csv_Date,
            'Sev': csv_Sev,
            'Type': csv_Type,
            'Group': csv_Group,
            'Text': csv_Text,
            'LogBook_ID': csv_LogbookID
            }
    
    Data_csv_vet = pd.DataFrame (data, columns = datacolum ).sort_values(by=['Date_T']).reset_index().drop(columns='index')
    
    
    filename = 'S6_Batch_'+DS.strftime('%Y_%j_%H_%M_%S')
    return Data_csv_vet, filename
#end def










