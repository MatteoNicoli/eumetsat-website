from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View
from django.http import JsonResponse
from django.utils import timezone



from . import forms
from . import models

# Create your views here.











def GEMSManager_Initial_data():
    dict_initial_data = {
    }
    return dict_initial_data
#end def











class GEMSManager_view(View):
    

    # =============================================================================
    # 
    # =============================================================================
    def get(self, request):
        dict_initial_data = GEMSManager_Initial_data()

        my_form_GEMSSet = forms.GEMS_SetUp_form()
        
        if ( request.is_ajax() ):
            context_ajax = {
            }
            # return JsonResponse(context_ajax, status=200)
        #end if

        # template & context
        template = 'GEMSManager/GEMSManager.html'
        context = {
            'context_form_GESMSet': my_form_GEMSSet,
        }
        return render(request, template, context=context)
    #end def











    # =============================================================================
    # POST def
    # =============================================================================
    def post(self, request):
        dict_initial_data = ShiftGen_Initial_data()
        
        return redirect('EPS1ShiftGenerator')
    #end def
#end class