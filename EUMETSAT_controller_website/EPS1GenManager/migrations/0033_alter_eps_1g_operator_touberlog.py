# Generated by Django 3.2.3 on 2021-08-19 14:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EPS1GenManager', '0032_auto_20210819_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eps_1g_operator',
            name='ToUberlog',
            field=models.CharField(choices=[('import', 'Import Batch'), ('submit', 'Submit Batch'), ('csv', 'Generate Batch (.csv)')], default='import', max_length=7),
        ),
    ]
