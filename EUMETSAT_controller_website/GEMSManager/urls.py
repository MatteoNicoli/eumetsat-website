from django.urls import path
from . import views

# from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

# Create your urls here.

urlpatterns = [
    path('', views.GEMSManager_view.as_view(), name='GEMSManager' ),
]