# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 20:58:15 2021

@author: Nicoli
"""
#%%
# =============================================================================
# 
# =============================================================================
#import urllib
import os 

import requests
import datetime

import pandas as pd



#%%
# =============================================================================
# 
# =============================================================================
def readNOAAwebsite(link):
    url = link
    r = requests.get(url, stream=True)
    Date = []
    SC = []
    Text = []
    for line in r.iter_lines():
        if ( line ):
            lineASCII = line[26:29].decode("utf-8")
            if ( lineASCII == 'SVL' ):
                n = len(line.decode("utf-8"))
                datestr = line[0:17].decode("utf-8")
                Date_date = datetime.datetime.strptime(datestr[:17], "%Y/%j/%H:%M:%S")
                
                YY = int(datestr[0:4])
                DOY = int(datestr[5:8])
                YYmmdd = datetime.datetime.strptime(str(YY)+'+'+str(DOY), "%Y+%j")
                
                HH = int(datestr[9:11])
                MM = int(datestr[12:14])
                SS = int(datestr[15:17])
                Date_date = datetime.datetime(YY, YYmmdd.month, YYmmdd.day, HH, MM, SS, 0*1000)
                
                SC_ID = int(line[23:25].decode("utf-8"))
                Text_str = line[34:n].decode("utf-8")
                
                Date.append(Date_date)
                SC.append(SC_ID)
                Text.append(Text_str)
    
    data = {'Date': Date,
            'S/C': SC,
            'Text': Text
            }
    
    NOAA_Pass = pd.DataFrame (data, columns = ['Date','S/C','Text'])
    return NOAA_Pass











def NOAA_week_sch():
    link_mon = "https://noaasis.noaa.gov/socc/polrrec.mon"
    link_tue = "https://noaasis.noaa.gov/socc/polrrec.tue"
    link_wed = "https://noaasis.noaa.gov/socc/polrrec.wed"
    link_thu = "https://noaasis.noaa.gov/socc/polrrec.thu"
    link_fri = "https://noaasis.noaa.gov/socc/polrrec.fri"
    link_sat = "https://noaasis.noaa.gov/socc/polrrec.sat"
    link_sun = "https://noaasis.noaa.gov/socc/polrrec.sun"
    NOAA_Pass_mon = readNOAAwebsite(link_mon)
    NOAA_Pass_tue = readNOAAwebsite(link_tue)
    NOAA_Pass_wed = readNOAAwebsite(link_wed)
    NOAA_Pass_thu = readNOAAwebsite(link_thu)
    NOAA_Pass_fri = readNOAAwebsite(link_fri)
    NOAA_Pass_sat = readNOAAwebsite(link_sat)
    NOAA_Pass_sun = readNOAAwebsite(link_sun)
    
    frames = [NOAA_Pass_mon, NOAA_Pass_tue, NOAA_Pass_wed, NOAA_Pass_thu, NOAA_Pass_fri, NOAA_Pass_sat, NOAA_Pass_sun]
    
    NOAA_week = pd.concat(frames).sort_values(by=['Date']).reset_index().drop(columns='index')
    NOAA_week = NOAA_week.drop_duplicates(subset=['Date', 'S/C', 'Text']).reset_index().drop(columns='index')
    
    return NOAA_week











def NOAA_BOS_shift(FDF_Pass_N19, DS, DE, filePath_NOAA_week_OLD):
    #filePath_NOAA_week_OLD = 'files\\'
    #filePath_NOAA_week_OLD='P:\\groups\\OPS\\EPS System Ops\\'
    #fileName_NOAA_week_OLD = 'Ops Support file\\NOAA_BOS.csv'
    file_Path_Name_NOAA_week_OLD = filePath_NOAA_week_OLD
    
    if (os.path.isfile(file_Path_Name_NOAA_week_OLD) == True):
        NOAA_week_OLD = pd.read_csv(file_Path_Name_NOAA_week_OLD)
        for iNOAA_week in range(0,NOAA_week_OLD.shape[0]):
            NOAA_week_OLD.at[iNOAA_week,'Date']=datetime.datetime.strptime(NOAA_week_OLD.loc[iNOAA_week,['Date']]['Date'], '%Y-%m-%d %H:%M:%S')
        
        date1N19 = NOAA_week_OLD.loc[0,['Date']]['Date']
        now_local =datetime.datetime.now()
        now_UTC =datetime.datetime.utcnow()
        Date1=datetime.datetime(now_local.year, now_local.month, now_local.day, 5, 0, 0, 0*1000)
        Date2=datetime.datetime(now_local.year, now_local.month, now_local.day, 11, 0, 0, 0*1000)
        if (date1N19<now_UTC-datetime.timedelta(hours=7*24) or (now_local>=Date1 and now_local<=Date2)):
            NOAA_week = NOAA_week_sch()
            NOAA_week.to_csv (r''+file_Path_Name_NOAA_week_OLD)
        else:
            NOAA_week = NOAA_week_OLD
    else:
        NOAA_week = NOAA_week_sch()
        NOAA_week.to_csv (r''+file_Path_Name_NOAA_week_OLD)
    
    
    
    Date = []
    SC = []
    Text = []
    N_NOAA_Pass_shift=[]
    flag = 0
    idata = []
    for wrow in range(0,NOAA_week.shape[0]):
        
        iDate = NOAA_week.loc[wrow,['Date']]['Date']
        iDateSC = NOAA_week.loc[wrow,['S/C']]['S/C']
        iDateText = NOAA_week.loc[wrow,['Text']]['Text']
        iDateText_list = list(iDateText.split(","))
        
        if ( iDateText_list[0]=='RECEIVE' and iDateText_list[1]=='HRPT' and iDate >= DS and iDate <= DE ):
            flag = 1
            idata = []
            i_GAC_ty = []
            i_GAC_fty = []
            i_GAC_fnum = []
            i_GAC_S = []
            i_GAC_E = []
            idata.append(iDateSC)
            idata.append(iDateText[13:19])
            
        if ( flag == 1):
            if (iDateText_list[0]=='RECEIVE' and iDateText_list[1]!='HRPT'):
                #if (iDateText[15:18]=='MSB'):
                
                i_GAC_ty.append(iDateText_list[3])
                i_GAC_fty.append(iDateText_list[2])
                i_GAC_fnum.append(str(int(iDateText_list[-1])*2))
                i_GAC_S=[0]*len(i_GAC_ty)
                i_GAC_E=[0]*len(i_GAC_ty)
           
            if (iDateText_list[0].lower().find('aos') != -1):
                idata.append(iDate)
            
            if (iDateText_list[0]=='PBK' and iDateText_list[1]=='START' ):
                for i_ty in range(0,len(i_GAC_ty)):
                    if (iDateText[-3:]==i_GAC_ty[i_ty]):
                        i_GAC_S[i_ty] = iDate
            
            if (iDateText_list[0]=='PBK' and iDateText_list[1]=='END' ):
                for i_ty in range(0,len(i_GAC_ty)):
                    if (iDateText[-3:]==i_GAC_ty[i_ty]):
                        i_GAC_E[i_ty] = iDate
            
            if ( iDateText_list[-1]=='END'):
                idata.append(i_GAC_ty)
                idata.append(i_GAC_fty)
                idata.append(i_GAC_fnum)
                idata.append(i_GAC_S)
                idata.append(i_GAC_E)
                idata.append(iDate)
                N_NOAA_Pass_shift.append(idata)
                flag = 0
    
    #pdb.set_trace()
    
    npass = len(N_NOAA_Pass_shift)
    FDF_Pass_N19['GDS_GAC']='NO'
    FDF_Pass_N19['NOAA_BOS']='NO'
    FDF_Pass_N19['Dump_Start']='NO'
    FDF_Pass_N19['Dump_End']='NO'
    
    iBOS=0
    wrow = 0
    flag = 1
    if (npass!=0):
        
        while (flag == 1):
            nOrbit_FDF = FDF_Pass_N19.loc[wrow,['nOrbit']]['nOrbit']
            #print(N_NOAA_Pass_shift[iBOS])
            nOrbit_BOS = int(N_NOAA_Pass_shift[iBOS][1])
            
            if (nOrbit_FDF == nOrbit_BOS):
                
                FDF_Pass_N19.at[wrow,'AOS0']=N_NOAA_Pass_shift[iBOS][2]
                FDF_Pass_N19.at[wrow,'LOS0']=N_NOAA_Pass_shift[iBOS][-1]
                GAC_Ty = N_NOAA_Pass_shift[iBOS][3]
                GAC_Type = N_NOAA_Pass_shift[iBOS][4]
                #print (nOrbit_BOS, (NOAA_Pass_shift.loc[iBOS*10+2,['Text']]['Text']))
                GAC_Frame = N_NOAA_Pass_shift[iBOS][5]
                FDF_Pass_N19.at[wrow,'GDS_GAC']=[GAC_Ty,GAC_Type,GAC_Frame]
                FDF_Pass_N19.at[wrow,'NOAA_BOS']='YES'
                
                if (N_NOAA_Pass_shift[iBOS][6]==0):
                    FDF_Pass_N19.at[wrow,'Dump_Start']=FDF_Pass_N19.loc[wrow,['AOSM']]['AOSM'] - datetime.timedelta(seconds=3*60+30)
                else:
                    FDF_Pass_N19.at[wrow,'Dump_Start']=N_NOAA_Pass_shift[iBOS][6]
                
                if (N_NOAA_Pass_shift[iBOS][7]==0):
                    FDF_Pass_N19.at[wrow,'Dump_End']=FDF_Pass_N19.loc[wrow,['AOSM']]['AOSM'] + datetime.timedelta(seconds=3*60+30)
                else:
                    FDF_Pass_N19.at[wrow,'Dump_End']=N_NOAA_Pass_shift[iBOS][7]
                
                
                iBOS = iBOS + 1
                if (iBOS == npass):
                    flag = 0
            wrow = wrow + 1
            if (wrow >= FDF_Pass_N19.shape[0]):
                flag = 0
    return FDF_Pass_N19